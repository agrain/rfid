<%@ Page Title="" Language="C#" MasterPageFile="~/Homepage.master" AutoEventWireup="true" CodeFile="BookingForm.aspx.cs" Inherits="BookingForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"/>
<div class="padding">
    <div id="border-top-booking" class="booking">
        <div>
            <div>
                <h1>Booking Form</h1>
            </div>
        </div>
    </div>

<div id="div_receipt" align="right" runat="server">
    <b><asp:Label ID="lbl_receiptbooking" runat="server" Text="Manual Receipt:"/></b>
    <div class="button1" id="div_receiptbooking">
        <div class="next">
            <asp:LinkButton ID="lnk_receiptbooking" runat="server" onclick="lnk_receiptbooking_Click" Enabled="false" >Receipt</asp:LinkButton>
		</div>					
	</div>
</div>
		
<div id="div_booking" runat="server">
    <asp:DropDownList ID="ddl_Hospital" Visible="false" runat="server" Width="5px" AppendDataBoundItems="True" ValidationGroup="Vbooking" BackColor="#FFFF99">
        <asp:ListItem/>
    </asp:DropDownList> 
    <table align="center">
        <tbody>
            <tr>
                <td><asp:label ID="lbl_RevisionNumberDisplay" runat="server" Font-Bold="true" ForeColor="#005992" text="Revision Number:" /></td>
                <td><asp:Label ID="lbl_revno" runat="server"/></td>
                <td><asp:label ID="lbl_BookingIDDisplay" runat="server" Font-Bold="true" ForeColor="#005992" text="BookingID:" /></td>
                <td class="style6"><asp:Label ID="lbl_bookingID" runat="server"/></td>
            </tr>
        </tbody>
    </table>
<table align="center" class="Vertical-AlignTop">
<tbody>
<tr>
<%--OPERATION--%><td class="Vertical-AlignTop">
    <table class="Border-Spacing5">
        <tbody>
            <tr>
                <td><b style="color:#005992">Operation Date:</b></td><td valign="middle">
                    <asp:TextBox ID="txt_opdate" runat="server" BackColor="#FFFF99" TabIndex="1"/>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_opdate" PopupButtonID="txt_opdate" Format="dd/MMM/yy" TodaysDateFormat="dd/MMM/yyyy"/> 
                    <asp:ImageButton ID="imgbtn_cal" runat="server" ImageUrl="~/Images/calendar-icon.jpg" ImageAlign="Top" />
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_opdate" PopupButtonID="imgbtn_cal" Format="dd/MMM/yy" TodaysDateFormat="dd/MMM/yyyy"/> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_OPdate" runat="server" ControlToValidate="txt_opdate" Display="None" ErrorMessage="Operation Date is Mandatory" ValidationGroup="Vbooking">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Opdate" runat="server" TargetControlID="RequiredFieldValidator_OPdate"/>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Operation Type:</b></td>
                <td><asp:DropDownList ID="ddl_operationtype" runat="server" BackColor="#FFFF99" Width="99%" TabIndex="2">
                        <asp:ListItem/>
                        <asp:ListItem Value="0">Knee</asp:ListItem>
                        <asp:ListItem Value="1">Hip</asp:ListItem>
                        <asp:ListItem Value="2">Shoulder</asp:ListItem>
                        <asp:ListItem Value="3">Spine</asp:ListItem>
                        <asp:ListItem Value="4">Trauma</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Validreqd_Oprtype" runat="server" ControlToValidate="ddl_operationtype" Display="None" ErrorMessage="Type of Operation is Mandatory" ValidationGroup="Vbooking">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Oprtype" runat="server" TargetControlID="Validreqd_Oprtype"/>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Procedure:</b></td>
                <td><asp:TextBox ID="txt_Procedure" runat="server" Width="95%" TabIndex="3"/></td>
            </tr>
            <tr>
                <td><b style="color: #005992">Side:</b></td>
                <td><asp:DropDownList ID="ddl_Side" runat="server" Width="99%" TabIndex="4">
                        <asp:ListItem/>
                        <asp:ListItem Value="0">Right</asp:ListItem>
                        <asp:ListItem Value="1">Left</asp:ListItem>
                        <asp:ListItem Value="2">Bilateral</asp:ListItem>
                        <asp:ListItem Value="3">NA</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><b style="color: #005992">Contact Number:</b></td><td><asp:TextBox ID="txt_contactnumber" runat="server" Width="95%" TabIndex="5"/></td>
            </tr>
            <tr>
                <td><b style="color: #005992">Case Type:</b></td>
                <td><asp:DropDownList ID="ddl_casetype" runat="server" Width="99%" TabIndex="6">
                        <asp:ListItem/>
                        <asp:ListItem Value="0">Primary</asp:ListItem>
                        <asp:ListItem Value="1">Revision</asp:ListItem>
                        <asp:ListItem Value="2">Removal</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </tbody>
    </table>
</td>

<%--SURGEON / PATIENT--%><td  class="Vertical-AlignTop">
<asp:label ID ="lbl_LockedForEditing" runat="server" Font-Bold="true" ForeColor="Red" Text="Fields Locked While Editing Booking." Visible="false" />
    <table class="Border-Spacing5">
        <tbody>
            <tr>
                <td><b style="color:#005992">Surgeon:</b></td>
                <td valign="middle" class=" Border-Spacing0">
                    <table class="Border-Spacing0 Browser-Margin-2" width="100%"><tbody><tr>
                    <td class="Padding0">
                        <asp:UpdatePanel ID="udp_SurgeonDDL" runat="server"  ><ContentTemplate>
                            <asp:ComboBox ID="ddl_Surgeon" CssClass=" FloatLeft CBX-PositonCorrect" runat="server" AutoPostBack="True" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" 
                                AppendDataBoundItems="True" Height="16px" Width="120px" BackColor="#FFFF99" ValidationGroup="vbooking" TabIndex="7" onselectedindexchanged="ddl_Surgeon_SelectedIndexChanged">
                                <asp:ListItem/>
                            </asp:ComboBox>
                        </ContentTemplate></asp:UpdatePanel>
                        <asp:RequiredFieldValidator ID="Validreqd_surgeon" runat="server" ControlToValidate="ddl_Surgeon" Display="None" ErrorMessage="Surgeon is Mandatory" InitialValue="0" ValidationGroup="Vbooking">*</asp:RequiredFieldValidator>
                        <%--<asp:RegularExpressionValidator ID="Validregex_Surgeon" runat="server" ControlToValidate="ddl_Surgeon" Display="None" ErrorMessage="Surgeon is Mandatory" ValidationExpression=".{1,}" ValidationGroup="vbooking"/>--%>
                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Surgeon" runat="server" TargetControlID="Validreqd_surgeon"/>
                    </td>
                    <td valign="top" class="">
                        <asp:Button ID="btn_AddSurgeon" runat="server" CssClass="icon-16-new buttons-small16 PadLeft5" Text=""/>
                    </td>
                    </tr></tbody></table>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Patient Ur</b></td>
                <td><asp:TextBox ID="txt_PatientUR" runat="server" MaxLength="20" BackColor="#FFFF99" width="95%" TabIndex="8"/>
                    <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_Urnumber" runat="server" TargetControlID="txt_PatientUR" WatermarkText="UR Number" WatermarkCssClass="WatermarkGrey"/>
                    <asp:RequiredFieldValidator ID="Validreqd_patientid" runat="server" ControlToValidate="txt_PatientUR" Display="None" ErrorMessage="Patient Id is Mandatory" ValidationGroup="Vbooking">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_PatientID" runat="server" TargetControlID="Validreqd_patientid"/>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Patient:</b></td><td>
                    <asp:TextBox ID="txt_Patientname" runat="server" BackColor="#FFFF99"  width="95%" TabIndex="9"/>
                        <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_Patientname" runat="server" TargetControlID="txt_Patientname" WatermarkText="Family Name" WatermarkCssClass="WatermarkGrey"/>
                        <asp:RequiredFieldValidator ID="Validreqd_Patientname" runat="server" ControlToValidate="txt_Patientname" Display="None" ErrorMessage="Patient Name is Mandatory" ValidationGroup="Vbooking">*</asp:RequiredFieldValidator>
                        <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Patientname" runat="server" TargetControlID="Validreqd_Patientname"/>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Patient Gender:</b></td><td>
                    <asp:DropDownList ID="ddl_gender" runat="server"  width="99%" TabIndex="10" AppendDataBoundItems="True" DataSourceID="SDS_Gender" DataTextField="refValue" DataValueField="refFlags">
                        <asp:ListItem/>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><b style="color:#005992">Patient Type:</b></td><td>
                    <asp:DropDownList ID="ddl_PatientType" runat="server"  width="99%" TabIndex="11" DataSourceID="SDS_PatType" DataTextField="refValue" DataValueField="refSortOrder" AppendDataBoundItems="True">
                        <asp:ListItem/>
                    </asp:DropDownList>
                </td>
            </tr>
        </tbody>
    </table>
</td>

<%--SUPPLIER--%><td  class="Vertical-AlignTop">
    <table class="Border-Spacing5">
        <tbody>
            <tr>
                <td><b style="color:#005992">Supplier:</b></td>
                <td valign="middle" class=" Border-Spacing0">
                    <table class="Border-Spacing0 Browser-Margin-2" width="100%"><tbody><tr>
                        <td class="Padding0">
                            <asp:UpdatePanel ID="udp_SupplierDDL" runat="server" ><ContentTemplate>
                                <asp:ComboBox ID="ddl_Supplier" CssClass="FloatLeft CBX-PositonCorrect" runat="server" AutoPostBack="True" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" 
                                    AppendDataBoundItems="True" Height="16px" Width="180px" BackColor="#FFFF99" ValidationGroup="vbooking" onselectedindexchanged="ddl_Supplier_SelectedIndexChanged" TabIndex="12" >
                                    <asp:ListItem/>
                                </asp:ComboBox>
                                <asp:RequiredFieldValidator ID="Validreqd_supplier" runat="server" ControlToValidate="ddl_Supplier" ErrorMessage="Supplier is Mandatory" InitialValue="0" ValidationGroup="Vbooking" Display="None">*</asp:RequiredFieldValidator>
                                <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_Supplier" runat="server" TargetControlID="Validreqd_supplier"/>
                            </ContentTemplate></asp:UpdatePanel>
                        </td>
                        <td valign="top" class="">
                            <asp:Button ID="btn_IncludeSupplier" runat="server" CssClass="icon-16-new buttons-small16" Text="" />
                        </td>
                    </tr></tbody></table>
                </td>
            </tr>
            <tr>
                <td><b style="color: #005992">Kit Type:</b></td>
                <td><asp:DropDownList ID="ddl_kittype" runat="server" Width="100%" TabIndex="13">
                        <asp:ListItem/>
                        <asp:ListItem Value="0">Consignment</asp:ListItem>
                        <asp:ListItem Value="1">Loan</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><b style="color: #005992">Products:</b></td><td>
                    <asp:TextBox ID="txt_productsreqd" runat="server" Width="95%" TabIndex="14"/>
                </td>
            </tr>
            <tr>
                <td><b style="color: #005992">Notes:</b></td>
                <td><asp:TextBox ID="txt_notes" runat="server" TextMode="MultiLine" Height="49px" Width="96%" TabIndex="15"/></td>
            </tr>
        </tbody>
    </table>
</td>
</tr>
</tbody>
</table>
<table align="center">
    <tbody>
        <tr>
            <td><asp:label ID="lbl_ProcessedByDisplay" runat="server" Font-Bold="true" ForeColor="#005992" text="ProcessedBy:" /></td><td><asp:Label ID="lbl_processedby" runat="server"/></td>
            <td><asp:Button ID="lnkbtn_Notify" runat="server" CssClass="icon-32-messaging buttons-big32" Text="" onclick="lnkbtn_Notify_Click"/></td>
        </tr>
    </tbody>
</table>
<div>
</div>

<asp:UpdatePanel ID="udp_Buttons" runat="server"><ContentTemplate>
<div class="button1" id="div_deleteBooking" runat="server">
<div class="next">
<asp:LinkButton ID="lnkbtn_deleteBooking" runat="server" onclick="lnkbtn_deleteBooking_Click" Enabled="false" TabIndex="18">Delete</asp:LinkButton>
</div>						
</div>
    <div class="button1" id="div_cancel" runat="server">
        <div class="next">
            <asp:LinkButton ID="lnkbtn_cancel" runat="server" onclick="lnkbtn_cancel_Click" TabIndex="17">Cancel</asp:LinkButton>
        </div>
    </div>
    <div class="button1" id="div_updateprocedure" runat="server">
        <div class="next">
            <asp:LinkButton ID="lnkbtn_updateProcedure" runat="server" onclick="lnkbtn_updateProcedure_Click" ValidationGroup="Vbooking" Enabled="false" TabIndex="16" >Update</asp:LinkButton>
        </div>							
    </div>
    <div class="button1" id="div_ack" runat="server">
        <div class="next">
            <asp:LinkButton ID="lnkbtn_supplierACK" runat="server" onclick="lnkbtn_supplierACK_Click">ACK</asp:LinkButton>
        </div>						
    </div>
</ContentTemplate></asp:UpdatePanel>
    <div align="center" style="vertical-align:bottom"><asp:Label ID="lbl_confirmProcedure" runat="server"/></div>
</div>

<%--DATASOURCES--%>
<asp:SqlDataSource ID="SDS_PatType" runat="server" ConnectionString="<%$ ConnectionStrings:RFIDConnectionString %>" SelectCommand="SELECT [refValue], [refSortOrder] FROM [tblImplantReference] WHERE ([refKey] = @refKey) ORDER BY [refSortOrder]">
    <SelectParameters>
        <asp:Parameter DefaultValue="PatType" Name="refKey" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>	
<asp:SqlDataSource ID="SDS_Gender" runat="server" ConnectionString="<%$ ConnectionStrings:RFIDConnectionString %>" SelectCommand="SELECT [refFlags], [refValue] FROM [tblImplantReference] WHERE ([refKey] = @refKey)">
    <SelectParameters>
        <asp:Parameter DefaultValue="Gender" Name="refKey" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>
	
	
<%--INCLUDE SUPPLIERS TO HOSPITAL--%>
<asp:Panel ID="Panel_IncludeSuppliers" runat="server" Width="776px" BackColor="White" CssClass=""  >
<%--INCLUDE SUPPLIERS HEADING--%>
    <asp:UpdatePanel ID="udp_IncludeSuppliers" runat="server" >
    <ContentTemplate>
<div class="Popup-Header">
        <h1>Add Suppliers to <asp:Label ID="lbl_HospName" runat="server" Height="36px"  CssClass="OverflowHidden Vertical-AlignTop" /></h1> 
        <div class="Popup-Header-Toolbar">
            <table class="FloatRight"><tbody ><tr>
                <td>
                    <asp:LinkButton ID="lnkbtn_Done" runat="server" onclick="lnkbtn_Done_Click"><span class="icon-32-deny"></span><b>Close</b></asp:LinkButton> 
                </td>
            </tr></tbody></table>
        </div> 
    </div>   

        <table width="100%" cellpadding="5px" >
            <tr valign="top" >
            <%--CURRENT SUPPLIERS--%>
                <td width="20%">
                <div class="Popup-Grid " >
                <div class="MinorHeading"> Current Suppliers </div>
                <asp:gridview ID="dg_IncludedSuppliers" runat="server" AutoGenerateColumns="false" GridLines="None" onrowdatabound="dg_IncludedSuppliers_RowDataBound" SelectedRowStyle-BackColor="#005992"  SelectedRowStyle-ForeColor="White" 
                OnSelectedIndexChanged="row_EditSupplier_Click" ShowHeader="False" Width="100%" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" >
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Panel ID="Panel_cell1" runat="server" >
                                <asp:Label ID="lbl_supId" runat="server" Text='<%#Eval("supId") %>' CssClass="FloatLeft" Visible="false" />
                                <asp:Label ID="lbl_supName" runat="server" Text='<%#Eval("supName") %>' CssClass="FloatLeft PadLeft5" />
                                <asp:Label ID="lbl_hsContactId" runat="server" Text='<%#Eval("hsContactId") %>' CssClass="FloatLeft" Visible="false" />
                                <asp:Label ID="lbl_hsEmailBooking" runat="server" Text='<%#Eval("hsEmailBooking") %>' CssClass="FloatLeft" Visible="false" />
                                <asp:panel ID="btn_RemoveSupplier" runat="server" CssClass="icon-16-download buttons-small16 FloatRight PadRight5" Text="" Visible="false" />
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                </asp:gridview>
                </div>
                <%--<asp:Button ID="testbutton" runat="server" Text="Tests" />--%>
                </td>

            <%--MODIFY DETAILS--%>
            <td width="50%">
            <asp:Panel ID="pnl_IncludingSuppliersToHospitals" runat="server" CssClass="FloatLeft" enabled="false">
            <asp:Panel ID="pnl_SelectedSupplier" runat="server" CssClass="Popup-Grid " >
            <asp:Label ID="lbl_SupplierID" runat="server" CssClass="DisplayNone" />
            <asp:Label ID="lbl_hsID" runat="server"  CssClass="DisplayNone" />
            <asp:Label ID="lbl_SupplierName" runat="server" Font-Bold="true" Font-Size="Large" Text="Supplier Details" />
            <br />
            <%--CONTACT EMAIL TEXTBOX--%>
            <div class="Popup-TxtPair" >
            Email: &nbsp
            <asp:TextBox ID="txt_ContactEmail" runat="server" ToolTip="Supplier Contact Email" BackColor="#FFFF99" />
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_ContactEmail" runat="server" TargetControlID="txt_ContactEmail" WatermarkText="Supplier Contact Email" WatermarkCssClass="WatermarkGrey" />

            <%--<asp:RequiredFieldValidator ID="validreqd_ContactEmail" runat="server" ControlToValidate="txt_ContactEmail" Display="None" ErrorMessage="Please provide the generic email address of this Supplier" ValidationGroup="VIncludeSupplier"/>
            <asp:ValidatorCalloutExtender ID="ValidExtender_ContactEmail" runat="server" TargetControlID="validreqd_ContactEmail"/>
            <asp:RegularExpressionValidator ID="Validregex_ContactEmail" runat="server" ControlToValidate="txt_ContactEmail" Display="None" ErrorMessage="Please provide the generic email address of this Supplier" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VIncludeSupplier"/>
            <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_CompareContactEmail" runat="server" TargetControlID="Validregex_ContactEmail"/>--%>
            <%--SEND EMAILS CHECKBOX--%>
            <asp:CheckBox id="cbx_EmailBooking" Checked="true" Text="Send Booking Emails" runat="server" CssClass="PadLeft5 PadRight5" />
            </div>

            <%--PHONE NUMBER--%>
            <div class="Popup-TxtPair FloatLeft " >
            Phone: &nbsp
            <asp:TextBox ID="txt_SupplierPhone" runat="server" ToolTip="Supplier Phone Number" />
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_SupplierPhone" runat="server" TargetControlID="txt_SupplierPhone" WatermarkText="Supplier Phone Number" WatermarkCssClass="WatermarkGrey" />
            </div>
            <%--FAX NUMBER--%>
            <div class="Popup-TxtPair FloatLeft" >
            Fax: &nbsp &nbsp &nbsp
            <asp:TextBox ID="txt_SupplierFax" runat="server" ToolTip="Supplier Fax Number" />
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_SupplierFax" runat="server" TargetControlID="txt_SupplierFax" WatermarkText="Supplier Fax Number" WatermarkCssClass="WatermarkGrey" />
            </div>
            <br /><br />

            <%--List of Contacts--%>
            <asp:panel ID="pnl_ISDSupplierContacts" runat="server" class="Popup-Grid" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" >
            <asp:Label ID="lbl_ISDSupplierContactsHeading" runat="server" CssClass="MinorHeading MargTop5 PadBottom2" Text="Contacts Associated With Supplier"  />

            <asp:gridview ID="gv_SupplierAssociatedContacts" runat="server" AutoGenerateColumns="false" onrowdatabound="gv_SupplierAssociatedContacts_RowDataBound" SelectedRowStyle-BackColor="#005992" SelectedRowStyle-ForeColor="White" 
            OnSelectedIndexChanged="row_RemoveThisContact_Click" GridLines="None" ShowHeader="False" Width="100%" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" >
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Panel ID="Panel_cell1" runat="server">
                            <asp:Label ID="lbl_hscId" runat="server" Text='<%#Eval("hscId") %>' CssClass="FloatLeft" Visible="false" />
                            <asp:Label ID="lbl_accId" runat="server" Text='<%#Eval("accId") %>' CssClass="FloatLeft PadLeft5" Visible="false" />
                            <asp:Label ID="lbl_accName" runat="server" Text='<%#Eval("accName") %>' CssClass="FloatLeft PadLeft5" />
                            <asp:Label ID="btn_accUsername" runat="server"  Text='<%#Eval("accUsername") %>'  CssClass="FloatLeft PadLeft5"/>
                            <asp:panel ID="btn_RemoveThisContact" runat="server" CssClass="icon-16-delete buttons-small16 FloatRight MargRight5" Text="" />
                        </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            </asp:gridview>
             
            <%--Adding New Contacts--%>
            <asp:Panel ID="pnl_ISDAddSupplierContact" runat="server" Height="15px" >
            <asp:LinkButton ID="btn_ISDAddSupplierContact" runat="server" CssClass="LinkButton2 icon-16-upload buttons-small16" Text="Add New" onclick="btn_ISDAddSupplierContact_Click" ValidationGroup="VAddSupplierContact" Width="50px" />
                <asp:Panel ID="pnl_ISDAddSupplierContact_Name" runat="server" CssClass="FloatLeft" >
                    <asp:Label ID="lbl_ISDAddSupplierContact_Name" runat="server" Text="" />
                    <asp:TextBox ID="txt_ISDAddSupplierContact_Name" runat="server" />
                    <asp:TextBoxWatermarkExtender id="txtwme_ISDAddSupplierContact_Name" runat="server" TargetControlID="txt_ISDAddSupplierContact_Name" WatermarkText="Contact Name" WatermarkCssClass="WatermarkGrey" />
                    <asp:RequiredFieldValidator ID="validreqd_ISDAddSupplierContact_Name" runat="server" ControlToValidate="txt_ISDAddSupplierContact_Name" Display="None" ErrorMessage="Please provide the name of the new Contact" ValidationGroup="VAddSupplierContact"/>
                    <asp:ValidatorCalloutExtender ID="ValidExtender_ISDAddSupplierContact_Name" runat="server" TargetControlID="validreqd_ISDAddSupplierContact_Name"/>
                </asp:Panel> 
                <asp:Panel ID="pnl_ISDAddSupplierContact_Email" runat="server" CssClass="FloatLeft">
                    <asp:Label ID="lbl_ISDAddSupplierContact_Email" runat="server" Text="" />
                    <asp:TextBox ID="txt_ISDAddSupplierContact_Email" runat="server" />
                    <asp:TextBoxWatermarkExtender id="txtwme_ISDAddSupplierContact_Email" runat="server" TargetControlID="txt_ISDAddSupplierContact_Email" WatermarkText="Contact Email" WatermarkCssClass="WatermarkGrey" />
                    <asp:RequiredFieldValidator ID="validreqd_ISDAddSupplierContact_Email" runat="server" ControlToValidate="txt_ISDAddSupplierContact_Email" Display="None" ErrorMessage="Please provide the Email of the new Contact" ValidationGroup="VAddSupplierContact"/>
                    <asp:ValidatorCalloutExtender ID="ValidExtender_ISDAddSupplierContact_Email" runat="server" TargetControlID="validreqd_ISDAddSupplierContact_Email"/>
                    <asp:RegularExpressionValidator ID="Validregex_ISDAddSupplierContact_Email" runat="server" ControlToValidate="txt_ISDAddSupplierContact_Email" Display="None" ErrorMessage="Please provide the email address of this Contact" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VAddSupplierContact"/>
                    <asp:ValidatorCalloutExtender ID="ValidatorCalloutExtender_CompareISDAddSupplierContact_Email" runat="server" TargetControlID="Validregex_ISDAddSupplierContact_Email"/>
                </asp:Panel> 
            </asp:Panel>

            <%--Removing Linked Contacts?--%></asp:panel>
            <asp:LinkButton ID="btn_IncludeThisSupplier" runat="server" onclick="btn_IncludeThisSupplier_Click" ValidationGroup="VIncludeSupplier"  CssClass="LinkButton2 icon-16-apply" Text="Confirm Supplier Changes"/>
            <br/>
            </asp:Panel>
            </asp:Panel>
            </td>
            <%--ALL EXISTING SUPPLIERS--%>
            <td width="30%">
            <%--INCLUDE SUPPLIER BUTTON--%>
            <div class="Popup-Grid">
            <div class="MinorHeading"> Other Existing Suppliers </div>
                <asp:GridView ID="dg_RemainingSuppliers" runat="server" AutoGenerateColumns="false" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" GridLines="None" onrowdatabound="dg_RemainingSuppliers_RowDataBound" OnSelectedIndexChanged="row_IncludeThisSupplier_Click" 
                    SelectedRowStyle-BackColor="#005992" SelectedRowStyle-ForeColor="White" ShowHeader="False" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Panel ID="Panel_cell1" runat="server">
                                    <asp:Label ID="lbl_supId" runat="server" CssClass="FloatLeft" Text='<%#Eval("supId") %>' Visible="false" />
                                    <asp:Label ID="lbl_supName" runat="server" CssClass="FloatLeft PadLeft5" Text='<%#Eval("supName") %>' />
                                    <asp:Panel ID="btn_IncludeThisSupplier" runat="server" CssClass="icon-16-upload buttons-small16 FloatRight PadRight5" Text="" />
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <asp:Panel ID="pnl_AddNewSupplierButtonHolder" runat="server" CssClass="Popup-Grid FloatLeft " >
            <asp:LinkButton id="lnkbtn_AddNewSupplier" runat="server" onclick="lnkbtn_AddNewSupplier_Click" CssClass="icon-16-new LinkButton2" Text="Add New Supplier"/>
            </asp:Panel>

            <asp:Panel ID="pnl_AddNewSupplier" runat="server" Visible="false" CssClass="Popup-Grid FloatLeft " >
            <%--NEW SUPPLIER NAME TEXTBOX--%>
            <asp:TextBox ID="txt_NewSupplierName" runat="server"  BackColor="#FFFF99" ontextchanged="txt_NewSupplierName_TextChanged" />
            <asp:TextBoxWatermarkExtender ID="TextBoxWatermark_NewSupplierName" runat="server" TargetControlID="txt_NewSupplierName" WatermarkText="New Supplier Name"/>
            <asp:Label ID="lbl_AddNewSupplierValidationExists" runat="server" Text="This supplier already exists." ForeColor="Red" visible="false" />
            <asp:RequiredFieldValidator ID="validreqd_AddNewSupplier" runat="server" ControlToValidate="txt_NewSupplierName" Display="None" ErrorMessage="Please provide the name of the new Supplier" ValidationGroup="VAddNewSupplier"/>
            <asp:ValidatorCalloutExtender ID="ValidExtender_AddNewSupplier" runat="server" TargetControlID="validreqd_AddNewSupplier"/>
            <br />
            <asp:LinkButton id="lnkbtn_ConfirmNewSupplier" runat="server" onclick="lnkbtn_ConfirmNewSupplier_Click" CssClass="LinkButton2 icon-16-upload" ValidationGroup="VAddNewSupplier" Text="Confirm" />
            <asp:LinkButton id="lnkbtn_CancelNewSupplier" runat="server" onclick="lnkbtn_CancelNewSupplier_Click"  CssClass="LinkButton2 icon-16-delete" Text="Cancel"/>
            </asp:Panel>
            </td>
            </tr>
        </table>      

        <div class="clr"></div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mpe_IncludedSuppliers" runat="server" TargetControlID="btn_IncludeSupplier" BackgroundCssClass="modalBackground" PopupControlID="Panel_IncludeSuppliers" />

<%--NOTIFY SUPPLIER, CONTACTS AND SURGEONS--%>
<asp:Panel ID="Panel_Notify" runat="server" Width="700px" BackColor="White" CssClass="" > 
<%--NOTIFY HEADING--%>
    <div id="border-top-bar" class="bar">
        <div class="user-toolbar" id="Div1">
            <asp:UpdatePanel ID="upd_N_Done" runat="server"><ContentTemplate>
                <asp:LinkButton ID="lnkbtn_N_Done" runat="server" onclick="lnkbtn_N_Close_Click"><span class="icon-32-deny"></span><b>Close</b></asp:LinkButton> 
                <asp:LinkButton ID="lnkbtn_N_Ok" runat="server" onclick="lnkbtn_N_Ok_Click" CssClass="DisplayNone"><span class="icon-32-apply"></span><b>Notify</b></asp:LinkButton>
            </ContentTemplate></asp:UpdatePanel>
        </div> 
        <h1>Booking Notification</h1> 
    </div>   
    <asp:UpdatePanel ID="udp_Notify" runat="server" >
        <ContentTemplate>
            <asp:GridView ID="gv_SupplierContactsEmailToggle" runat="server" Width="90%" AutoGenerateColumns="false" CssClass="MargTop5 MargBot10 " HorizontalAlign="Center" onrowdatabound="gv_SupplierContactsEmailToggle_RowDataBound" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px">
                <Columns>
<%--                    <asp:ImageField Visible="false" />
                    <asp:BoundField DataField="accId" HeaderText="" Visible="false" />
                    <asp:BoundField DataField="to" HeaderText="" Visible="false" />
                    <asp:BoundField DataField="accName" HeaderText="Name" />
                    <asp:BoundField DataField="accUsername" HeaderText="Email" />
                    <asp:CheckBoxField DataField="Selected" HeaderText="Notify" ItemStyle-CssClass="enabled" />--%>
                
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table width="100%">
                                <tr>
                                    <td width="30%" class="PadLeft10 FloatLeft"><asp:Label ID="hlbl_Name" runat="server" Text="Name" /></td>
                                    <td width="40%" class="PadLeft10 FloatLeft"><asp:Label ID="hlbl_Email" runat="server" Text="Email" /></td>
                                    <td width="20%" class="PadLeft10 FloatLeft"><asp:Label ID="hlbl_Notify" runat="server" Text="Notify" /></td>                    
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table width="100%">
                                <tr>
                                    <td width="30%" class="PadLeft10 FloatLeft"><asp:Label ID="lbl_accId" runat="server" Text='<%#Eval("accId") %>' Visible="false" />
                                    <asp:Label ID="lbl_to" runat="server" Text='<%#Eval("to") %>' Visible="false" />
                                    <asp:Label ID="lbl_accName" runat="server" Text='<%#Eval("accName") %>' /></td>
                                    <td width="40%" class="PadLeft10 FloatLeft"><asp:Label ID="lbl_accUsername" runat="server" Text='<%#Eval("accUsername") %>' ToolTip='<%#Eval("accName") %>' /></td>
                                    <td width="20%" class="PadLeft10 FloatLeft"><asp:CheckBox ID="cbx_ContactNotify" runat="server" Checked='<%# Convert.ToBoolean(Eval("Selected")) %>' AutoPostBack="true" BorderStyle="None" /></td> 
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="clr"></div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mpe_Notify" runat="server" TargetControlID="lnkbtn_Notify" BackgroundCssClass="modalBackground" PopupControlID="Panel_Notify" />

<%--ADD SURGEONS TO HOSPITAL--%>
<asp:Panel ID="Panel_AddSurgeons" runat="server"  BackColor="White" CssClass="WidthAuto " >
<%--INCLUDE SUPPLIERS HEADING--%>
<asp:UpdatePanel ID="pas_udp_IncludeSuppliers" runat="server" >
<ContentTemplate>
    <div class="Popup-Header">
        <h1>Add Surgeons to <asp:Label ID="pas_lbl_HospName" runat="server" Height="36px"  CssClass="OverflowHidden Vertical-AlignTop" /></h1> 
        <div class="Popup-Header-Toolbar">
            <table class="FloatRight"><tbody><tr>
                <td>
                    <asp:UpdatePanel ID="pas_udp_Add_Supplier" runat="server"><ContentTemplate>
                        <asp:LinkButton ID="pas_lnkbtn_Add" runat="server" onclick="pas_lnkbtn_Add_Click"><span class="icon-32-new"></span><b>Add</b></asp:LinkButton> 
                    </ContentTemplate></asp:UpdatePanel>
                </td>
                <td>
                    <asp:LinkButton ID="pas_lnkbtn_Done" runat="server" onclick="pas_lnkbtn_Done_Click"><span class="icon-32-deny"></span><b>Close</b></asp:LinkButton> 
                </td>
            </tr></tbody></table>
        </div> 
    </div>   
<table width="100%" cellpadding="5px" >
<tr valign="top" >
<%--CURRENT SURGEONS--%>
<td width="40%">
<div class="Popup-Grid" >
<div class="MinorHeading"> Current Surgeons </div>
<div class="OverflowYScroll height-400px BorderTemplate">
<asp:gridview ID="pas_dg_CurrentSurgeons" runat="server" AutoGenerateColumns="false" GridLines="None" onrowdatabound="pas_dg_CurrentSurgeons_RowDataBound" SelectedRowStyle-BackColor="#005992"  SelectedRowStyle-ForeColor="White" 
OnSelectedIndexChanged="row_EditSurgeon_Click" ShowHeader="False" Width="100%">
<Columns>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:Panel ID="Panel_cell1" runat="server" >
                <asp:Label ID="lbl_hsurId" runat="server" Text='<%#Eval("hsurId") %>' CssClass="FloatLeft" Visible="false" />
                <asp:Label ID="lbl_accId" runat="server" Text='<%#Eval("accId") %>' CssClass="FloatLeft" Visible="false" />
                <asp:Label ID="lbl_accName" runat="server" Text='<%#Eval("accName") %>' CssClass="FloatLeft" Visible="false" />
                <asp:Label ID="lbl_accUsername" runat="server" Text='<%#Eval("accUsername") %>' CssClass="FloatLeft PadLeft5" Visible="true" />
                <asp:Label ID="lbl_drFamilyName" runat="server" Text='<%#Eval("drFamilyName") %>' CssClass="FloatLeft" Visible="false" />
                <asp:Label ID="lbl_drGivenName" runat="server" Text='<%#Eval("drGivenName") %>' CssClass="FloatLeft" Visible="false" />
                <asp:Label ID="lbl_drTitle" runat="server" Text='<%#Eval("drTitle") %>' CssClass="FloatLeft" Visible="false" />
                <asp:panel ID="btn_RemoveSurgeon" runat="server" CssClass="icon-16-download buttons-small16 FloatRight PadRight5" Text="" Visible="false" />
            </asp:Panel>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:gridview>
</div>
</div>
</td>

<%--MODIFY DETAILS--%>
<td width="60%">
<asp:Panel ID="pas_pnl_IncludingSurgeonsToHospitals" runat="server" CssClass="FloatLeft" Width="100%" enabled="true">
<asp:Panel ID="pas_pnl_SelectedSurgeon" runat="server" CssClass="Popup-Grid " >
<asp:Label ID="pas_lbl_SurgeonID" runat="server" CssClass="DisplayNone" />
<asp:Label ID="pas_lbl_hsurID" runat="server"  CssClass="DisplayNone" />
<asp:Label ID="pas_lbl_SurgeonName" runat="server" Font-Bold="true" Font-Size="Large" Text="Surgeon Name" />
<br />
<%--CONTACT EMAIL TEXTBOX--%>
<div class="Popup-TxtPair" >
<div class="width-10 FloatLeft "> Email:</div>
<asp:TextBox ID="pas_txt_ContactEmail" runat="server" ToolTip="Surgeon Contact Email" BackColor="#FFFF99" Width="88%" />
<asp:RegularExpressionValidator ID="Validregex_pas_txt_ContactEmail" runat="server" ControlToValidate="pas_txt_ContactEmail" Display="None" ErrorMessage="An Email address is Mandatory" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="VIncludeSurgeon">*</asp:RegularExpressionValidator>
<asp:ValidatorCalloutExtender ID="ValidExtender_pas_txt_ContactEmail" runat="server" TargetControlID="Validregex_pas_txt_ContactEmail"/>
<asp:TextBoxWatermarkExtender ID="pas_TextBoxWatermark_ContactEmail" runat="server" TargetControlID="pas_txt_ContactEmail" WatermarkText="Surgeon Contact Email" WatermarkCssClass="WatermarkGrey" />
</div>
<%--TITLE--%>
<div class="Popup-TxtPair FloatLeft " >
Title:<br></br>
<asp:TextBox ID="pas_txt_SurgeonTitle" runat="server" ToolTip="Surgeon Title" Width="40px" />
<asp:RequiredFieldValidator ID="Validreq_pas_txt_SurgeonTitle" runat="server" ControlToValidate="pas_txt_SurgeonTitle" ErrorMessage="Surgeons require a Title" ValidationGroup="VIncludeSurgeon" Display="None">*</asp:RequiredFieldValidator>
<asp:ValidatorCalloutExtender ID="ValidExtender_pas_txt_SurgeonTitle" runat="server" TargetControlID="Validreq_pas_txt_SurgeonTitle"/>
</div>
<%--GIVEN NAME--%>
<div class="Popup-TxtPair FloatLeft width-40" >
Given Name:<br></br>
<asp:TextBox ID="pas_txt_SurgeonGivenName" runat="server" ToolTip="Surgeon Given Name" Width="100%"/>
<asp:RequiredFieldValidator ID="Validreq_pas_txt_SurgeonGivenName" runat="server" ControlToValidate="pas_txt_SurgeonGivenName" ErrorMessage="Surgeons require a Given Name" ValidationGroup="VIncludeSurgeon" Display="None">*</asp:RequiredFieldValidator>
<asp:ValidatorCalloutExtender ID="ValidExtender_pas_txt_SurgeonGivenName" runat="server" TargetControlID="Validreq_pas_txt_SurgeonGivenName"/>
<asp:TextBoxWatermarkExtender ID="pas_TextBoxWatermark_SurgeonGivenName" runat="server" TargetControlID="pas_txt_SurgeonGivenName" WatermarkText="Surgeon Given Name" WatermarkCssClass="WatermarkGrey" />
</div>
<%--FAMILY NAME--%>
<div class="Popup-TxtPair FloatLeft width-40" >
Family Name:<br></br>
<asp:TextBox ID="pas_txt_SurgeonFamilyName" runat="server" ToolTip="Surgeon Family Name" Width="100%"/>
<asp:RequiredFieldValidator ID="Validreq_pas_txt_SurgeonFamilyName" runat="server" ControlToValidate="pas_txt_SurgeonFamilyName" ErrorMessage="Surgeons require a Family Name" ValidationGroup="VIncludeSurgeon" Display="None">*</asp:RequiredFieldValidator>
<asp:ValidatorCalloutExtender ID="ValidExtender_pas_txt_SurgeonFamilyName" runat="server" TargetControlID="Validreq_pas_txt_SurgeonFamilyName"/>
<asp:TextBoxWatermarkExtender ID="pas_TextBoxWatermark_SurgeonFamilyName" runat="server" TargetControlID="pas_txt_SurgeonFamilyName" WatermarkText="Surgeon Family Name" WatermarkCssClass="WatermarkGrey" />
</div>

<div class="clr"></div>
<asp:LinkButton ID="pas_btn_IncludeThisSurgeon" runat="server" onclick="pas_btn_IncludeThisSurgeon_Click" ValidationGroup="VIncludeSurgeon"  CssClass="LinkButton2 icon-16-apply" Text="Confirm Surgeon"/>
<br/>
</asp:Panel>
</asp:Panel>
</td>
</tr>
</table>      
<div class="clr"></div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mpe_AddSurgeons" runat="server" TargetControlID="btn_AddSurgeon" BackgroundCssClass="modalBackground" PopupControlID="Panel_AddSurgeons" />

</div> 
</asp:Content>