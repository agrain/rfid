﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Homepage : System.Web.UI.MasterPage
{
    RFIDDataContext Orfid = new RFIDDataContext();
    RFID ClassRFID = new RFID();
    /// <summary>
    /// In this functionality we are trying to Populte Pending Rrequests 
    ///We also have Idea of Hiding the Admin button but it won`t be hidden for the moment
    ///one drawback with this is we don`t want to send requests twice to the database evey time Master page is loaded, because master page will be loaded evertime a Page is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["User"] != null)
            {
                lbl_User.Text = Session["User"].ToString();
                var Accountreq = Orfid.rp_implantAccountRequests(int.Parse(Session["AccID"].ToString()), null);
                int intAccountreq = Accountreq.Count();
                if (intAccountreq > 0)
                {
                    lbl_request.Text = intAccountreq.ToString();
                    requests_pending_status.Visible = true;
                }
                else
                {
                    requests_pending_status.Visible = false;
                }
            }
            else//New user
            {
                lnk_Admin.Visible = false;
                lnk_logout.Visible = false;
                lnk_Home.Visible = false;
                lbl_User.Visible = false;
                requests_pending_status.Visible = false;
                lnk_logout.Visible = false;
                lnkbtn_Bookingform.Visible = false;
            }
            if (Session["Admin"] != "Active")
            {
                lnk_Admin.Visible = false;
                requests_pending_status.Visible = false;
            }

            if (Session["OrderLoanKit"] != null ) // || Session["SuplierID"] != null)
            {
                lnkbtn_Bookingform.Visible = true;
            }
            else
            {
                lnkbtn_Bookingform.Visible = false;
            }
            if (Session["Hospname"] != null)
            {
                lbl_HospBanner.Text = Session["Hospname"].ToString() + "  " + Session["HospCampus"].ToString();
            }
            if (Session["SuppName"] != null)
            {
                lbl_HospBanner.Text = Session["SuppName"].ToString();
            }
        }
        catch (Exception EMaster)
        {            
            Orfid.Usp_WebErrors("Homepage.Master", "PageLoad", DateTime.Now.AddDays(0),EMaster.Message.ToString());
        }
    }
    
    protected void lnk_logout_Click(object sender, EventArgs e)
    {
        ClassRFID.StopReader();
        Session.Abandon();
        Response.Redirect("Login.aspx", false);
    }
    protected void lnk_profile_Click(object sender, EventArgs e)
    {
        ClassRFID.StopReader();
        Response.Redirect("ProfileSettings.aspx", false);
    }
    protected void lnk_Admin_Click(object sender, EventArgs e)
    {
        ClassRFID.StopReader();
        Response.Redirect("Users.aspx?qry=Admin");
    }
    protected void lnk_Home_Click(object sender, EventArgs e)
    {
        ClassRFID.StopReader();
        Session["ReceiveType"] = "0";
        if (Session["SuppName"] !=null)
        {
            Response.Redirect("Procedure.aspx?qSupplier=" + Session["SuppName"].ToString());
        }
        else
        Response.Redirect("Procedure.aspx", false);
    }
    protected void lnkbtn_Bookingform_Click(object sender, EventArgs e)
    {
        ClassRFID.StopReader();
        Response.Redirect("BookingForm.aspx");
    }
}
