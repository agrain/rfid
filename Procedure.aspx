<%@ Page Title="" Language="C#" MasterPageFile="~/Homepage.master" AutoEventWireup="true" CodeFile="Procedure.aspx.cs" Inherits="Procedure" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"/>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers><asp:PostBackTrigger ControlID="ddl_filter_Supplier" /></Triggers>
<ContentTemplate>
<div class="border">
<div class="padding">

<%--FILTER BOX--%>
<div id="submenu-box" >
<!--[if IE 6]><div id="min-width"></ div><![endif]-->
<div class="t"><div class="t"><div class="t"></div></div></div>
<%--HEADING--%>
<div class="filter">
<%--<table style=" width:100%; border-spacing:0;">
<tr>
<td style="width:70%">
<h2>Filter Orders List</h2>  
</td>
<td  style="width:30%; ">     
<div id="colour-palette" style="visibility:hidden; height:1px;">
<span>Booking Status Key:  </span>
<asp:LinkButton runat="server" ID="Lnkbtn_pallet1" BackColor="OldLace" ToolTip="New Bookings - Before supplier ASNs are Received."  Width="20" Height="20" ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton1" BackColor="Khaki" ToolTip="Order Booked - Booking is awaiting the arrival of loan kits." Width="20" Height="20" ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton2" BackColor="LightGreen" ToolTip="Received - Loan kits have been Received and are ready to be used in surgery." Width="20" Height="20"   ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton3" BackColor="LightCoral" ToolTip="Usage - Surgery in progress or recently completed." Width="20" Height="20" BorderColor="Black" ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton4" BackColor="Plum" ToolTip="Dispatch - Loan kits have been put back through the tunnel and are ready to ship." Width="20" Height="20" ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton5" BackColor="LightBlue" ToolTip="Shipped - Loan kits have been returned to supplier and order is ready for billing." Width="20" Height="20"  ></asp:LinkButton>
<asp:LinkButton runat="server" ID="LinkButton6" BackColor="Wheat"  ToolTip="Consumed - Final consumption has been calculated and the order is complete." Width="20" Height="20" ></asp:LinkButton>
</div>
</td>      
</tr>
</table> 
<hr />  --%>

<%--FILTERS BOX--%>
<div id="filter-wrapper">
<table>
    <tr>
        <td>
            <div id="filter-inputs">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="Lnkbtn_search">
                    <asp:panel ID="pnl_Filt_Date" runat="server" CssClass="filt">
                    <%--<div class="filt">--%>
                        <span id="spanToDate" runat="server">
                            <span>Date:<br></span><asp:TextBox ID="txt_Todate" runat="server" AutoPostBack="True" ValidationGroup="order"/>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_Todate" Format="dd/MMM/yyyy" PopupButtonID="txt_Todate"/>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_Todate" Format="dd/MMM/yyyy" PopupButtonID="Img_todate"/>
                            <asp:ImageButton ID="Img_todate" runat="server" ImageUrl="~/Images/calendar-icon.jpg" ImageAlign="Top" width="15px" />
                        </span> 
                    <%--</div>--%>
                    </asp:panel>
                    <div class="filt">
                        <span id="spanPatientFilter" runat="server">
                            <span>Patient:<br></span><asp:TextBox ID="txt_filter_Patient" runat="server" ToolTip="Patient Family Name"/>
                        </span>
                    </div>
                    <div class="filt">
                        <span id="spanFilterDoctor" runat="server">
                            <span>Doctor:<br></span><asp:TextBox ID="txt_filter_doctor" runat="server" ValidationGroup="order" ToolTip="Doctor Family Name"/>
                        </span>
                    </div>
                    <div class="filt">
                        <span id="span_bknofilter" runat="server">
                            <span>Booking Ref#<br></span><asp:TextBox ID="txt_filter_bookingno" runat="server"/>
                        </span>
                    </div>
                    <div class="filt">
                        <span id="spanStatus" runat="server">
                            <span>Status:<br></span>
                            <asp:DropDownList ID="ddl_fileterby" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>New Booking</asp:ListItem>
                                <asp:ListItem>Acknowledged</asp:ListItem>
                                <asp:ListItem>Shipping Note Received</asp:ListItem>
                                <asp:ListItem>Received</asp:ListItem>
                                <asp:ListItem>In Theatre</asp:ListItem>
                                <asp:ListItem>Ready For Dispatch</asp:ListItem>
                                <asp:ListItem>Dispatched</asp:ListItem>
                                <asp:ListItem>Complete</asp:ListItem>
                            </asp:DropDownList>
                        </span> 
                    </div>
                    <div class="filt">
                        <span id="spanSupplier" runat="server">
                            <span>Supplier:<br></span><asp:DropDownList ID="ddl_filter_Supplier" runat="server" AppendDataBoundItems="True"><asp:ListItem></asp:ListItem></asp:DropDownList>
                        </span> 
                    </div>
                    <div class="filt">
                        <span id="span_hospfilter" runat="server">
                            <span>Hospital:<br></span><asp:DropDownList ID="ddl_filter_hosp" runat="server" AppendDataBoundItems="True"><asp:ListItem></asp:ListItem></asp:DropDownList>
                        </span>
                    </div>  
                    
                </asp:Panel> 
            </div> 
        </td>
        <td style="width:150px">
            <asp:linkbutton ID="lnkbtn_Reset" runat="server" onclick="lnkbtn_Reset_Click" ValidationGroup="validuser" class="button-dynamic FloatRight"><div id="Lg"></div><div id="Cg">&nbsp; RESET &nbsp;</div><div id="Rg"></div></asp:linkbutton>
            <asp:linkbutton ID="Lnkbtn_search" runat="server" onclick="Lnkbtn_search_Click" ValidationGroup="validuser" class="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp; FILTER &nbsp;</div><div id="R"></div></asp:linkbutton>
        </td> 
    </tr>
</table> 
<em><asp:Label ID="lbl_SearchResults" runat="server"></asp:Label></em>         
</div>
</div>
<div class="b"><div class="b"><div class="b"></div></div></div>
</div>

<%--NAVIGATION BUTTONS BOX--%>
<div id="content-box">
<!--[if IE 6]><div id="min-width"></ div><![endif]-->
<div class="border">
<div class="padding">
<div id="toolbar-box">
<div class="t"><div class="t"><div class="t"></div></div></div>
<div class="m">
    <asp:Label ID="lbl_current_day" runat="server" CssClass="DisplayNone"/><%--keep this lable to track the current day--%>
    <table width="100%" >
        <tr>
            <td id="date_nav_left">
                
                <asp:linkbutton ID="Imgbtn_prev" runat="server" onclick="Lnkbtn_prev_Click" ValidationGroup="validuser" class="button-dynamic FloatLeft"><div id="L"></div><div id="C">&nbsp;&nbsp; PREV &nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
                <asp:linkbutton ID="Imgbtn_next" runat="server" onclick="Lnkbtn_next_Click" ValidationGroup="validuser" class="button-dynamic FloatLeft"><div id="L"></div><div id="C">&nbsp;&nbsp; NEXT &nbsp;&nbsp;</div><div id="R"></div></asp:linkbutton>
            </td>
            <td id="date_nav_center">
                <h2><asp:Label ID="lbl_DateDisplay" runat="server"/></h2>
            </td>
            <td id="date_nav_right">
                <div class="buttons-right">
                    <asp:LinkButton ID="lnkbtn_DayToggle" runat="server" onclick="lnkbtn_DayToggle_Click">DAY </asp:LinkButton>
                    <asp:LinkButton ID="lnkbtn_WeekToggle" runat="server" onclick="lnkbtn_WeekToggle_Click">WEEK </asp:LinkButton>
                    <asp:LinkButton ID="lnkbtn_MonthToggle" runat="server" onclick="lnkbtn_MonthToggle_Click">MONTH </asp:LinkButton>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="b"><div class="b"><div class="b"></div></div></div>
</div>
</div>
</div>
</div>
<div class="clr"></div>

<%--CALENDAR BOX--%>
<div id="element-box">
<!--[if IE 6]><div id="min-width"></ div><![endif]-->
<div class="t"><div class="t"><div class="t"></div></div></div>
<div class="m"><div id="calendar-wrapper"><div id="calendar">
<%--WEEK--%>
<asp:panel id="weekView" runat="server">
<table style="width: 100%">
<tr>
<%--WEEK DAY 1--%>
<td valign="top"><table><tbody>
    <tr>
        <th class="days"><span class="week-dayname"><asp:Label ID="dow1" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day1" runat="server" onclick="lbl_day1_Click"/></span></th>
    </tr>
    <tr>
        <td class="opas">
            <asp:GridView ID="GridView_list1" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list1_RowDataBound"  OnSelectedIndexChanged="GridView_list1_SelectedIndexChanged" GridLines="None" ShowHeader="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
                                <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                                <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>                                                        
                                <asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                                <asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                                <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"/>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</tbody></table></td>
<%--WEEK DAY 2--%>
<td valign="top"><table><tbody>
    <tr>
        <th class="days"><span class="week-dayname"><asp:Label ID="dow2" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day2" runat="server" onclick="lbl_day2_Click"/></span></th>
    </tr>
    <tr>
        <td class="opas">
            <asp:GridView ID="GridView_list2" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list2_RowDataBound" OnSelectedIndexChanged="GridView_list2_SelectedIndexChanged" GridLines="None" ShowHeader="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
                                <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                                <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>                                                        
                                <asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                                <asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                                <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"/>
                            </asp:Panel>                                                                         
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</tbody></table></td>
<%--WEEK DAY 3--%>
<td valign="top"><table><tbody>
    <tr>
        <th class="days"><span class="week-dayname"><asp:Label ID="dow3" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day3" runat="server" onclick="lbl_day3_Click"/></span></th>
    <tr>
        <td class="opas">
            <asp:GridView ID="GridView_list3" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list3_RowDataBound" OnSelectedIndexChanged="GridView_list3_SelectedIndexChanged" GridLines="None" ShowHeader="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
                                <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                                <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>                                                        
                                <asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label><br />
                                <asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                                <asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                                <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"/>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</tbody></table></td>
<%--WEEK DAY 4--%>
<td valign="top"><table><tr><th class="days"><asp:Label ID="dow4" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day4" runat="server" onclick="lbl_day4_Click" ></asp:LinkButton></span></th>
</tr>
<tr><td class="opas">
<asp:GridView ID="GridView_list4" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list4_RowDataBound" OnSelectedIndexChanged="GridView_list4_SelectedIndexChanged" GridLines="None" ShowHeader="False">
<Columns>
<asp:TemplateField>
<ItemTemplate>
<asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
<asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
<asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>                                                        
<asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>

<asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>

</asp:Panel>
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr></table>
</td>
<%--WEEK DAY 5--%>
<td valign="top"><table><tr><th class="days"><asp:Label ID="dow5" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day5" runat="server" onclick="lbl_day5_Click" ></asp:LinkButton></span></th>
</tr>
<tr><td class="opas">
<asp:GridView ID="GridView_list5" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list5_RowDataBound" OnSelectedIndexChanged="GridView_list5_SelectedIndexChanged" GridLines="None" ShowHeader="False" >
<Columns>
<asp:TemplateField>
<ItemTemplate>
<asp:Panel ID="Panel_cell1"  CssClass="BookingItem" runat="server">
<asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' 
Visible="false"></asp:Label>
<asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' 
Visible="false"></asp:Label>                                                        
<asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>

</asp:Panel>
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr>
</table>
</td>
<%--WEEK DAY 6--%>
<td valign="top"><table><tr><th class="days"><asp:Label ID="dow6" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day6" runat="server" onclick="lbl_day6_Click" ></asp:LinkButton></span></th>
</tr>
<tr>
<td class="opas">
<asp:GridView ID="GridView_list6" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list6_RowDataBound" OnSelectedIndexChanged="GridView_list6_SelectedIndexChanged" GridLines="None" ShowHeader="False" >
<Columns>
<asp:TemplateField>
<ItemTemplate>
<asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
<asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' 
Visible="false"></asp:Label>
<asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' 
Visible="false"></asp:Label>                                                        
<asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>

</asp:Panel>

</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr>
</table>
</td>
<%--WEEK DAY 7--%>
<td valign="top"><table><tr><th class="days"><asp:Label ID="dow7" runat="server" CssClass="mcal-date-anchor" /><asp:LinkButton ID="lbl_day7" runat="server" onclick="lbl_day7_Click" ></asp:LinkButton></span></th>
</tr>
<tr><td class="opas">
<asp:GridView ID="GridView_list7" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_list7_RowDataBound" OnSelectedIndexChanged="GridView_list7_SelectedIndexChanged" GridLines="None" ShowHeader="False" >
<Columns>
<asp:TemplateField>
<ItemTemplate>
<asp:Panel ID="Panel_cell1" CssClass="BookingItem" runat="server">
<asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
<asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>                                                        
<asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
<br />
<asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
<asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
</asp:Panel>
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>
</td>
</tr>
</table>
</td>
</tr>
</table>
</asp:panel>

<%--DAY--%>
<asp:panel id="dayView" runat="server" Visible="false">
<table><tbody><tr><td valign="top"><table><tbody><td class="opas">
                            <asp:GridView ID="GridViewDay" runat="server" AutoGenerateColumns="false" GridLines="None" OnRowDataBound="GridViewDay_list_RowDataBound" OnSelectedIndexChanged="GridViewDay_list_SelectedIndexChanged" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Panel ID="Panel_cell1" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="BookingBoxes" style="width:20%">
                                                            <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                                                            <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                                                            <asp:Label ID="lbl_patientname" runat="server"><span class="pro-detail-title">Name:</span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_doctorname" runat="server"><span class="pro-detail-title">Doctor:</span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_procedurename" runat="server"><span class="pro-detail-title">Procedure:</span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_statusname" runat="server"><span class="pro-detail-title">Status:</span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_suppname" runat="server"><span class="pro-detail-title">Supplier:</span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_hospname" runat="server"><span class="pro-detail-title">Hospital:</span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                                                            <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"/>
                                                        </td>
                                                        <td class="BookingBoxes" style="width:20%">
                                                            <asp:Label ID="lbl_opTypeValue" runat="server"><span class="pro-detail-title">Operation Type:</span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_kitSideCode" runat="server"><span class="pro-detail-title">Kit Side:</span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_ptURno" runat="server"><span class="pro-detail-title">Patient UR:</span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'/></asp:Label><br />
                                                            <asp:Label ID="lbl_ptSexType" runat="server"><span class="pro-detail-title">Patient Sex:</span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'/></asp:Label>
                                                        </td>
                                                        <td class="BookingBoxes" style="width:20%">
                                                            <asp:Label ID="lbl_productsReqd" runat="server"><span class="pro-detail-title">Products:</span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lbl_caseTypeCode" runat="server"><span class="pro-detail-title">Case Type:</span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lbl_kitTypeValue" runat="server"><span class="pro-detail-title">Kit Type:</span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lbl_lastProcessedBy" runat="server"><span class="pro-detail-title">Last Processed By:</span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                                                        </td>
                                                        <td class="BookingBoxes" style="width:35%">
                                                            <asp:Label ID="lbl_revNotes" runat="server" style="width:150px"><span class="pro-detail-title">Notes:</span><asp:Label ID="lbl_Notes" runat="server" Text='<%#Eval("revNotes") %>'/></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
</td></tbody></table></td></tr></tbody></table>
</asp:panel> 

<%--MONTH--%>
<asp:Panel ID="monthView" runat="server" Visible="false" style="width:100%" CssClass="mcal" >
    <div class="mcal-header">
        <div class="weekday">Mon</div><div class="weekday">Tue</div><div class="weekday">Wed</div><div class="weekday">Thu</div><div class="weekday">Fri</div><div class="weekend">Sat</div><div class="weekend">Sun</div>
    </div>
<div class="mcal-week">
<%--MONTH DAY 1--%><asp:Panel ID="mcd1" runat="server" CssClass="mcal-day weekday">
    <asp:Label ID="dom1" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom1" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom1_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day1" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid" OnRowDataBound="gvm_Day1_RowDataBound" OnSelectedIndexChanged="gvm_Day1_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'/></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"/>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'/></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'/></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'/></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'/></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'/></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'/></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'/></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'/></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 2--%><asp:Panel ID="mcd2" runat="server" CssClass="mcal-day weekday">
    <asp:Label ID="dom2" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom2" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom2_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day2" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid" OnRowDataBound="gvm_Day2_RowDataBound" OnSelectedIndexChanged="gvm_Day2_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'/></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'/></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 3--%><asp:Panel ID="mcd3" runat="server" CssClass="mcal-day weekday">
    <asp:Label ID="dom3" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom3" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom3_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day3" runat="server" AutoGenerateColumns="false" CssClass="mcal-day-grid" GridLines="None" OnRowDataBound="gvm_Day3_RowDataBound" OnSelectedIndexChanged="gvm_Day3_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 4--%><asp:Panel ID="mcd4" runat="server" CssClass="mcal-day weekday">
    <asp:Label ID="dom4" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom4" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom4_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day4" runat="server" AutoGenerateColumns="false" CssClass="mcal-day-grid" GridLines="None" OnRowDataBound="gvm_Day4_RowDataBound" OnSelectedIndexChanged="gvm_Day4_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 5--%><asp:Panel ID="mcd5" runat="server" CssClass="mcal-day weekday">
    <asp:Label ID="dom5" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom5" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom5_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day5" runat="server" AutoGenerateColumns="false" CssClass="mcal-day-grid" GridLines="None" OnRowDataBound="gvm_Day5_RowDataBound" OnSelectedIndexChanged="gvm_Day5_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 6--%><asp:Panel ID="mcd6" runat="server" CssClass="mcal-day weekend">
    <asp:Label ID="dom6" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom6" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom6_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day6" runat="server" AutoGenerateColumns="false" CssClass="mcal-day-grid" GridLines="None" OnRowDataBound="gvm_Day6_RowDataBound" OnSelectedIndexChanged="gvm_Day6_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<%--MONTH DAY 7--%><asp:Panel ID="mcd7" runat="server" CssClass="mcal-day weekend">
    <asp:Label ID="dom7" runat="server" CssClass="mcal-date-anchor" />
    <asp:LinkButton ID="lbl_dom7" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom7_Click">dom</asp:LinkButton><br />
    <div class="mcal-day-minheight"></div>
    <asp:GridView ID="gvm_Day7" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day7_RowDataBound" OnSelectedIndexChanged="gvm_Day7_SelectedIndexChanged" ShowHeader="False">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Panel ID="Panel_cell1" runat="server">
                        <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                        &nbsp
                        <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label>
                        
                        <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                        <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                        <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
</div>
<div class="mcal-week">
<asp:Panel ID="mcd8" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom8" runat="server"  CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom8" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom8_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day8" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day8_RowDataBound" OnSelectedIndexChanged="gvm_Day8_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd9" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom9" runat="server"  CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom9" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom9_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day9" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day9_RowDataBound" OnSelectedIndexChanged="gvm_Day9_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd10" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom10" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom10" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom10_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day10" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day10_RowDataBound" OnSelectedIndexChanged="gvm_Day10_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd11" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom11" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom11" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom11_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day11" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day11_RowDataBound" OnSelectedIndexChanged="gvm_Day11_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd12" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom12" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom12" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom12_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day12" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day12_RowDataBound" OnSelectedIndexChanged="gvm_Day12_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd13" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom13" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom13" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom13_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day13" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day13_RowDataBound" OnSelectedIndexChanged="gvm_Day13_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'/></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'/></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'/></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'/></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd14" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom14" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom14" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom14_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day14" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day14_RowDataBound" OnSelectedIndexChanged="gvm_Day14_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
</div>
<div class="mcal-week">
<asp:Panel ID="mcd15" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom15" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom15" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom15_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day15" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day15_RowDataBound" OnSelectedIndexChanged="gvm_Day15_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd16" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom16" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom16" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom16_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day16" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day16_RowDataBound" OnSelectedIndexChanged="gvm_Day16_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd17" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom17" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom17" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom17_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day17" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day17_RowDataBound" OnSelectedIndexChanged="gvm_Day17_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd18" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom18" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom18" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom18_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day18" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day18_RowDataBound" OnSelectedIndexChanged="gvm_Day18_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd19" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom19" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom19" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom19_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day19" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day19_RowDataBound" OnSelectedIndexChanged="gvm_Day19_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd20" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom20" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom20" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom20_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day20" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid" OnRowDataBound="gvm_Day20_RowDataBound" OnSelectedIndexChanged="gvm_Day20_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd21" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom21" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom21" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom21_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day21" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day21_RowDataBound" OnSelectedIndexChanged="gvm_Day21_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
</div>
<div class="mcal-week">
<asp:Panel ID="mcd22" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom22" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom22" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom22_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day22" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day22_RowDataBound" OnSelectedIndexChanged="gvm_Day22_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd23" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom23" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom23" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom23_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day23" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day23_RowDataBound" OnSelectedIndexChanged="gvm_Day23_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'/></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'/></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>'/></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'/></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd24" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom24" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom24" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom24_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day24" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day24_RowDataBound" OnSelectedIndexChanged="gvm_Day24_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd25" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom25" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom25" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom25_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day25" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day25_RowDataBound" OnSelectedIndexChanged="gvm_Day25_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd26" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom26" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom26" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom26_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day26" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day26_RowDataBound" OnSelectedIndexChanged="gvm_Day26_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd27" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom27" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom27" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom27_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day27" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day27_RowDataBound" OnSelectedIndexChanged="gvm_Day27_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd28" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom28" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom28" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom28_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day28" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day28_RowDataBound" OnSelectedIndexChanged="gvm_Day28_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
</div>
<div class="mcal-week">
<asp:Panel ID="mcd29" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom29" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom29" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom29_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day29" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day29_RowDataBound" OnSelectedIndexChanged="gvm_Day29_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd30" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom30" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom30" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom30_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day30" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day30_RowDataBound" OnSelectedIndexChanged="gvm_Day30_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd31" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom31" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom31" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom31_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day31" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day31_RowDataBound" OnSelectedIndexChanged="gvm_Day31_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd32" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom32" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom32" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom32_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day32" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day32_RowDataBound" OnSelectedIndexChanged="gvm_Day32_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd33" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom33" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom33" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom33_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day33" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid" OnRowDataBound="gvm_Day33_RowDataBound" OnSelectedIndexChanged="gvm_Day33_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd34" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom34" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom34" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom34_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day34" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day34_RowDataBound" OnSelectedIndexChanged="gvm_Day34_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd35" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom35" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom35" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom35_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day35" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day35_RowDataBound" OnSelectedIndexChanged="gvm_Day35_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
</div>
<div class="mcal-week">
<asp:Panel ID="mcd36" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom36" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom36" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom36_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day36" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day36_RowDataBound" OnSelectedIndexChanged="gvm_Day36_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd37" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom37" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom37" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom37_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day37" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day37_RowDataBound" OnSelectedIndexChanged="gvm_Day37_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd38" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom38" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom38" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom38_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day38" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day38_RowDataBound" OnSelectedIndexChanged="gvm_Day38_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd39" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom39" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom39" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom39_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day39" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day39_RowDataBound" OnSelectedIndexChanged="gvm_Day39_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"/>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"/>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd40" runat="server" CssClass="mcal-day weekday">
<asp:Label ID="dom40" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom40" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom40_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day40" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid" OnRowDataBound="gvm_Day40_RowDataBound" OnSelectedIndexChanged="gvm_Day40_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd41" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom41" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom41" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom41_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day41" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day41_RowDataBound" OnSelectedIndexChanged="gvm_Day41_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
<asp:Panel ID="mcd42" runat="server" CssClass="mcal-day weekend">
<asp:Label ID="dom42" runat="server" CssClass="mcal-date-anchor" />
<asp:LinkButton ID="lbl_dom42" runat="server" CssClass="mcal-day-ofweek" onclick="lbl_dom42_Click">dom</asp:LinkButton><br />
<div class="mcal-day-minheight"></div>
<asp:GridView ID="gvm_Day42" runat="server" AutoGenerateColumns="false" GridLines="None" CssClass="mcal-day-grid"  OnRowDataBound="gvm_Day42_RowDataBound" OnSelectedIndexChanged="gvm_Day42_SelectedIndexChanged" ShowHeader="False">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:Panel ID="Panel_cell1" runat="server">
                    <asp:Label ID="lbl_opTimeValue" runat="server"><span class="pro-detail-title"></span><asp:Label ID="lbl_opTime" runat="server" Text='<%#Eval("revOperationTime") %>'></asp:Label></asp:Label>
                    &nbsp
                    <asp:Label ID="lbl_doctorname" runat="server" ><span class="pro-detail-title"></span><asp:Label ID="lbl_Doctor" runat="server" Text='<%#Eval("Doctor") %>'></asp:Label></asp:Label>
                    
                    <asp:Label ID="lbl_orderID" runat="server" Text='<%#Eval("ordOrgId") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_orderno" runat="server" Text='<%#Eval("OrdNO") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_patientname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_patient" runat="server" Text='<%#Eval("Patient") %>' ></asp:Label></asp:Label>
                    <asp:Label ID="lbl_procedurename" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Opname" runat="server" Text='<%#Eval("ordOperationName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_statusname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_status" runat="server" Text='<%#Eval("OrderStatus") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_suppname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_supplier" runat="server" Text='<%#Eval("SupName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_hospname" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_Hospital" runat="server" Text='<%#Eval("hospName") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_orderBookingnumber" runat="server" Text='<%#Eval("ordBookingNumber") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lbl_opTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_opType" runat="server" Text='<%#Eval("opType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitSideCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitSide" runat="server" Text='<%#Eval("kitSide") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptURno" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptUR" runat="server" Text='<%#Eval("ptUR") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_ptSexType" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ptSex" runat="server" Text='<%#Eval("ptSex") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_productsReqd" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_products" runat="server" Text='<%#Eval("revProductsReqd") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_caseTypeCode" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_caseType" runat="server" Text='<%#Eval("caseType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_kitTypeValue" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_kitType" runat="server" Text='<%#Eval("kitType") %>'></asp:Label></asp:Label>
                    <asp:Label ID="lbl_lastProcessedBy" runat="server" Visible="false"><span class="pro-detail-title"></span><asp:Label ID="lbl_ProcessedBy" runat="server" Text='<%#Eval("accName") %>'></asp:Label></asp:Label>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</asp:Panel>
</div>
</asp:Panel>

</div></div></div>
<div class="b"><div class="b"><div class="b"></div></div></div>
</div>
</div>

<%--Alert box is at the bottom--%>  
<%--<div style="width: 400px; height: 30px">--%>
<%--Allows the popup Warning to display over the top of the dropdown menues in IE6--%>
<asp:Panel ID="Panel_AdminAlertsIFrame" runat="server" CssClass="alertbox_iframe">
    <!--[if IE 6]><iframe frameborder="0" scrolling="no"></iframe><![endif]-->
</asp:Panel>
<asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender2" runat="server" TargetControlID="Panel_AdminAlertsIFrame" HorizontalSide="Center" VerticalSide="Middle"/>
<%--IE6--%>
<asp:Panel ID="Panel_AdminAlerts" runat="server" BackColor="white" Direction="LeftToRight" CssClass="AlertPanel">
    <p>There are &nbsp <asp:Label ID="lbl_alerts" runat="server" CssClass="Alertslbl"/> &nbsp User Requests Pending</p>
    <div align="center">
        <asp:Button ID="btn_Process" runat="server" Text="Process" OnClick="btn_Process_Click" />
        &nbsp
        <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click" />
    </div>
</asp:Panel>
<asp:RoundedCornersExtender ID="RoundedCornersExtender2" runat="server" TargetControlID="Panel_AdminAlerts" BorderColor="red"/>
<asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="Panel_AdminAlerts" HorizontalSide="Center" VerticalSide="Middle"/><%--VerticalOffset="35" HorizontalOffset="15"--%>
</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>