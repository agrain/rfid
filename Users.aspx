﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Homepage.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Users" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:ToolkitScriptManager runat="server"/>
    
<asp:Panel ID="Panel_NewUser" runat="server">
    <div id="content-box">
	    <div class="padding">
		    <div class="clr"></div>
		    <div id="new-account-box" class="login">
			    <div id="border-top" class="login">
                    <div>
                        <div >
                            <h1>Create New Account</h1>
                        </div>
                    </div>
                </div>
			    <div class="m wbg">
				    <div id="account-field-wrapper">
                        <fieldset class="loginform">
                            <table>
                                <tr>
                                    <td><b>First Name:</b></td>
                                    <td><asp:TextBox ID="txt_Firstname" runat="server"/></td>
                                </tr>
                                <tr>
                                    <td><b>Last Name:</b></td>
                                    <td><asp:TextBox ID="txt_Lastname" runat="server"/></td>
                                </tr>
                                <tr>
                                    <td><b>Email:</b></td>
                                    <td><asp:TextBox ID="txt_Username" runat="server" ValidationGroup="validuser" Width="295px"/>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_Username" Display="None" 
                                            ErrorMessage="Username should be in the following format username@xx.xxx" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="validuser"/>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="txt_Username" ErrorMessage="Username is Mandatory" ValidationGroup="validuser"/>
                                    </td>
                                </tr>
                            </table>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="validuser" />
                            <div class="clr"></div>
                        </fieldset>
                    </div> 
                
                    <div id="account-description"><p>Please enter details below for either a Hospital Based Access, or Loan Kit Supplier </p></div>
                    <div class="width-45 fltlft">
                        <h3> Hospital Based Access</h3>
                        <fieldset>
	                        <ul class="creatacclist">
                                <li><h4>Role(s):</h4></li>
                                <li>
                                    <asp:CheckBoxList ID="chk_Roles" runat="server" AutoPostBack="True" CssClass="createacchkbox" OnSelectedIndexChanged="chk_Roles_SelectedIndexChanged">                            
                                        <asp:ListItem>Or</asp:ListItem>
                                        <asp:ListItem>Process Loan Kit in and out</asp:ListItem>
                                        <asp:ListItem>Financial Recording</asp:ListItem>
                                    </asp:CheckBoxList>
                                    <h4>Hospital/Campus:</h4>
                                    <asp:DropDownList ID="ddl_hospital" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddl_hospital_SelectedIndexChanged">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </li>
	                        </ul>	
                        </fieldset>
                    </div>
                    <div>
                        <%--<div class="width-45-inactive fltrt">--%>
                        <div class="width-45 fltlft">
                            <div class="pane-slider content">		
                                <%-- <h3 class="inactive"> Loan Kit Supplier</h3>--%>
                                <h3> Loan Kit Supplier</h3>
                                <fieldset>
                                    <ul class="adminformlist">
                                        <li>
                                            <h4>Supplier</h4>
                                            <asp:DropDownList ID="ddl_Supplier" runat="server" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddl_Supplier_SelectedIndexChanged">
                                                <asp:ListItem></asp:ListItem>
                                            </asp:DropDownList>
                                        </li>
                                    </ul>
                                </fieldset>			
                            </div>
	                    </div>
                    </div>
				    <div class="clr"></div>           		
                </div>
                <div>
                    <div>
                        <asp:linkbutton ID="Imgbtn_Createaccount" runat="server" onclick="Imgbtn_Createaccount_Click" ValidationGroup="validuser" class="button-dynamic FloatRight"><div id="L"></div><div id="C">&nbsp; CREATE ACCOUNT &nbsp;</div><div id="R"></div></asp:linkbutton>
                        <asp:Linkbutton ID="Imgbtn_Cancel" runat="server" class="button-dynamic FloatRight" onclick="Imgbtn_Cancel_Click"><div id="Lg"></div><div id="Cg">&nbsp; CANCEL &nbsp;</div><div id="Rg"></div></asp:Linkbutton>
                    </div>
                </div>
            </div>
        </div>
     </div>  
     
</asp:Panel>

<asp:Panel ID="Panel_gridrequests" runat="server">
    <div id="div_listrequests" align="left" runat="server">
        <div class="clr"></div>
		<div id="submenu-box">
		    <div class="t wbg"><div class="t"><div class="t"></div></div></div>
	        <div class="user-profile-header">
                <div class="user-toolbar width-400" id="toolbar">             
                    <asp:LinkButton ID="lnkbtn_deny" runat="server" onclick="lnkbtn_deny_Click"><span class="icon-32-deny"></span>Deny</asp:LinkButton>
                    <asp:LinkButton ID="lnkbtn_Approve" runat="server" onclick="lnkbtn_Approve_Click"><span class="icon-32-publish"></span>Enable</asp:LinkButton>
                    <asp:CheckBox ID="Chk_addadmin" runat="server" AutoPostBack="True" CssClass="adminchkbox" OnCheckedChanged="Chk_addadmin_CheckedChanged" Text="Add as Role Administrator" />
                    <div class="clr"></div>
                </div>
                <h2>User Requests Pending</h2>
                <div class="clr"></div>
                <div class="border">
	                <div class="padding">
		                <div >
                            <div id="element-box">
                                <div class="t"><div class="t"><div class="t"></div></div></div>
		                        <div class="m">
                                    <div class="clr"> </div>
                                    <asp:GridView ID="Grid_requests" runat="server" AutoGenerateColumns="false" OnRowDataBound="Grid_requests_RowDataBound" RowStyle-CssClass="row0" 
                                    BorderColor="White" BorderStyle="None" AlternatingRowStyle-CssClass="row1" CssClass="adminlist">
                                        <Columns>
                                            <asp:BoundField DataField="reqid" HeaderText="ReqID"/>
                                            <asp:BoundField DataField="Requestor" HeaderText="Requestor" />
                                            <asp:TemplateField HeaderText="Select">
                                                <ItemTemplate><asp:CheckBox ID="chk_Requests" runat="server" /></ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="RequestorName" HeaderText="Name"/>
                                            <asp:BoundField DataField="Supplier" HeaderText="Supplier"/>
                                            <asp:BoundField DataField="Hospital" HeaderText="Hospital"/>
                                            <asp:BoundField DataField="Role" HeaderText="Role" />
                                            <asp:BoundField DataField="reqDttm" HeaderText="Date" DataFormatString="{0:d/MMM hh:mm}" HtmlEncode="false" />
                                        </Columns>
                                    </asp:GridView>
                                    <%--<asp:Button ID="btn_Approve" runat="server" OnClick="btn_Approve_Click" Style="margin-left: 0px" Text="Approve Request" CssClass="btnAdmin" />--%>
                                    <%--<asp:Button ID="btn_deny" runat="server" OnClick="btn_deny_Click" Style="margin-left: 0px" Text="Deny Request" CssClass="btnAdmin" />--%>
                                    <em><asp:Label ID="lbl_summary" runat="server"/></em>
                                    <div class="clr"></div>
		                        </div>
		                        <div class="b"><div class="b"><div class="b"></div></div></div>
	                        </div>
                        </div>
		            </div>
	            </div>
                <div class="clr"></div>
            </div>
            <div class="b wbg"><div class="b"><div class="b"></div></div></div>
        </div>
        <div id="Div1">
	        <div class="t wbg"><div class="t"><div class="t"></div></div></div>
        </div>
    </div>
    <div id="div_Listusers" runat="server">
        <div id="submenu-box">
            <div class="t wbg"><div class="t"><div class="t"></div></div></div>
            <div class="order-deats">
                <div class="user-toolbar" id="Div3">             
                    <asp:LinkButton ID="lnkbtn_disable" runat="server" onclick="lnkbtn_disable_Click"><span class="icon-32-deny"></span>Disable</asp:LinkButton>
                    <asp:LinkButton ID="lnkbtn_enable" runat="server" onclick="lnkbtn_enable_Click"><span class="icon-32-publish"></span>Enable</asp:LinkButton>
                    <%--<asp:LinkButton ID="lnkbtn_edit" runat="server"><span class="icon-32-user-edit"></span>Edit</asp:LinkButton>--%>
                    <%--<asp:LinkButton ID="lnkbtn_Delete" runat="server"><span class="icon-32-delete"></span>Delete</asp:LinkButton>--%>
                    <div class="clr"></div>
                </div>
                <h2>User Profile List</h2>				
                <div class="clr"></div>
                <div class="border">
                    <div class="padding">
                        <div >
                            <div id="Div4">
                                <div class="t"><div class="t"><div class="t"></div></div></div>
                                <div class="m">
                                    <div class="clr"></div>
                                    <asp:GridView ID="Grd_ListUsers" runat="server" AutoGenerateColumns="false" CssClass="adminlist"  BorderColor="White" BorderStyle="None" GridLines="Both" RowStyle-CssClass="row0" AlternatingRowStyle-CssClass="row1">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate><asp:CheckBox ID="chk_listusers" runat="server" /></ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="accID" HeaderText="ID" />
                                            <asp:BoundField DataField="accUsername" HeaderText="Username" />
                                            <asp:BoundField DataField="hospname" HeaderText="Hospital" />
                                            <asp:BoundField DataField="hospcampus" HeaderText="Campus" />
                                            <asp:BoundField DataField="arSuppWhsId" HeaderText="Supplier" />
                                            <%-- <asp:BoundField DataField="arRoleID" HeaderText="RoleID" />--%>
                                            <asp:BoundField DataField="roleName" HeaderText="Role" />
                                            <asp:BoundField DataField="aradmin" HeaderText="Admin" />
                                            <asp:BoundField DataField="arInactive" HeaderText="InActive" />
                                        </Columns>
                                    </asp:GridView>
                                    <%--<asp:Button ID="btn_Delete" runat="server" Text="Delete" onclick="btn_Delete_Click" CssClass="btnAdmin" />--%>
                                    <div class="clr"></div>
                                </div>
                                <div class="b"><div class="b"><div class="b"></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            <div class="b wbg"><div class="b"><div class="b"></div></div></div>
        </div>
    </div>
</asp:Panel>
</asp:Content>