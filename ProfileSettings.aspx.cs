﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ProfileSettings : System.Web.UI.Page
{
    RFIDDataContext Orfid = new RFIDDataContext();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.ServerVariables["http_referer"] != null)
        {
            try
            {
                if (Page.IsPostBack == false)
                {
                    var Orole = from Role in Orfid.tblImplantRoles
                                select new { RoleID = Role.roleId, Rolename = Role.roleName };
                    Chk_Roles.DataSource = Orole;
                    Chk_Roles.DataValueField = "RoleID";
                    Chk_Roles.DataTextField = "Rolename";
                    Chk_Roles.DataBind();

                    var oHospital = from hosp in Orfid.tblImplantHospitals
                                    select new
                                    {
                                        HospitalID = hosp.hospId,
                                        Hospitalname = hosp.hospName + "," + hosp.hospCampus
                                    };
                    ddl_Hospital.DataSource = oHospital;
                    ddl_Hospital.DataValueField = "HospitalID";
                    ddl_Hospital.DataTextField = "Hospitalname";
                    ddl_Hospital.DataBind();
                }
            }
            catch (Exception expageload)
            {
                Orfid.Usp_WebErrors("ProfileSettings.aspx", "Page_Load", DateTime.Now.AddDays(0), expageload.Message.ToString());
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    protected void Imgbtn_pwd_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Session["AccID"] != null)
            {
                if (txt_NewPwd.Text == txt_Cpwd.Text)
                {
                    Orfid.rp_implantAccountChangePassword(int.Parse(Session["AccID"].ToString()), txt_Pwd.Text, txt_NewPwd.Text);
                    lbl_PwdSummary.Text = "Password has been changed Successfully";
                }
                else
                {
                    return;
                }
            }
        }
        catch (Exception EImgbtn_Pwd)
        {
            Orfid.Usp_WebErrors("ProfileSettings.aspx", "Imgbtn_pwd_Click", DateTime.Now.AddDays(0), EImgbtn_Pwd.Message.ToString());
        }
    }

    protected void Imgbtn_profile_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            var Ins = from tblAccounts in Orfid.tblImplantAccounts
                      where tblAccounts.accId == int.Parse(Session["AccID"].ToString())
                      select tblAccounts;
            foreach (var UpIns in Ins)
            {
                if (txt_Firstname.Text != "" || txt_Lastname.Text != "")
                    UpIns.accName = txt_Firstname.Text + " " + txt_Lastname.Text;
                if (txt_Username.Text != "")
                    UpIns.accUsername = txt_Username.Text;
            }

            Orfid.SubmitChanges();
            if (txt_Firstname.Text != "" || txt_Lastname.Text != "" || txt_Username.Text != "")
                lbl_confirmprofile.Text = "Profile has been Updated Successfully";
            else
                lbl_confirmprofile.Text = "Enter the Field to be changed";

            txt_Firstname.Text = "";
            txt_Lastname.Text = "";
            txt_Username.Text = "";
            
        }
        catch (Exception ex_Imgbtnprofile)
        {
            Orfid.Usp_WebErrors("ProfileSettings.aspx", "Imgbtn_profile_Click", DateTime.Now.AddDays(0), ex_Imgbtnprofile.Message.ToString()); 
        }
    }
    protected void Imgbtn_rolesrequest_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            System.Nullable<int> intHospitalID;
            if (ddl_Hospital.SelectedValue.ToString() != "")
                intHospitalID = int.Parse(ddl_Hospital.SelectedValue.ToString());
            else
                intHospitalID = int.Parse(Session["HospID"].ToString());
            Chk_Roles.Enabled = true;
            foreach (ListItem chk_li in Chk_Roles.Items)
            {
                if (chk_li.Selected == true)
                {
                    int RoleID = int.Parse(chk_li.Value.ToString());
                    var RequestAccess = Orfid.rp_implantRequestAccess(int.Parse(Session["AccID"].ToString())
                      , null, intHospitalID, RoleID);
                    Orfid.SubmitChanges();
                }
            }
        }
        catch (Exception ex_Imgbtnrolerequest)
        {
            Orfid.Usp_WebErrors("ProfileSettings.aspx", "Imgbtn_rolesrequest_Click", DateTime.Now.AddDays(0), ex_Imgbtnrolerequest.Message.ToString()); 
        }


    }
}
