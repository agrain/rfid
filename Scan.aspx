<%@ Page Title="" Language="C#" MasterPageFile="~/Homepage.master" AutoEventWireup="true" CodeFile="Scan.aspx.cs" Inherits="Scan" EnableEventValidation="false" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

<script type="text/javascript">
    //SCROLLING FIX
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(beginRequest);

    function beginRequest() { prm._scrollPosition = null; }

    //    function Onscrollfnction() {
    //        var div = document.getElementById('divmt');
    //        var div2 = document.getElementById('divhead');
    //        //****** Scrolling divhead along with divmt ******
    //        div2.scrollLeft = div.scrollLeft;
    //        return false;
    //    }

    //    function CreateGridHeader(divmt, gv_Scan, divhead) {
    //        var DataDivObj = document.getElementById(divmt);
    //        var DataGridObj = document.getElementById(gv_Scan);
    //        var HeaderDivObj = document.getElementById(divhead);
    //        //********* Creating new table which contains the header row ***********
    //        var HeadertableObj = HeaderDivObj.appendChild(document.createElement('table'));
    //        DataDivObj.style.paddingTop = '0px';
    //        var DataDivWidth = DataDivObj.clientWidth;
    //        DataDivObj.style.width = '5000px';
    //        //********** Setting the style of Header Div as per the Data Div ************
    //        HeaderDivObj.className = DataDivObj.className;
    //        HeaderDivObj.style.cssText = DataDivObj.style.cssText;
    //        //**** Making the Header Div scrollable. *****
    //        HeaderDivObj.style.overflow = 'auto';
    //        //*** Hiding the horizontal scroll bar of Header Div ****
    //        //*** this is because we have to scroll the Div along with the divmt.  
    //        HeaderDivObj.style.overflowX = 'hidden';
    //        //**** Hiding the vertical scroll bar of Header Div **** 
    //        HeaderDivObj.style.overflowY = 'hidden';
    //        HeaderDivObj.style.height = DataGridObj.rows[0].clientHeight + 'px';
    //        //**** Removing any border between Header Div and Data Div ****
    //        HeaderDivObj.style.borderBottomWidth = '0px';
    //        //********** Setting the style of Header Table as per the GridView ************
    //        HeadertableObj.className = DataGridObj.className;
    //        //**** Setting the Headertable css text as per the GridView css text 
    //        HeadertableObj.style.cssText = DataGridObj.style.cssText;
    //        HeadertableObj.border = '1px';
    //        HeadertableObj.rules = 'all';
    //        HeadertableObj.cellPadding = DataGridObj.cellPadding;
    //        HeadertableObj.cellSpacing = DataGridObj.cellSpacing;
    //        //********** Creating the new header row **********
    //        var Row = HeadertableObj.insertRow(0);
    //        Row.className = DataGridObj.rows[0].className;
    //        Row.style.cssText = DataGridObj.rows[0].style.cssText;
    //        Row.style.fontWeight = 'bold';
    //        //******** This loop will create each header cell *********
    //        for (var iCntr = 0; iCntr < DataGridObj.rows[0].cells.length; iCntr++) {
    //            var spanTag = Row.appendChild(document.createElement('td'));
    //            spanTag.innerHTML = DataGridObj.rows[0].cells[iCntr].innerHTML;
    //            var width = 0;
    //            //****** Setting the width of Header Cell **********
    //            if (spanTag.clientWidth > DataGridObj.rows[1].cells[iCntr].clientWidth) {
    //                width = spanTag.clientWidth;
    //            }
    //            else {
    //                width = DataGridObj.rows[1].cells[iCntr].clientWidth;
    //            }
    //            if (iCntr <= DataGridObj.rows[0].cells.length - 2) {
    //                spanTag.style.width = width + 'px';
    //            }
    //            else {
    //                spanTag.style.width = width + 20 + 'px';
    //            }
    //            DataGridObj.rows[1].cells[iCntr].style.width = width + 'px';
    //        }
    //        var tableWidth = DataGridObj.clientWidth;
    //        //********* Hidding the original header of GridView *******
    //        DataGridObj.rows[0].style.display = 'none';
    //        //********* Setting the same width of all the components **********
    //        HeaderDivObj.style.width = DataDivWidth + 'px';
    //        DataDivObj.style.width = DataDivWidth + 'px';
    //        DataGridObj.style.width = tableWidth + 'px';
    //        HeadertableObj.style.width = tableWidth + 20 + 'px';
    //        return false;
    //    }   

</script>
<asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"/>

<%--TOOLBAR PANEL--%>
<div class="border">
	<div class="padding">
		<div id="toolbar-box">
		<div class="t"><div class="t"><div class="t"></div></div></div>
	    <div class="m">
	        <!--[if IE 7]><table class="HiddenSpacing"><tr><td><![endif]-->
	        <!--[if IE 6]><table class="HiddenSpacing"><tr><td><![endif]-->
	        <div class="toolbar-list" id="toolbar" runat="server">
                <ul>
                    <li class="button" id="toolbar-scan" style="float:left; " >
                        <asp:LinkButton ID="lnkbtn_STP_ScanIn" runat="server" onclick="lnkbtn_STP_ScanIn_Click"> <span id="spanactive" class="icon-32-upload-active2" runat="server"></span><span id="Spaninactive" class="icon-32-upload" runat="server"></span>Scan In</asp:LinkButton>
                    </li>       
                    <li class="button" id="toolbar-usage" style="float:left;">
                        <asp:LinkButton ID="lnkbtn_STP_Usage" runat="server" onclick="lnkbtn_STP_Usage_Click"> <span class="icon-32-Usage" id="spanusageinactive" runat="server"></span><span class="icon-32-usage-active" id="spanusageactive" runat="server"></span>Usage</asp:LinkButton>
                    </li>
                    <li class="button" id="toolbar-edit" style="float:left">
                        <asp:LinkButton ID="lnkbtn_STP_Dispatch" runat="server" onclick="lnkbtn_STP_Dispatch_Click"> <span class="icon-32-dispatch" id="Spandisinactive" runat="server"></span><span class="icon-32-dispatch-active" id="SpandisActive" runat="server"></span>Dispatch</asp:LinkButton>
                    </li>
                    <li class="button" id="toolbar-checkin" style="float:left;">
                        <asp:LinkButton ID="lnkbtn_STP_Reconcile" runat="server" onclick="lnkbtn_STP_Reconcile_Click"><span class="icon-32-checkin"></span>Reconcile</asp:LinkButton>
                    </li>
                    <li class="button" id="toolbar-shipped" style="float:left">
                        <asp:LinkButton ID="lnkbtn_STP_Shipped" runat="server" onclick="lnkbtn_STP_Shipped_Click"> <span class="icon-32-shipping"></span>Ship</asp:LinkButton>
                        <%--<asp:ConfirmButtonExtender ID="Confirmbtnshipping" runat="server" TargetControlID="lnkbtn_STP_Shipped" Enabled="true" ConfirmText="Do you want the items to be shipped"/>--%>
                    </li>
                    <li>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <Triggers><asp:AsyncPostBackTrigger ControlID="Timer_gridscan" /></Triggers>
                        <ContentTemplate>
                            <ul>
                                <li class="button" id="toolbar-publish" style="float:left">  
                                    <asp:LinkButton ID="lnkbtn_STP_Undo" runat="server" onclick="lnkbtn_STP_Undo_Click"><span class="icon-32-undo"></span>Undo</asp:LinkButton>
                                </li>
                                <li class="button" id="toolbar-featured" style="float:left">
                                    <asp:LinkButton ID="lnkbtn_STP_Save" runat="server" onclick="lnkbtn_STP_Save_Click"><span class="icon-32-save-new"></span>Save</asp:LinkButton>
                                </li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </li>
                    <li class="button" id="toolbar-archive" style="float:left">
                            <asp:LinkButton ID="lnkbtn_STP_Consumption" runat="server" onclick="lnkbtn_STP_Consumption_Click"><span class="icon-32-banner-client"></span>Consumption</asp:LinkButton> 
                    </li> 
<%--                                        <li class="button" id="toolbar-archive" style="float:left">
                            <asp:LinkButton ID="lnkbtn_STP_Print" runat="server" onclick="lnkbtn_STP_Print_Click"><span class="icon-32-print"></span>Print Consumption</asp:LinkButton>    
                    </li> --%>

                    <li class="button" id="Li1" style="float:left">
                            <asp:LinkButton ID="lnkbtn_STP_AddNewItem" OnClick="lnkbtn_STP_AddNewItem_Click" runat="server"><span class="icon-32-new"></span>Add New Item</asp:LinkButton>
                    </li>
                </ul>

                <div class="clr"></div>
            </div>
	        <!--[if IE 7]></td><td><![endif]-->
	        <!--[if IE 6]></td><td><![endif]-->
	        <div style="float:right; padding:10px ">
	            <%--<asp:LinkButton ID="lnkbtn_reader" runat="server" onclick="ClearReader" >--%>
	            <div class="icon-32-reader" style="width:32px; height:32px; float:right"></div>
	            <%--</asp:LinkButton> --%>
	            <div style="float:right; padding:6px"><asp:Label ID="lbl_Reader" runat="server" Text="Reader" Font-Bold="True" /></div>
	        </div> 
	        <!--[if IE 7]></td></tr></table><![endif]-->	
	        <!--[if IE 6]></td></tr></table><![endif]-->	
            <div class="clr"></div>
	    </div>
	    <div class="b"><div class="b"><div class="b"></div></div></div>
	    </div>
	</div>
</div>
<div class="clr"></div>
	
<%--BOOKING DETAILS PANEL--%>
<div id="submenu-box">
    <div class="t wbg"><div class="t"><div class="t"></div></div></div>
    <div class="order-deats">
        <asp:Label ID="lbl_PatientUR" runat="server" Visible="false" />
        <asp:Label ID="lbl_PatientType" runat="server" Visible="false" />
        <asp:Label ID="lbl_HospitalName" runat="server" Visible="false" />
        <asp:Label ID="lbl_CaseType" runat="server" Visible="false" />
        <ul id="submenu">
            <li><strong>Procedure ID:</strong><asp:Label ID="lbl_ProcedureID" runat="server"/>	</li>
            <li><strong>Surgeon:</strong><asp:Label ID="lbl_surgeon" runat="server"/></li>
            <li><strong>Status:</strong><asp:Label ID="lbl_Status" runat="server"/></li>
            <li><strong>Patient:</strong><asp:Label ID="lbl_Patient" runat="server"/></li>
		    <li><strong>Date:</strong><asp:Label ID="lbl_date" runat="server"/></li>
            <li><strong>Loan Kit Supplier:</strong><asp:Label ID="lbl_loankit" runat="server"/></li>
            <li><strong>Procedure:</strong><asp:Label ID="lbl_procedure" runat="server"/></li>
            <li><strong>Supplier Ref:</strong><asp:Label ID="lbl_bookingnumber" runat="server"/></li>
        </ul>				
        <div class="clr"></div>
    </div>
	<div class="b wbg"><div class="b"><div class="b"></div></div></div>
</div>

<%--SCAN GRIDVIEW--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">    
    <Triggers><asp:AsyncPostBackTrigger ControlID="Timer_gridscan" /></Triggers>
    <ContentTemplate>
        <div id="element-box" class="scan">
            <div class="t"><div class="t"><div class="t"></div></div></div>
            <div id="divhead" runat="server"></div>
            <div class="mt" id="divmt" runat="server">
                <asp:GridView ID="gv_Scan" runat="server" AutoGenerateColumns="false" BorderColor="White" BorderStyle="None" onselectedindexchanged="gv_Scan_SelectedIndexChanged"  
                onrowdatabound="gv_Scan_RowDataBound1" onselectedindexchanging="gv_Scan_SelectedIndexChanging"  CssClass="adminlist" RowStyle-CssClass="row0" AlternatingRowStyle-CssClass="row1">
                    <Columns>            
                        <asp:BoundField DataField="sortOrder" HeaderText="No" />
                        <asp:BoundField DataField="linpartno" HeaderText="Part No" />
                        <asp:BoundField DataField="linpartDesc" HeaderText="Description" HeaderStyle-Wrap="false" />
                        <asp:BoundField DataField="linlotno" HeaderText="Lot No" />
                        <asp:BoundField DataField="linexpiry" HeaderText="Expiry" DataFormatString="{0:yyyy-MM}" HtmlEncode="false"/>
                        <asp:BoundField DataField="linrfid" HeaderText="Tag No"/>
                        <asp:BoundField DataField="asnQty" HeaderText="ASNQty" />
                        <asp:BoundField DataField="asnQtyBO" HeaderText="BackOrder" />
                        <asp:BoundField DataField="asnDttm" HeaderText="SupplierASN" DataFormatString="{0:d/MMM HH:mm}" HtmlEncode="false" />
                        <asp:BoundField DataField="receiptdttm" HeaderText="Received" DataFormatString="{0:d/MMM HH:mm}" HtmlEncode="false" />
                        <asp:BoundField DataField="UsedDttm" HeaderText="Used" DataFormatString="{0:d/MMM HH:mm}" HtmlEncode="false" />
                        <asp:BoundField DataField="dispatchdttm" HeaderText="Dispatched" DataFormatString="{0:d/MMM HH:mm}" HtmlEncode="false" />
                        <asp:BoundField DataField="binrfid" HeaderText="BinTag" />
                        <asp:BoundField DataField="lincomment" HeaderText="Comment" />
                        <asp:BoundField DataField="iAsn" HeaderText="ASN NO" />
                        <asp:BoundField DataField="iScan" HeaderText="Scanno" />
                        <asp:BoundField DataField="iDispatch" HeaderText="Dispatched" />
                        <asp:BoundField DataField="cExpiry" HeaderText="cExpiry" />
                        <asp:BoundField DataField="cAsn" HeaderText="cAsn" />
                        <asp:BoundField DataField="cReceipt" HeaderText="cReceipt" />
                        <asp:BoundField DataField="cUsage" HeaderText="cUsage" />
                        <asp:BoundField DataField="cDispatch" HeaderText="cDispatch" />
                        <asp:BoundField DataField="linNo" HeaderText="LinNo" />
                        <asp:BoundField DataField="orderLineNo" HeaderText="orderLineNo" />
                        <asp:BoundField DataField="sortOrder" HeaderText="sortOrder" />
                    </Columns>
                </asp:GridView>
	            <div class="clr"></div>
		    </div>
		    <div class="b"><div class="b"><div class="b"></div></div></div>
	    </div>
	    <div class="clr"></div>
    </ContentTemplate> 
</asp:UpdatePanel>

<%--CONSUMPTION PANEL--%>
<asp:Panel ID="Panel_consumption" runat="server" CssClass="ScanConsumptionReport" >
    <div id="border-top-bar" class="bar">
            <div class="user-toolbar" id="Div1">
                <asp:LinkButton ID="lnkbtn_CR_Cancel" runat="server" onclick="lnkbtn_CR_Cancel_Click"><span class="icon-32-deny"></span>Close</asp:LinkButton> 
                <asp:LinkButton ID="lnkbtn_CR_Print" runat="server" onclick="lnkbtn_CR_Print_Click"><span class="icon-32-print"></span>Print</asp:LinkButton>    
                <asp:UpdatePanel ID="udp_CR_Save" runat="server"><ContentTemplate>
                    <asp:LinkButton ID="lnkbtn_CR_Save" runat="server" onclick="lnkbtn_CR_Save_Click"><span class="icon-32-save-new"></span>Save</asp:LinkButton>
                </ContentTemplate></asp:UpdatePanel>
                <asp:LinkButton ID="lnkbtn_CR_Finalize" runat="server" onclick="lnkbtn_CR_Finalize_Click"><span class="icon-32-banner-client"></span>Consumption Report</asp:LinkButton>
            </div> 
            <h1>Consumption Report</h1> 
    </div>   
    
    <asp:UpdatePanel ID="udp_AddPart" runat="server" UpdateMode="Conditional"><ContentTemplate>
    <asp:GridView ID="GridView_consumption" runat="server" AutoGenerateColumns="false" CssClass="adminlist" RowStyle-CssClass="row0" AlternatingRowStyle-CssClass="row1" 
    onrowdatabound="GridView_consumption_RowDataBound" AllowPaging="True" onpageindexchanged="GridView_consumption_PageIndexChanged"
    onpageindexchanging="GridView_consumption_PageIndexChanging" PageSize="10">
        <Columns>
            <asp:BoundField DataField="asnLineNo" HeaderText="Sno" />
            <asp:BoundField DataField="conLineNo" HeaderText="lno" />
            <asp:BoundField DataField="linPartNo" HeaderText="PartNo" />
            <asp:BoundField DataField="linPartDesc" HeaderText="PartDesc" />
            <asp:BoundField DataField="conStatus" HeaderText="conStatus"/>
            <asp:BoundField DataField="conFMISCode" HeaderText="FMISCode"/> 
            <asp:BoundField DataField="conComment" HeaderText="comment"/>             
            <asp:BoundField DataField="asnLotNo" HeaderText="LotNo"/>   
            <asp:BoundField DataField="asnExpiry" HeaderText="Expiry"/>   
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:DropDownList ID="ddl_consumption_status" runat="server"/>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="FMIS Code">
                <ItemTemplate>
                     <asp:TextBox ID="txt_fmisCode" runat="server" TextMode="MultiLine" Width="100px"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Comments">
                <ItemTemplate><asp:TextBox ID="txt_consumption_comments" runat="server" TextMode="MultiLine"/></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>      
    </ContentTemplate></asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtender_Consumption" BackgroundCssClass="modalBackground"  PopupControlID="Panel_consumption" TargetControlID="lnkbtn_STP_Consumption" CancelControlID="lnkbtn_CR_Cancel" runat="server" />

<%--VIEW ITEM DETAILS--%>
<asp:Panel ID="Panel_viewitem" runat="server" CssClass="PopupPanelBorder" >
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div id="element-box" class="requests">
                <div id="border-top-requests" class="requests">
                    <div>
                        <div >
                            <div class="user-toolbar width-400" id="Div2">  
                                    <asp:LinkButton ID="lnkbtn_VPD_ViewCancel" runat="server" onclick="lnkbtn_VPD_ViewCancel_Click"><span class="icon-32-deny"></span>Close</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_Save" runat="server" Enabled="true" onclick="lnkbtn_VPD_Save_Click"><span class="icon-32-save"></span>Save</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_Delete" runat="server" Enabled="true" onclick="lnkbtn_VPD_Delete_Click"><span class="icon-32-trash"></span>Delete</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualUndispatch" Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualUndispatch_Click"><span class="icon-32-stats"></span>Un-Dispatch</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualDispatch"  Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualDispatch_Click"><span class="icon-32-dispatch"></span>Dispatch</asp:LinkButton><%--CssClass="disabled" --%>
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualItemUnused"  Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualItemUnused_Click" Visible="False"><span class="icon-32-UnUsage"></span>Un-Used</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualItemUsed"  Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualItemUsed_Click"><span class="icon-32-Usage"></span>Used</asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualItemUnreceipted" Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualItemUnreceipted_Click" Visible="False"><span class="icon-32-download"></span>Un-Receipt</asp:LinkButton>                                                                                                                                                                                    
                                    <asp:LinkButton ID="lnkbtn_VPD_ManualItemReceipted" Enabled="true" runat="server" onclick="lnkbtn_VPD_ManualItemReceipted_Click" Visible="False"><span class="icon-32-upload"></span>Receipted</asp:LinkButton>
                            <div class="clr"></div>
                            </div>
                            <h1>View Part Details</h1>
                        </div>
                    </div>
                </div>
	            <div>
	                <div class="part-details">
                        <ul id="part-details">
                            <li><strong>Part Number:</strong><asp:TextBox ID="txt_partno" runat="server"  Enabled="false" CssClass="MargLeft5"/>&nbsp;</li>
                            <li><strong>Lot No:</strong><asp:TextBox ID="txt_lotno" runat="server"  Enabled="false" CssClass="MargLeft5"/>&nbsp;</li>
                            <li><strong>Expiry Date: </strong><asp:TextBox ID="VPD_txt_expiry" runat="server"  Enabled="false" CssClass="MargLeft5"/>&nbsp;<asp:CalendarExtender ID="VPD_CalendarExtender" runat="server" TargetControlID="VPD_txt_expiry" Format="dd/MMM/yyyy" EnableViewState="false"/></li>
                            <li><strong>Description:</strong><asp:TextBox ID="txt_desc" runat="server" Width="307px"  Enabled="false" CssClass="MargLeft5"/>&nbsp;</li>
		                    <li><strong>RFID Tag:</strong><asp:TextBox ID="txt_tag0" runat="server"  Enabled="false" CssClass="MargLeft5"/>&nbsp;</li>
		                    <li><strong>Comment:</strong><asp:TextBox ID="VPD_txt_ExistingComment" runat="server"  Width="650px" TextMode="MultiLine" Enabled="false"  CssClass="MinusBottomMargin"/></li>
		                    <li><asp:TextBox ID="VPD_txt_YourComment" runat="server"  Width="650px" TextMode="MultiLine" CssClass="MinusTopMargin"/></li>
                        </ul>				
	                    <div class="clr"></div>
                    </div>
	            </div>
                <div class="clr"></div>
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" >
                
    <%--ASN TAB--%>
                    <asp:TabPanel runat="server" ID="Tab_Asn">
                        <HeaderTemplate>   ASN  </HeaderTemplate>
                        <ContentTemplate>
                            <table width="708px">
                                <tr>
                                    <td  width="60%" >
                                        <table width="60%">
                                            <tr><td class="lbl_DetailsWindow">RFID Tag No:</td><td><asp:Label ID="lbl_Asn_Rfidtag" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Part No:</td><td><asp:Label ID="lbl_Asn_Partno" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Lot No:</td><td><asp:Label ID="lbl_Asn_lotno" runat="server"></asp:Label></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Expiry Date:</td><td><asp:Label ID="lbl_Asn_Expiry" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Bin Rfid TagNO:</td><td><asp:Label ID="lbl_Asn_Bin" runat="server"/></td></tr>
                                        </table>
                                    </td>
                                    <td  width="40%" >
                                        <asp:Label ID="ASN_lbl_user_time_details" runat="server" CssClass="FloatRight" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:TabPanel>
                
    <%--RECIEPT TAB--%>
                    <asp:TabPanel ID="Tab_Receipt" runat="server">
                        <HeaderTemplate>Receipt  </HeaderTemplate>
                        <ContentTemplate>
                            <table width="708px">
                                <tr>
                                    <td  width="60%" >
                                        <table width="60%">
                                            <tr><td class="lbl_DetailsWindow">RFID Tag No:</td><td><asp:Label ID="lbl_Rec_RfidTag" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Part No:</td><td><asp:Label ID="lbl_Rec_PartNo" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Lot No:</td><td><asp:Label ID="lbl_Rec_lot" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Expiry Date:</td><td><asp:Label ID="lbl_Rec_Expiry" runat="server"/></td></tr>
                                            <tr><td class="lbl_DetailsWindow">Bin Rfid TagNO:</td><td><asp:Label ID="lbl_Rec_rfidbin" runat="server"/></td></tr>
                                        </table>
                                    </td>
                                    <td  width="40%" >
                                        <asp:Label ID="RCP_lbl_user_time_details" runat="server" CssClass="FloatRight" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:TabPanel>
                    
    <%--USAGE TAB--%>
                    <asp:TabPanel ID="Tab_usage" runat="server">
                        <HeaderTemplate>Usage  </HeaderTemplate>
                        <ContentTemplate>
                            <asp:GridView runat = "server" ID="DG_Usage" AutoGenerateColumns="false" BorderColor="White" BorderStyle="None" Width="100%" CssClass="adminlist" >
                            <%-- onselectedindexchanged="gv_Scan_SelectedIndexChanged" onrowdatabound="gv_Scan_RowDataBound1" onselectedindexchanging="gv_Scan_SelectedIndexChanging"  RowStyle-CssClass="row0" AlternatingRowStyle-CssClass="row1">--%>
                                <Columns>            
                                    <asp:BoundField DataField="addDttm" HeaderText="Date" DataFormatString="{0:dd/MMM/yyyy hh:mm}" HtmlEncode="false" />
                                    <asp:BoundField DataField="addUser" HeaderText="Operator" />
                                    <asp:BoundField DataField="useManualEntry" HeaderText="Entry" />
                                    <asp:BoundField DataField="useRfid" HeaderText="RFID Tag" />
                                    <asp:BoundField DataField="usePartNo" HeaderText="Part No" />
                                    <asp:BoundField DataField="useLotNo" HeaderText="Lot No"/>
                                    <asp:BoundField DataField="useExpiry" HeaderText="Expiry"  DataFormatString="{0:dd/MMM/yyyy HH:mm}" HtmlEncode="false"/>
                                    <asp:BoundField DataField="useSeqNo" HeaderText="useSeqNo" Visible="false" />
                                    <asp:BoundField DataField="usePartDesc" HeaderText="usePartDesc"  Visible="false" />
                                    <asp:BoundField DataField="delDttm" HeaderText="delDttm"  DataFormatString="{0:dd/MMM/yyyy HH:mm}" HtmlEncode="false" Visible="false" />
                                    <asp:BoundField DataField="delUser" HeaderText="delUser" Visible="false" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:TabPanel>
                    
    <%--DISPATCH TAB--%>
                    <asp:TabPanel ID="Tab_Dispatch" runat="server">
                        <HeaderTemplate>Dispatch  </HeaderTemplate>
                        <ContentTemplate>
                            <asp:GridView runat = "server" ID="DG_Dispatch" AutoGenerateColumns="false" BorderColor="White" BorderStyle="None" Width="100%" CssClass="adminlist" >
                            <%-- onselectedindexchanged="gv_Scan_SelectedIndexChanged" onrowdatabound="gv_Scan_RowDataBound1" onselectedindexchanging="gv_Scan_SelectedIndexChanging"  RowStyle-CssClass="row0" AlternatingRowStyle-CssClass="row1">--%>
                                <Columns>            
                                    <asp:BoundField DataField="addDttm" HeaderText="Date" DataFormatString="{0:dd/MMM/yyyy hh:mm}" HtmlEncode="false" />
                                    <asp:BoundField DataField="addUser" HeaderText="Operator" />
                                    <asp:BoundField DataField="disManualEntry" HeaderText="Entry" />
                                    <asp:BoundField DataField="disRfid" HeaderText="RFID Tag" />
                                    <asp:BoundField DataField="disPartNo" HeaderText="Part No" />
                                    <asp:BoundField DataField="disLotNo" HeaderText="Lot No"/>
                                    <asp:BoundField DataField="disExpiry" HeaderText="Expiry"  DataFormatString="{0:dd/MMM/yyyy HH:mm}" HtmlEncode="false"/>
                                    <asp:BoundField DataField="disSeqNo" HeaderText="useSeqNo" Visible="false" />
                                    <asp:BoundField DataField="disPartDesc" HeaderText="usePartDesc"  Visible="false" />
                                    <asp:BoundField DataField="delDttm" HeaderText="delDttm"  DataFormatString="{0:dd/MMM/yyyy HH:mm}" HtmlEncode="false" Visible="false" />
                                    <asp:BoundField DataField="delUser" HeaderText="delUser" Visible="false" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
                <div class="clr"></div>
            </div>
    	</ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="label_popViewItems" runat="server" Text="" /><%--Only exists to let some code with the non click activated modalpopups work --%>
<asp:ModalPopupExtender ID="ModalPopupExtender_ViewItem" TargetControlID="label_popViewItems" BackgroundCssClass="modalBackground" PopupControlID="Panel_viewitem" EnableViewState="false" CancelControlID="lnkbtn_VPD_ViewCancel" runat="server" />

<%--ADD PART--%>
<asp:Panel ID="Panel_Addnewitem" runat="server" CssClass="PopupPanelBorder ">  <%--DisplayNone--%>
<asp:UpdatePanel runat="server"><ContentTemplate>
<div id="element-box" class="requests">
<div id="border-top-requests" class="requests">
    <div>
        <div>
            <div class="user-toolbar" id="Div4">  
                <asp:LinkButton ID="lnkbtn_AP_Cancel" runat="server" onclick="lnkbtn_AP_Cancel_Click"><span class="icon-32-deny"></span> Cancel</asp:LinkButton>
                <asp:LinkButton ID="lnkbtn_AP_Ok" runat="server" onclick="lnkbtn_AP_Ok_Click" ValidationGroup="newitem"><span class="icon-32-save-new"></span>Save</asp:LinkButton>
                <div class="clr"></div>
            </div>
            <h1>Add Part</h1>
        </div>
    </div>
</div>
<div class="m">
    <div id="add-part-box">
        <table>
            <tr>
                <td class="lbl_DetailsWindow">Part Number:</td>
                <td>
                    <asp:TextBox ID="txt_partnumber" runat="server" BackColor="#FFFF99" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Part Number is Mandatory" ControlToValidate="txt_partnumber" Display="None" ValidationGroup="newitem">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1"/>
                    <asp:CompareValidator ID="Validcomp_partnumber" runat="server" ControlToValidate="txt_partnumber" enabled="true" ErrorMessage="Part number needs to be an Integer" Operator="DataTypeCheck" Type="Integer" ValidationGroup="newitem">*</asp:CompareValidator>
                    <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" runat="server" Enabled="True" TargetControlID="Validcomp_partnumber"/>
                </td>
            </tr>
            <tr>
                <td class="lbl_DetailsWindow">Description:</td>
                <td>
                    <asp:TextBox ID="txt_description" runat="server" BackColor="#FFFF99" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_partno" runat="server" ControlToValidate="txt_description" Display="None" ErrorMessage="Desc is mandatory" ValidationGroup="newitem">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="RequiredFieldValidator_partno_ValidatorCalloutExtender" runat="server" Enabled="True" TargetControlID="RequiredFieldValidator_partno"/>
                </td>
            </tr>
            <tr>
                <td class="lbl_DetailsWindow">Lot No:</td>
                <td><asp:TextBox ID="txt_lotnumber" runat="server"/></td>
            </tr>
            <tr>
                <td class="lbl_DetailsWindow">Expiry Date:</td>
                <td>
                    <asp:TextBox ID="txt_expiryDate" runat="server" BackColor="#FFFF99" /> 
                    <asp:ImageButton ID="Imgbtn_calendar" runat="server" ImageUrl="~/Images/calendar-icon.jpg" Width="16px"/>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_expiryDate" PopupButtonID="Imgbtn_calendar" Format="dd/MMM/yyyy" EnableViewState="false"/>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_expiryDate" PopupButtonID="txt_expiryDate" Format="dd/MMM/yyyy" EnableViewState="false"/>
                    <asp:RequiredFieldValidator ID="ReqdField_txt_expiryDate" runat="server" ControlToValidate="txt_expiryDate" Display="None" ErrorMessage="A valid date is mandatory" ValidationGroup="newitem" EnableViewState="false">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="txt_expiryDate_ValidatorCalloutExtender" runat="server" Enabled="True" TargetControlID="ReqdField_txt_expiryDate"/>
                </td>
            </tr>
            <tr>
                <td class="lbl_DetailsWindow">Comment:</td>
                <td>
                    <asp:TextBox ID="txt_comment" runat="server" BackColor="#FFFF99" />
                    <asp:RequiredFieldValidator ID="ReqdField_comment" runat="server" ControlToValidate="txt_comment" Display="None" ErrorMessage="Comment is mandatory" ValidationGroup="newitem" EnableViewState="false">*</asp:RequiredFieldValidator>
                    <asp:ValidatorCalloutExtender ID="ReqdField_comment_ValidatorCalloutExtender" runat="server" Enabled="True" TargetControlID="ReqdField_comment"/>
                </td>
            </tr>
        </table>
    </div>
</div>
</div>
</ContentTemplate></asp:UpdatePanel>
</asp:Panel>       
<asp:ModalPopupExtender ID="ModalPopupExtender_Addnewitem" TargetControlID="lnkbtn_STP_AddNewItem" BackgroundCssClass="modalBackground" PopupControlID="Panel_Addnewitem"  runat="server" /><%-- EnableViewState="false" CancelControlID="lnkbtn_AP_Cancel"--%>

<%--MULTIPLE HOSPITAL CHOOSER--%>
<asp:Panel ID="Panel_multiHosp" runat="server" BackColor="White" >
    <div id="Div3" class="requests_small">
        <div id="border-top1" class="requests_small"><h3>Multiple Readers</h3></div>
        <div style=" padding-left:10px; padding-right:10px;">
            <asp:ListBox ID="List_reader" runat="server" AppendDataBoundItems="True" cssclass="Open-ListBox" Width="100%" >
                <asp:ListItem/>
            </asp:ListBox>
        </div>
        <div class="clr"></div>
        <div class="button-holder" style="float:right;">
            <div class="login-button">
                <div class="next"><asp:LinkButton ID="lnkbtn_SubmitReaders" runat="server" onclick="lnkbtn_SubmitReaders_Click">Submit</asp:LinkButton></div>
            </div>
	    </div>
        <div class="clr"></div>
    </div>
</asp:Panel>
<asp:ModalPopupExtender ID="ModalPopupExtender_multiplehosp" runat="server" BackgroundCssClass="modalBackground" PopupControlID="Panel_multiHosp" TargetControlID="label_popreaders" EnableViewState="false" />  

<%--READER IN USE WARNING--%>
<asp:Panel ID="Panel_readerstate" runat="server" BackColor="White" >
    <div id="Div5" class="requests_small">
        <div id="border-top1" class="requests_small"><h3>Selected Reader Currently In Use</h3></div>    
	    <div class ="clr"></div>
        <div style ="display:block; padding:5px; padding-left:20px; color:Red;" ><b>If You Proceed other Users connecting to the Reader will be affected.<br />Do you Want to Proceed?</b></div>
        <div class="clr"></div>
        <div class="buttons-left" style="padding-left:120px;">
            <asp:LinkButton ID="lnk_reader_deselect" runat="server" onclick="lnk_reader_deselect_Click">No</asp:LinkButton>
            <div style="float:left; width:30px;">&nbsp</div>
            <asp:LinkButton ID="lnk_reader_select" runat="server" onclick="lnk_reader_select_Click">Yes</asp:LinkButton>
        </div>
    </div> 
</asp:Panel>  
<asp:ModalPopupExtender ID="ModalPopupExtender_Readerstate" runat="server" BackgroundCssClass="modalBackground" PopupControlID="Panel_readerstate" TargetControlID="label_popreadersConfict" EnableViewState="false"/>  

<asp:panel ID="pnl_ScannerFeedback" runat="server" Visible="false" CssClass="ScanerFeedback">
<asp:LinkButton ID="SF_lnkbtn_Scanin" runat="server"><span id="SF_spanactive" class="icon-32-upload-active2" runat="server"></span>
<asp:label ID="SF_lbl_ScanFeedback" runat="server" Text="0" Font-Bold="true"  /> items/sec</asp:LinkButton> <%--Font-Size="Large"--%>

</asp:panel>


<%--HIDDEN VARIABLES--%>
<asp:Label ID="label_popreaders" runat="server" Text=""  /><%--Only exists to let some code with the non click activated modalpopups work --%>
<asp:Label ID="label_popreadersConfict" runat="server" Text="" /><%--Only exists to let some code with the non click activated modalpopups work --%>
<asp:Timer ID="Timer_gridscan" runat="server" Interval="3000" ontick="Timer_gridscan_Tick"/>

<div id="divorderdetails" runat="server" visible="false">
    <asp:GridView ID="GridView_orderDetails" runat="server" AutoGenerateColumns="false" GridLines="None" BackColor="AntiqueWhite" onrowdatabound="GridView_orderDetails_RowDataBound">    
        <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblODPatient" runat="server" Text="Patient"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Patient" runat="server" Text='<%#Eval("Patient") %>'/></ItemTemplate></asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblOD_Hospital" runat="server" Text="Hospital:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Hospital" runat="server" Text='<%#Eval("HospitalName") %>'/></ItemTemplate></asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblOD_date" runat="server" Text="OrderDate:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Date" runat="server" Text='<%#Eval("OrderDate") %>'/></ItemTemplate></asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblODSurgeon" runat="server" Text="Surgeon:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Surgeon" runat="server" Text='<%#Eval("Doctor") %>'/></ItemTemplate></asp:TemplateField>    
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblODProcedure" runat="server" Text="Procedure:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Procedure" runat="server" Text='<%#Eval("OperationName") %>'/></ItemTemplate></asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label ID="lblODSupplier" runat="server" Text="Supplier:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_Supplier" runat="server" Text='<%#Eval("Supplier") %>'/></ItemTemplate></asp:TemplateField> 
            <asp:TemplateField ItemStyle-HorizontalAlign="Right"><ItemTemplate><b style="text-align:center"><asp:Label id="lbl_OD_bkref" runat="server" Text="BookingRef#:"/></b></ItemTemplate></asp:TemplateField>
            <asp:TemplateField><ItemTemplate><asp:Label id="lbl_OD_bookingref" runat="server" Text='<%#Eval("BookingNumber") %>'/></ItemTemplate></asp:TemplateField>  
        </Columns>
    </asp:GridView>
</div>
 
</asp:Content>
