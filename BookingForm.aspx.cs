﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Requirements and Functionality for a Booking form
/// Doctors/Other users will be able to add a Procedure and assign kit to a Supplier.
/// Once a Procedure is added kit for the patient can be view /edited.
/// once booking is made an email will be sent to the Supplier with Booking ID(Should the email be sent once loan kit is ordered ?)
/// A Supplier will be able to see bookings
/// Bookings grid must be sorted so that a Supplier can see only bookings that he supplies
/// 
/// </summary>
public partial class BookingForm : System.Web.UI.Page
{
    #region DEFENITIONS
    RFIDDataContext orfid = new RFIDDataContext();
    RfidClass OrfidClass = new RfidClass();
    RFID ClassRFID = new RFID();
    static string SysAdminAddress = ConfigurationManager.AppSettings["SystemAdminAddress"].ToString();

    protected override void Render(HtmlTextWriter writer)
    {
        for (int grdrows = 0; grdrows <= dg_IncludedSuppliers.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(dg_IncludedSuppliers.UniqueID, "Select$" + grdrows.ToString());
        }

        for (int grdrows = 0; grdrows <= dg_RemainingSuppliers.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(dg_RemainingSuppliers.UniqueID, "Select$" + grdrows.ToString());
        }

        for (int grdrows = 0; grdrows <= gv_SupplierAssociatedContacts.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(gv_SupplierAssociatedContacts.UniqueID, "Select$" + grdrows.ToString());
        }

        for (int grdrows = 0; grdrows <= gv_SupplierContactsEmailToggle.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(gv_SupplierContactsEmailToggle.UniqueID, "Select$" + grdrows.ToString());
        }

        for (int grdrows = 0; grdrows <= pas_dg_CurrentSurgeons.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(pas_dg_CurrentSurgeons.UniqueID, "Select$" + grdrows.ToString());
        }

        base.Render(writer);
    }
    #endregion
    #region PAGE PROPERTIES
    private int SessINT(string n)
    {
        if (Session[n] != null)
        {
            return RfidClass.strInt(Session[n].ToString());
        }
        else
        {
            return -1;
        }
    }
    private Nullable<int> SessINTnull(string n)
    {
        if (Session[n] != null)
        {
            string bID = Session[n].ToString();
            return RfidClass.strInt(bID);
        }
        else
            return null;
    }
    public int HOSPID { get { return SessINT("HospID"); } set { Session["HospID"] = value; } }
    public int ACCID { get { return SessINT("accID"); } set { Session["accID"] = value; } }
    public Nullable<int> BOOKID { get { return SessINTnull("BookID"); } }
    public Nullable<int> SUPID { get { return SessINTnull("SupID"); } set { Session["SupID"] = value; } }
    //INCLUDING SUPPLIERS SELECTED VARIABLES
    public Nullable<int> HSID { get { return SessINTnull("hsID"); } set { Session["hsID"] = value; } } 
    public string HSIDENTIFIER
    {
        get
        {
            var v = Session["hsIdentifier"];
            if (v != null)
                return v.ToString();
            else
                return null;
        }
        set { Session["hsIdentifier"] = value; }
    }
    public string HSGLN
    {
        get
        {
            var v = Session["hsGLN"];
            if (v != null)
                return v.ToString();
            else
                return null;
        }
        set { Session["hsGLN"] = value; }
    }
    public int INCLUDESUPID { get { return RfidClass.strInt(Session["IncludeSupID"].ToString()); } set { Session["IncludeSupID"] = value; } }

    //INCLUDING SURGEONS SELECTED VARIABLES
    public Nullable<int> HSURID
    {
        get
        {
            if (Session["hsurID"] != null)
            {
                string bID = Session["hsurID"].ToString();
                return RfidClass.strInt(bID);
            }
            else
                return null;
        }
        set
        {
            Session["hsurID"] = value;
        }
    }
    public string HSURIDENTIFIER
    {
        get
        {
            var v = Session["hsurIdentifier"];
            if (v != null)
                return v.ToString();
            else
                return null;
        }
        set { Session["hsurIdentifier"] = value; }
    }
    public string HSURGLN
    {
        get
        {
            var v = Session["hsurGLN"];
            if (v != null)
                return v.ToString();
            else
                return null;
        }
        set { Session["hsurGLN"] = value; }
    }
    public int INCLUDESURID { get { return RfidClass.strInt(Session["IncludeSurID"].ToString()); } set { Session["IncludeSurID"] = value; } }

    public bool ORDERLOANKIT
    {
        get
        {
            if (Session["OrderLoanKit"] == null)
                return false;
            else
                return true;
        }
    }
    public bool PROCESSLOANKIT
    {
        get
        {
            if (Session["ProcessLoankit"] == null)
                return false;
            else
                return true;
        }
    }
    public bool FINCANCIALRECORDING
    {
        get
        {
            if (Session["FinancialRecording"] == null)
                return false;
            else
                return true;
        }
    }
    #endregion

    #region BOOKING PROPERTIES
        #region ACCESSING CONTROLS
            #region DDL
            private System.Nullable<int> DdlInt(DropDownList ddl)
            {
                if (ddl.SelectedIndex != 0)
                    return int.Parse(ddl.SelectedValue.ToString());
                else
                    return null;
            }
            private String DdlStr(DropDownList ddl)
            {
                if (ddl.SelectedIndex != 0)
                    return ddl.SelectedItem.Text;
                else
                    return null;
            }
            private System.Nullable<char> DdlChr(DropDownList ddl)
            {
                if (ddl.SelectedIndex != 0)
                    return char.Parse(ddl.SelectedValue.ToString());
                else
                    return null;
            }
        #endregion
            #region CBX
                private System.Nullable<int> CbxInt(AjaxControlToolkit.ComboBox cbx)
                {
                    if (cbx.SelectedIndex != 0)
                        return int.Parse(cbx.SelectedValue.ToString());
                    else
                        return null;
                }
                private String CbxStr(AjaxControlToolkit.ComboBox cbx)
                {
                    if (cbx.SelectedIndex != 0)
                        return cbx.SelectedItem.Text;
                    else
                        return null;
                }
            #endregion
            #region TXT
                private System.Nullable<DateTime> TxtDt(TextBox txt)
                {
                    if (txt.Text != "")
                    {
                        DateTime D = DateTime.Parse(txt.Text);
                        return DateTime.Parse(D.ToString("dd/MMM/yyyy"));
                    }
                    else
                        return null;
                }
                private System.Nullable<int> TxtInt(TextBox txt)
                {
                    if (txt.Text != "")
                        return int.Parse(txt.Text.Replace(" ", "").Replace("+",""));
                    else
                        return null;
                }
            #endregion
        #endregion

        #region SURGEON
            private String SurgeonStr { get { return CbxStr(ddl_Surgeon);}}
            private System.Nullable<int> SurgeonInt { get { return CbxInt(ddl_Surgeon); } }
        #endregion
        #region KITTYPE
            private System.Nullable<int> KitTypeInt { get { return DdlInt(ddl_kittype);}}
            private String KitTypeStr { get { return DdlStr(ddl_kittype);}}
        #endregion
        #region KITSIDE
            private System.Nullable<int> KitSideInt { get { return DdlInt(ddl_Side);}}
            private String KitSideStr { get { return DdlStr(ddl_Side);}}
        #endregion
        #region OPDATE
            private System.Nullable<DateTime> OpDateDt { get { return TxtDt(txt_opdate);}}
        #endregion
        #region OPTYPE
            private System.Nullable<int> OpTypeInt { get { return DdlInt(ddl_operationtype); } }
            private String OpTypeStr { get { return DdlStr(ddl_operationtype); } }
        #endregion
        #region PATTYPE
            private System.Nullable<int> PatTypeInt { get { return DdlInt(ddl_PatientType); } }
        #endregion
        #region CASETYPE
            private System.Nullable<int> CaseTypeInt { get { return DdlInt(ddl_casetype); } }
        #endregion
        #region GENDER
            private System.Nullable<char> GenderChr { get { return DdlChr(ddl_gender); } }
        #endregion
        #region CONTACTNUMBER
            private System.Nullable<int> ContactNumberInt { get { return TxtInt(txt_contactnumber);}}
            private string ContactNumberStr { get { return txt_contactnumber.Text; } }
        #endregion
    #endregion

    #region LOADING / POPULATE / INITIALIZE
    protected void Page_Load(object sender, EventArgs e)
{
    if (Request.ServerVariables["http_referer"] != null)
    {
        try
        {
            if (Page.IsPostBack == false)
            {
                LoadIncludeSupplier();
                LoadSuppliersBookedDDL(null);
                mpe_IncludedSuppliers.Hide();
                LoadSurgeons();
                LoadSurgeonsDDL(null);
                mpe_AddSurgeons.Hide();
                ClassRFID.StopReader();
                div_ack.Visible = false;

                if (Session["BookId"] != null)
                    Session.Remove("BookId");
                if (Request.QueryString["BookingEdit"] != null)
                    div_receipt.Visible = true;
                else
                    div_receipt.Visible = false;
                // By default loan kit cannot be added/updated without selecting the procedure

                //this dropdown is no longer displayed but I will check if it referenced later
                if (Session["SuplierID"] != null)
                {
                    if (Session["ProcedureHosp"] != null)
                    {
                        IQueryable<int> tblhospid = from tbhosp in orfid.tblImplantHospitals
                                                    where tbhosp.hospName == Session["ProcedureHosp"].ToString()
                                                    select tbhosp.hospId;

                        populateddlHospitalbasedonHospitalID(tblhospid.SingleOrDefault().ToString());
                    }
                    else
                    {
                        populateddlHospitalbasedonHospitalID("");//we are supplying an empty string
                    }
                    //Supplier will not have any functional right in booking form
                    div_receipt.Visible = false;
                    div_cancel.Visible = false;
                    div_updateprocedure.Visible = false;
                }
                else
                {
                    populateddlHospitalbasedonHospitalID(Session["HospID"].ToString());
                }

                //RevisonNumber (By default Revison Number must be 0 for a new booking)
                lbl_revno.Text = "1";
                if (Request.QueryString["BookingEdit"] != null)
                {
                    EditProcedure();
                }
                else
                {
                    PrimeDisplayForNewBooking();
                }
            }
            if (Session["HospID"] != null)
                ddl_Hospital.SelectedValue = Session["HospID"].ToString();// Hospital is set to a default value (Users who belong to a hospital will be able to create a booking)

            RoleBasedAccessToBooking();
        }
        catch (Exception exPageload)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "Page_Load", DateTime.Now.AddDays(0), exPageload.Message.ToString());
        }
    }
    else
    {
        Response.Redirect("Login.aspx");
    }
}
    private void RoleBasedAccessToBooking()
    {
        if (ORDERLOANKIT)
        {
            lnkbtn_updateProcedure.Enabled = true;
            lnkbtn_deleteBooking.Enabled = true;
        }
        if (PROCESSLOANKIT)//This Role is must for a Person to Scan and Dispatch Items
        {
            lnk_receiptbooking.Enabled = true;
        }
    }

private void populateddlHospitalbasedonHospitalID(string StrHospID)
{
    try
    {
        if (StrHospID != "")
        {
            var oHospital = from hosp in orfid.tblImplantHospitals
                            where hosp.hospId == int.Parse(StrHospID.ToString())
                            select new
                            {
                                HospitalID = hosp.hospId,
                                Hospitalname = hosp.hospName + "," + hosp.hospCampus
                            };
            ddl_Hospital.DataSource = oHospital;
        }
        else
        {
            var oHospital = from hosp in orfid.tblImplantHospitals
                            select new
                            {
                                HospitalID = hosp.hospId,
                                Hospitalname = hosp.hospName + "," + hosp.hospCampus
                            };
            ddl_Hospital.DataSource = oHospital;
        }
        ddl_Hospital.DataValueField = "HospitalID";
        ddl_Hospital.DataTextField = "Hospitalname";
        ddl_Hospital.DataBind();
    }
    catch (Exception exHospitalddl)
    {
        orfid.Usp_WebErrors("BookingForm.aspx", "populateddlHospitalbasedonHospitalID", DateTime.Now.AddDays(0), exHospitalddl.Message.ToString());
    }
}
#endregion

    #region UPDATE / SAVE / SUBMIT
    #region CLEARING FIELDS
    private void Procedure_Updateclear()
    {
        ddl_Hospital.SelectedIndex = 0;
        lbl_revno.Text = "1";
        txt_contactnumber.Text = "";
        txt_opdate.Text = "";
        //txt_oprtime_hr.Text = "";
        //txt_oprtime_mn.Text = "";
        ddl_Surgeon.SelectedIndex = 0;
        txt_Patientname.Text = "";
        //ddl_oprtime.SelectedIndex = 0;
        ddl_gender.SelectedIndex = 0;
        ddl_PatientType.SelectedIndex = 0;
        txt_PatientUR.Text = "";
        txt_Procedure.Text = "";
        ddl_Supplier.SelectedIndex = 0;
        txt_productsreqd.Text = "";
        ddl_operationtype.SelectedIndex = 0;
        ddl_casetype.SelectedIndex = 0;
        txt_notes.Text = "";
        ddl_kittype.SelectedIndex = 0;
        ddl_operationtype.SelectedIndex = 0;
        div_receipt.Visible = false;
        lbl_confirmProcedure.Text = "";
    }
    private void Loankit_UpdateClear()
    {
        txt_productsreqd.Text = "";
        ddl_kittype.SelectedIndex = 0;
        ddl_Side.SelectedIndex = 0;
        ddl_casetype.SelectedIndex = 0;
        txt_notes.Text = "";
        ddl_Supplier.SelectedIndex = 0;
        ddl_Surgeon.SelectedIndex = 0;
        ddl_operationtype.SelectedIndex = 0;
    }
#endregion
    #region BUTTONS
    protected void btn_Orderloankit_Click(object sender, EventArgs e)
    {
        div_booking.Visible = false;
    }
    protected void btn_Cancelloankit_Click(object sender, EventArgs e)
    {
        div_booking.Visible = true;
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        if (Session["BookId"] != null)
        {
            lbl_confirmProcedure.Text = "Values cannot be canceled during Edit Mode";
            return;
        }
        else
        {
            Procedure_Updateclear();
        }
    }
    protected void lnkbtn_updateProcedure_Click(object sender, EventArgs e)
    {
        LoadAvailableSelectedContacts();
        N_Ok_Vis = true;
        mpe_Notify.Show();
    }
    #endregion 

    private void EditProcedure()
    {
        try
        {
            string StrOpDate = "";
            //CheckBox chk_booking = (CheckBox)Grd_Procedures.Rows[i].Cells[0].FindControl("Chk_Booking");
            if (Request.QueryString["BookingEdit"] != null || Session["BookId"] != null)
            {
                string StrbookId = "";

                if (Request.QueryString["BookingEdit"] != null)
                    StrbookId = Request.QueryString["BookingEdit"].ToString();
                if (Session["BookId"] == null)
                    Session["BookId"] = StrbookId.ToString();

                var bookingEdit = orfid.rp_implantGetBookingDetails(BOOKID);

                foreach (var v in bookingEdit)
                {
                        if (v.revOperationDttm != null)
                        {
                            DateTime DtOpdate = v.revOperationDttm.Value;
                            if (DtOpdate != null)
                                StrOpDate = DtOpdate.ToString("dd/MMM/yyyy");
                        }
                        LockFieldsForEditing();
                        lbl_processedby.Text = v.ProcessedBy;
                        ddl_Hospital.SelectedValue = v.bookHospId.ToString();

                        if (Session["SuplierID"] != null && v.revlkitstatus == "Pending")//pending and entered are tblbookingversions value
                        {
                            div_ack.Visible = true;
                        }

                        int Intrevno = OrfidClass.BookingRevisionNumber(int.Parse(Session["BookId"].ToString()));
                        lbl_revno.Text = Intrevno.ToString();
                        ddl_Surgeon.SelectedValue = v.bookHospSurgeonId.ToString() ;
                        txt_contactnumber.Text = v.revContactNo;
                        txt_opdate.Text = StrOpDate;
                        txt_Patientname.Text = v.ptFamilyName;
                        txt_PatientUR.Text = v.ptUR.ToString();
                        ddl_Supplier.SelectedValue = v.suppId.ToString();
                        SUPID = (int)v.suppId;
                        LoadAvailableSelectedContacts();
                    
                        txt_Procedure.Text = v.revProcedure;
                        if (v.ptSex != "" && v.ptSex != null)
                            ddl_gender.SelectedValue = v.ptSex;
                        if (v.revPatientType != null)
                            ddl_PatientType.SelectedValue = v.revPatientType.ToString();
                        if (v.revKitSide != null)
                            ddl_Side.SelectedValue = v.revKitSide.ToString();
                        if (v.revProcedure != null)
                            txt_Procedure.Text = v.revProcedure;

                        txt_productsreqd.Text = v.revProductsReqd;
                        if (v.revKitType != null)
                            ddl_kittype.SelectedValue = v.revKitType.ToString();
                        if (v.revOpType != null)
                            ddl_operationtype.SelectedValue = v.revOpType.ToString();
                        //VarBitems.revOther = "";
                        if (v.revKitSide != null)
                            ddl_Side.SelectedValue = v.revKitSide.ToString();
                        if (v.revCaseType != null)
                            ddl_casetype.SelectedValue = v.revCaseType.ToString();
                        txt_notes.Text = v.revNotes;
                        if (Session["BookId"] != null)
                            lbl_bookingID.Text = Session["BookId"].ToString();
                }
            }
        }
        catch (Exception exEditProcedure)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "EditProcedure", DateTime.Now.AddDays(0), exEditProcedure.Message.ToString());
        }
    }
    protected void LockFieldsForEditing()
    {
        lbl_LockedForEditing.Visible = true;
        ddl_Surgeon.Enabled = false;
        txt_PatientUR.Enabled = false;
        txt_Patientname.Enabled = false;
        ddl_gender.Enabled = false;
        ddl_PatientType.Enabled = false;
    }
    protected void PrimeDisplayForNewBooking()
    {
        lbl_RevisionNumberDisplay.Visible = false;
        lbl_revno.Visible = false;
        lbl_BookingIDDisplay.Visible = false;
        lbl_bookingID.Visible = false;
        lbl_ProcessedByDisplay.Visible = false;
        lbl_processedby.Visible = false;
        lnkbtn_Notify.Enabled = false;
        //lnkbtn_Notify.Visible = false;
    }

    protected void UpdateProcedure()
    {
        try
        {
            if (Request.QueryString["BookingEdit"] != null || Session["BookId"] != null)    //UPDATE EXISTING BOOKINGS
            {
                if (Session["BookId"] == null)
                    Session["BookId"] = Request.QueryString["BookingEdit"].ToString();

                orfid.rp_UpdateBooking(BOOKID, int.Parse(ddl_Supplier.SelectedValue.ToString()), int.Parse(lbl_revno.Text), ContactNumberStr, "", OpDateDt, null,
                    txt_productsreqd.Text, KitTypeInt, KitTypeStr, KitSideInt, KitSideStr, OpTypeInt,
                    OpTypeStr, txt_notes.Text, CaseTypeInt, "Entered", txt_Procedure.Text, ACCID);
                Procedure_Updateclear();
            }
            else   //ADDING NEW BOOKINGS
            {
                int IntHospID = int.Parse(ddl_Hospital.SelectedValue.ToString());
                int IntRevno = int.Parse(lbl_revno.Text);
                //int IntPtientUR = int.Parse(txt_PatientUR.Text);
                int IntProcessedBy = ACCID;
                Guid gOrgID = new Guid(Session["Se_ORGID"].ToString());/**** OrgiD is organization ID can be obtained from Login.aspx*/

                var BookID = orfid.rp_CreateBooking(gOrgID, IntHospID, IntRevno, 1, (int.Parse(ddl_Supplier.SelectedValue.ToString())), ContactNumberInt, null, txt_Procedure.Text,
                            SurgeonInt, txt_Patientname.Text, txt_PatientUR.Text, GenderChr, PatTypeInt, OpDateDt, null, txt_productsreqd.Text, KitTypeInt, OpTypeInt,
                            KitSideInt, CaseTypeInt, txt_notes.Text, IntProcessedBy, DateTime.Now);
                Session["BookID"] = BookID.ToString();  //ReturnValue
                /*Mail functionality should be added in order to send mail to supplier
             Here we need SystemAdmin Address, Supplier Address
             Supplier Address Must be decided based on the Supplier chosen from ddl_Supplier
             */
                /*Mail should be sent to supplierwarehouse of that region not to a generalised supplier code has to be changed*/
                // The below function is right and it can be uncommented once speculation about supplier Email Address is clear

                //SupplierEmail = OrfidClass.SupplierDetails(int.Parse(ddl_Supplier.SelectedValue.ToString()), 2);
                //MailBody = "BookingId for " + "  " + Session["Hospname"].ToString() + "  " + ":" + BookingID.ToString() + "";
                //MailSubject = "BookingID from eHealthFlow";
                ////OrfidClass.BookingEmailSupplierwh(int.Parse(ddl_Supplier.SelectedValue.ToString()), SysAdminAddress, MailSubject, MailBody);
                //OrfidClass.MailRequests(SupplierEmail, SysAdminAddress, MailSubject, MailBody);
            }

            //ATTACH CONTACT EMAILS - Once the booking has been created supplier contact emails can be attached by looping throught he rows of the Gridview
            foreach (GridViewRow r in gv_SupplierContactsEmailToggle.Rows)
            {
                Label lbl_accId = (Label)r.FindControl("lbl_accId");
                CheckBox cbx_ContactNotify = (CheckBox)r.FindControl("cbx_ContactNotify");
                Label lbl_to = (Label)r.FindControl("lbl_to");

                int emailAccId = RfidClass.strInt(lbl_accId.Text);
                bool status = cbx_ContactNotify.Checked;
                char to = lbl_to.Text.ToCharArray()[0];


                if (BOOKID != null)    //new bookings contact emails will be added in bulk as the new booking is created
                {
                    if (status)
                        orfid.rp_implantAddBookingEmailRecipient(BOOKID, emailAccId, to, ACCID);
                    else
                        orfid.rp_implantDeleteBookingEmailRecipient(BOOKID, emailAccId);
                }
            }

            Response.Redirect("Procedure.aspx", false);
        }
        catch (Exception exupdateprocedure)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "lnkbtn_UpdateProcedure", DateTime.Now.AddDays(0), exupdateprocedure.Message.ToString());
        }
    }
    #endregion

    #region MANUAL RECEIPT BOOKING
    /// <summary>
    /// Receipt booking adds a Booking number in  tblImplantorder 
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnk_receiptbooking_Click(object sender, EventArgs e)
    {
        try
        {
            string StrOrderID = "";

            if (Request.QueryString["BookingEdit"] != null || Session["BookId"] != null)
            {
                var tblBookingorder = from tblbooking in orfid.tblImplantBookings
                                      where tblbooking.bookId == int.Parse(Request.QueryString["BookingEdit"].ToString())
                                      select new { OrderID = tblbooking.bookOrderId };
                foreach (var Bkorderid in tblBookingorder)
                {
                    StrOrderID = Bkorderid.OrderID.ToString();
                }

                Guid DBkOrder = new Guid(StrOrderID);
                var tblimporder = (from tbimporder in orfid.tblImplantOrders
                                   where tbimporder.ordId == DBkOrder
                                   select tbimporder).Single();
                tblimporder.ordBookingNumber = "0000";
                orfid.SubmitChanges();
                Response.Redirect("Procedure.aspx", false);
            }
        }
        catch (Exception ExReceiptbooking)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "lnk_receiptbooking_Click", DateTime.Now.AddDays(0), ExReceiptbooking.Message.ToString());
        }
    }
    #endregion

    #region SUPPLIER
    protected void ddl_Supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadAvailableSelectedContacts();
    }

    #region SUPPLIER CONTACTS
    protected void LoadAvailableSelectedContacts()
    {
        int SelectedSupId = int.Parse(ddl_Supplier.SelectedValue.ToString());
        int SelectedSurgId = int.Parse(ddl_Surgeon.SelectedValue.ToString());
        //Determine editing or Creating booking
        if (SelectedSupId >= 0 && SelectedSurgId >= 0)   //cover the event of the list being set to nothing.
        {
            if (Request.QueryString["BookingEdit"] != null || Session["BookId"] != null)
            {
                if (Session["BookId"] == null)
                    Session["BookId"] = Request.QueryString["BookingEdit"].ToString();

                if (SUPID == RfidClass.strInt(ddl_Supplier.SelectedValue))
                    gv_SupplierContactsEmailToggle.DataSource = orfid.rp_implantGetAllEmailRecipients(BOOKID, HOSPID, SelectedSupId, SelectedSurgId);
                else
                    gv_SupplierContactsEmailToggle.DataSource = orfid.rp_implantGetAllEmailRecipients(null, HOSPID, SelectedSupId, SelectedSurgId);
            }
            else
            {
                gv_SupplierContactsEmailToggle.DataSource = orfid.rp_implantGetAllEmailRecipients(null, HOSPID, SelectedSupId, SelectedSurgId);
            }
        }
        else
            gv_SupplierContactsEmailToggle.DataSource = null;

        gv_SupplierContactsEmailToggle.DataBind();
    }

    protected void gv_SupplierContactsEmailToggle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_SupplierContactsEmailToggle, "Select$" + e.Row.RowIndex.ToString()));
            }
        }
        catch (Exception EGridrowdatabound)
        {
            int lakj = 1;
        }
    }
    #endregion
    #region INCLUDE SUPPLIERS
    protected void btn_IncludeSupplier_Click(object sender, EventArgs e)
    {
        ClearSupplierDetails();
        LoadIncludeSupplier();
        //Display Include Window (attached in aspx code with ModalPopup control)
    }

    //Prepare Clean Include Supplier Layout
    protected void ClearSupplierDetails()
    {
        //Clear supplier lists
        dg_IncludedSuppliers.SelectedIndex = -1;
        dg_IncludedSuppliers.DataSource = null;
        dg_IncludedSuppliers.DataBind();
        dg_RemainingSuppliers.SelectedIndex = -1;
        dg_RemainingSuppliers.DataSource = null;
        dg_RemainingSuppliers.DataBind();

        WipeSupplierDetails();
        pnl_AddNewSupplierButtonHolder.Visible = true;
        pnl_AddNewSupplier.Visible = false;
    }
    //Load Include supplier Fields
    protected void LoadIncludeSupplier()
    {
        dg_RemainingSuppliers.SelectedIndex = -1;
        dg_IncludedSuppliers.SelectedIndex = -1;
        WipeSupplierDetails();

        //Set HospName header
        lbl_HospName.Text = Session["HospName"].ToString();

        //Load Existing Suppliers
        var oSupplier = orfid.rp_implantGetHospitalSuppliers(HOSPID);
        dg_IncludedSuppliers.DataSource = oSupplier;
        dg_IncludedSuppliers.DataBind();

        //Load Remaining Suppliers
        var rSupplier = orfid.rp_implantGetHospitalExtraSuppliers(HOSPID);
        dg_RemainingSuppliers.DataSource = rSupplier;
        dg_RemainingSuppliers.DataBind();
        mpe_IncludedSuppliers.Show();
    }
    //Load the supplier list on the base page booking dropdown list, parameter allows for a value to be selected after control being populated
    protected void LoadSuppliersBookedDDL(string selected)
    {
        if (selected == null)
            selected = ddl_Supplier.SelectedValue;
        ddl_Supplier.Items.Clear();
        ddl_Supplier.Items.Insert(0, new ListItem("", "-1"));

        var oSupplier = orfid.rp_implantGetHospitalSuppliers(HOSPID);
        ddl_Supplier.DataSource = oSupplier;
        ddl_Supplier.DataTextField = "SupName";
        ddl_Supplier.DataValueField = "SupID";
        ddl_Supplier.DataBind();
        ddl_Supplier.SelectedValue = selected;
    }

    protected void dg_RemainingSuppliers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(dg_RemainingSuppliers, "Select$" + e.Row.RowIndex.ToString()));
            }
        }
        catch (Exception EGridrowdatabound)
        {
            int lakj = 1;
        }
    }
    protected void dg_IncludedSuppliers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(dg_IncludedSuppliers, "Select$" + e.Row.RowIndex.ToString()));
            }
        }
        catch (Exception EGridrowdatabound)
        {
            int lakj = 1;
        }
    }

    #region "EDIT / INCLUDE SUPPLIERS"
    protected void row_EditSupplier_Click(object sender, EventArgs e)
    {
        ShowIncludeSupplierDialog(dg_IncludedSuppliers, true);
    }
    protected void row_IncludeThisSupplier_Click(object sender, EventArgs e)
    {
        ShowIncludeSupplierDialog(dg_RemainingSuppliers, false);
    }
    protected void ShowIncludeSupplierDialog(GridView gv, bool editing)
    {
        try
        {
            if (editing)
            {
                dg_RemainingSuppliers.SelectedIndex = -1;
                btn_IncludeThisSupplier.Text = "Confirm Supplier Changes";
            }
            else
            {
                dg_IncludedSuppliers.SelectedIndex = -1;
                btn_IncludeThisSupplier.Text = "Include Selected Supplier";
            }

            WipeSupplierDetails();
            pnl_IncludingSuppliersToHospitals.Enabled = true;
            pnl_IncludingSuppliersToHospitals.ForeColor = Color.Black;
            Label lbl_supId = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_supId");
            Label lbl_supName = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_supName");
            INCLUDESUPID = RfidClass.strInt(lbl_supId.Text);
            lbl_SupplierName.Text = lbl_supName.Text;

            //Check for a Current Supplier
            var supplier = orfid.rp_implantGetHospitalSupplier(HOSPID, INCLUDESUPID);
            foreach (var s in supplier)
            {
                txt_ContactEmail.Text = s.accUsername;
                if (s.hsSendEmail == 'Y')
                    cbx_EmailBooking.Checked = true;
                else
                    cbx_EmailBooking.Checked = false;
                txt_SupplierPhone.Text = s.hsTelephone;
                txt_SupplierFax.Text = s.hsFax;

                HSID = s.hsId;
                HSIDENTIFIER = s.hsIdentifier;
                HSGLN = s.hsGln;
            }
            LoadSupplierContacts(INCLUDESUPID);
        }
        catch (Exception Exgridviewindexchanged)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "row_IncludeThisSupplier_Click", DateTime.Now.AddDays(0), Exgridviewindexchanged.Message.ToString());
        }
    }
    protected void WipeSupplierDetails()
    {
        HSID = null;
        HSIDENTIFIER = null;
        HSGLN = null;

        lbl_SupplierName.Text = "Supplier Details";
        pnl_IncludingSuppliersToHospitals.ForeColor = Color.Silver;
        txt_ContactEmail.Text = "";
        txt_SupplierPhone.Text = "";
        txt_SupplierFax.Text = "";
        gv_SupplierAssociatedContacts.DataSource = null;
        gv_SupplierAssociatedContacts.DataBind();
        pnl_IncludingSuppliersToHospitals.Enabled = false;
    }

    #region "SUPPLIER CONTACTS"
    //Show Current Supplier Contacts
    protected void LoadSupplierContacts(int supID)
    {
        var oContacts = orfid.rp_implantGetHospitalSupplierContacts(HOSPID, supID);
        gv_SupplierAssociatedContacts.DataSource = oContacts;
        gv_SupplierAssociatedContacts.DataBind();
    }
    protected void gv_SupplierAssociatedContacts_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_SupplierAssociatedContacts, "Select$" + e.Row.RowIndex.ToString()));
            }
        }
        catch (Exception EGridrowdatabound)
        {
            int lakj = 1;
        }
    }
    protected void btn_ISDAddSupplierContact_Click(object sender, EventArgs e)
    {
        orfid.rp_implantAddChangeHospitalSupplierContact(null, HOSPID, INCLUDESUPID, txt_ISDAddSupplierContact_Name.Text, txt_ISDAddSupplierContact_Email.Text, ACCID);
        LoadSupplierContacts(INCLUDESUPID);
        LoadAvailableSelectedContacts();
        txt_ISDAddSupplierContact_Name.Text = "";
        txt_ISDAddSupplierContact_Email.Text = "";
    }
    protected void row_RemoveThisContact_Click(object sender, EventArgs e)
    {
        Label lbl_hscId = (Label)gv_SupplierAssociatedContacts.SelectedRow.Cells[0].FindControl("lbl_hscId");
        orfid.rp_implantDeleteHospitalSupplierContact(RfidClass.strInt(lbl_hscId.Text));
        LoadSupplierContacts(INCLUDESUPID);
    }
    #endregion

    protected void btn_IncludeThisSupplier_Click(object sender, EventArgs e)
    {
        try
        {
            //include this supplier
            Nullable<char> emailChecked = null;
            if (cbx_EmailBooking.Checked)
                emailChecked = 'Y';
            string SupName = lbl_SupplierName.Text;

            orfid.rp_implantAddChangeHospitalSupplier(HSID, HOSPID, INCLUDESUPID, HSIDENTIFIER, HSGLN, txt_SupplierPhone.Text, txt_SupplierFax.Text, txt_ContactEmail.Text, emailChecked, null, ACCID);

            LoadIncludeSupplier();
            LoadSuppliersBookedDDL(INCLUDESUPID.ToString());
            WipeSupplierDetails();
        }
        catch (Exception ExInclude)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "btn_IncludeThisSupplier_Click", DateTime.Now.AddDays(0), ExInclude.Message.ToString());
        }
    }
    #endregion

    #region "ADD/CREATE NEW SUPPLIER"
    protected void lnkbtn_AddNewSupplier_Click(object sender, EventArgs e)
    {
        //clean and initialize all fields
        lbl_AddNewSupplierValidationExists.Visible = false;
        txt_NewSupplierName.Text = "";

        pnl_AddNewSupplierButtonHolder.Visible = false;
        pnl_AddNewSupplier.Visible = true;
    }

    protected void lnkbtn_ConfirmNewSupplier_Click(object sender, EventArgs e)
    {
        //CleanValidationMessages
        lbl_AddNewSupplierValidationExists.Visible = false;
        //Add New Supplier
        var result = orfid.rp_implantAddSupplier(txt_NewSupplierName.Text, ACCID);
        string addResult = null;
        Nullable<int> supId = -1;
        foreach (var s in result)
        {
            addResult = s.addResult;
            supId = (int)s.supId;
        }

        if (addResult == "Existing")
        {
            lbl_AddNewSupplierValidationExists.Visible = true;
        }
        else
        {
            //Close dialog
            pnl_AddNewSupplierButtonHolder.Visible = true;
            pnl_AddNewSupplier.Visible = false;
            LoadIncludeSupplier();
        }
        ClearSupplierDetails();
        LoadIncludeSupplier();
        //Select New or Existing Supplier
        int c = 0;
        foreach (GridViewRow r in dg_RemainingSuppliers.Rows)
        {
            Label lbl_supId = (Label)r.Cells[0].FindControl("lbl_supId");
            if (lbl_supId.Text == supId.ToString())
            {
                dg_RemainingSuppliers.SelectedIndex = c;
                ShowIncludeSupplierDialog(dg_RemainingSuppliers, false);
            }
            c++;
        }
    }
    protected void lnkbtn_CancelNewSupplier_Click(object sender, EventArgs e)
    {
        pnl_AddNewSupplierButtonHolder.Visible = true;
        pnl_AddNewSupplier.Visible = false;
    }
    protected void txt_NewSupplierName_TextChanged(object sender, EventArgs e)
    { }
    #endregion
    #endregion
    #endregion

    #region SURGEON
    protected void ddl_Surgeon_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadAvailableSelectedContacts();
    }

    /// <summary>
    /// Load all Surgeons for a Hospital
    /// </summary>
    /// <param name="selected"></param>
    protected void LoadSurgeonsDDL(string selected)
    {
        try
        {
            if (selected == null)
                selected = ddl_Surgeon.SelectedValue;
            ddl_Surgeon.Items.Clear();
            ddl_Surgeon.Items.Insert(0, new ListItem("", "-1"));

            var oSurgeon = orfid.rp_implantGetHospitalSurgeons(HOSPID);
            foreach (var s in oSurgeon)
            {
                ddl_Surgeon.Items.Add(new ListItem(s.accName, s.hsurId.ToString()));
            }
            ddl_Surgeon.SelectedValue = selected;
        }
        catch (Exception exEditProcedure)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "LoadSurgeonsDDL", DateTime.Now.AddDays(0), exEditProcedure.Message.ToString());
        }
    }
    #region ADDING SURGEON
    protected void LoadSurgeons()
        {
        try
            {
            WipeSurgeonDetails();

            //Set HospName header
            pas_lbl_HospName.Text = Session["HospName"].ToString();

            //Load Existing Suppliers
            var oSurgeon = orfid.rp_implantGetHospitalSurgeons(HOSPID);
            pas_dg_CurrentSurgeons.DataSource = oSurgeon;
            pas_dg_CurrentSurgeons.DataBind();
            mpe_AddSurgeons.Show();

            LoadSurgeonsDDL(null);
            }
        catch (Exception exEditProcedure)
        {
            orfid.Usp_WebErrors("BookingForm.aspx", "LoadSurgeons", DateTime.Now.AddDays(0), exEditProcedure.Message.ToString());
        }
        }
        protected void WipeSurgeonDetails()
        {
            pas_lbl_SurgeonName.Text = "Add New Surgeon:";
            pas_txt_ContactEmail.Text = "";
            pas_txt_SurgeonFamilyName.Text = "";
            pas_txt_SurgeonGivenName.Text = "";
            pas_txt_SurgeonTitle.Text = "";
        }

        protected void pas_dg_CurrentSurgeons_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(pas_dg_CurrentSurgeons, "Select$" + e.Row.RowIndex.ToString()));
                }
            }
            catch (Exception EGridrowdatabound)
            {
                int lakj = 1;
            }
        }

        protected void row_EditSurgeon_Click(object sender, EventArgs e)
        {
            EditSurgeonDisplay(pas_dg_CurrentSurgeons);
        }
        protected void EditSurgeonDisplay(GridView gv)
        {
            try
            {
                WipeSurgeonDetails();
                //pas_pnl_IncludingSurgeonsToHospitals.Enabled = true;
                //pas_pnl_IncludingSurgeonsToHospitals.ForeColor = Color.Black;
                Label lbl_hsurId = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_hsurId");
                Label lbl_accName = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_accName");
                Label lbl_accUsername = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_accUsername");
                Label lbl_drFamilyName = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_drFamilyName");
                Label lbl_drGivenName = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_drGivenName");
                Label lbl_drTitle = (Label)gv.SelectedRow.Cells[0].FindControl("lbl_drTitle");
                INCLUDESURID = RfidClass.strInt(lbl_hsurId.Text);
                pas_lbl_SurgeonName.Text = lbl_accName.Text;
                pas_txt_ContactEmail.Text = lbl_accUsername.Text;
                pas_txt_SurgeonFamilyName.Text = lbl_drFamilyName.Text;
                pas_txt_SurgeonGivenName.Text = lbl_drGivenName.Text;
                pas_txt_SurgeonTitle.Text = lbl_drTitle.Text;
            }
            catch (Exception Exgridviewindexchanged)
            {
                orfid.Usp_WebErrors("BookingForm.aspx", "row_IncludeThisSupplier_Click", DateTime.Now.AddDays(0), Exgridviewindexchanged.Message.ToString());
            }
        }
        protected void pas_lnkbtn_Add_Click(object sender, EventArgs e)
        {
            WipeSurgeonDetails();
        }
        protected void pas_btn_IncludeThisSurgeon_Click(object sender, EventArgs e)
        {
            rp_implantAddHospitalSurgeonResult result = orfid.rp_implantAddHospitalSurgeon(HOSPID, pas_txt_ContactEmail.Text, pas_txt_SurgeonFamilyName.Text, pas_txt_SurgeonGivenName.Text, pas_txt_SurgeonTitle.Text, ACCID).FirstOrDefault();
            LoadSurgeons();
            LoadSurgeonsDDL(result.hsurId.ToString());
        }
        protected void pas_lnkbtn_Done_Click(object sender, EventArgs e)
        {
            mpe_AddSurgeons.Hide();
        }
        #endregion
    #endregion

    #region BUTTONS
        protected void lnkbtn_Done_Click(object sender, EventArgs e)
    {
        WipeSupplierDetails();

        dg_IncludedSuppliers.SelectedIndex = -1;
        dg_RemainingSuppliers.SelectedIndex = -1;
        mpe_IncludedSuppliers.Hide();
    }
    protected void lnkbtn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Procedure.aspx", false);
    }
    protected void lnkbtn_deleteBooking_Click(object sender, EventArgs e)
    {
        orfid.rp_DeleteBooking(BOOKID);
        Response.Redirect("Procedure.aspx", false);
    }
    protected void lnkbtn_supplierACK_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["BookingEdit"] != null || Session["BookId"] != null)
        {
            var updaterevkitstatus = (from tbookingversion in orfid.tblImplantBookingVersions
                                      where tbookingversion.revBookId == int.Parse(Session["BookId"].ToString())
                                      select tbookingversion).Single();
            updaterevkitstatus.revlkitstatus = "Acknowledged";
            orfid.SubmitChanges();
            Response.Redirect("Procedure.aspx");
        }
    }
    #endregion

    #region NOTIFICATION
    public bool N_Ok_Vis
    {
        get
        {
            if (lnkbtn_N_Ok.CssClass == "DisplayNone")
                return false;
            else
                return true;
        }
        set
        {
            if (value)
                lnkbtn_N_Ok.CssClass = "DisplayBlock";
            else
                lnkbtn_N_Ok.CssClass = "DisplayNone";
        }
    }

    protected void lnkbtn_Notify_Click(object sender, EventArgs e)
    {
        N_Ok_Vis = false; 
    }
    protected void lnkbtn_N_Ok_Click(object sender, EventArgs e)
    {
        N_Ok_Vis = false;
        UpdateProcedure();
        mpe_Notify.Hide();
    }
    protected void lnkbtn_N_Close_Click(object sender, EventArgs e)
    {
        N_Ok_Vis = false;
        mpe_Notify.Hide();
    }
    #endregion
}