﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
public partial class Procedure : System.Web.UI.Page
{
    #region PAGE PROPERTIES
    private int SessINT(string n)
    {
        if (Session[n] != null)
        {
            return RfidClass.strInt(Session[n].ToString());
        }
        else
        {
            return -1;
        }
    }
    private Nullable<int> SessINTnull(string n)
    {
        if (Session[n] != null)
        {
            string bID = Session[n].ToString();
            return RfidClass.strInt(bID);
        }
        else
            return null;
    }
    public int HOSPID { get { return SessINT("HospID"); } set { Session["HospID"] = value; } }
    public int ACCID { get { return SessINT("accID"); } set { Session["accID"] = value; } }
    public Nullable<int> BOOKID { get { return SessINTnull("BookID"); } }
    public Nullable<int> SUPID { get { return SessINTnull("SupID"); } set { Session["SupID"] = value; } }
    #endregion
    #region PAGE SETUP
    RfidClass ORfidclass = new RfidClass();
    RFIDDataContext Orfid = new RFIDDataContext();
    RFID ClassRFID = new RFID();
    protected override void Render(HtmlTextWriter writer)
    {
        for (int grdrows = 0; grdrows <= GridView_list1.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(GridView_list1.UniqueID, "Select$" + grdrows.ToString());
        }
        for (int grdrows1 = 0; grdrows1 <= GridView_list2.Rows.Count; grdrows1++)
        {
            ClientScript.RegisterForEventValidation(GridView_list2.UniqueID, "Select$" + grdrows1.ToString());
        }
        for (int grdrows3 = 0; grdrows3 <= GridView_list3.Rows.Count; grdrows3++)
        {
            ClientScript.RegisterForEventValidation(GridView_list3.UniqueID, "Select$" + grdrows3.ToString());
        }
        for (int grdrows4 = 0; grdrows4 <= GridView_list4.Rows.Count; grdrows4++)
        {
            ClientScript.RegisterForEventValidation(GridView_list4.UniqueID, "Select$" + grdrows4.ToString());
        }
        for (int grdrows5 = 0; grdrows5 <= GridView_list5.Rows.Count; grdrows5++)
        {
            ClientScript.RegisterForEventValidation(GridView_list5.UniqueID, "Select$" + grdrows5.ToString());
        }
        for (int grdrows6 = 0; grdrows6 <= GridView_list6.Rows.Count; grdrows6++)
        {
            ClientScript.RegisterForEventValidation(GridView_list6.UniqueID, "Select$" + grdrows6.ToString());
        }
        for (int grdrows7 = 0; grdrows7 <= GridView_list7.Rows.Count; grdrows7++)
        {
            ClientScript.RegisterForEventValidation(GridView_list7.UniqueID, "Select$" + grdrows7.ToString());
        }

        //DAY
        for (int grdrowsDay = 0; grdrowsDay <= GridViewDay.Rows.Count; grdrowsDay++)
        {
            ClientScript.RegisterForEventValidation(GridViewDay.UniqueID, "Select$" + grdrowsDay.ToString());
        }

        //MONTH
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day1.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day1.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day2.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day2.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day3.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day3.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day4.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day4.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day5.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day5.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day6.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day6.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day7.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day7.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day8.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day8.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day9.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day9.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day10.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day10.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day11.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day11.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day12.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day12.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day13.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day13.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day14.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day14.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day15.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day15.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day16.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day16.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day17.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day17.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day18.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day18.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day19.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day19.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day20.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day20.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day21.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day21.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day22.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day22.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day23.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day23.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day24.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day24.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day25.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day25.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day26.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day26.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day27.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day27.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day28.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day28.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day29.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day29.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day30.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day30.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day31.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day31.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day32.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day32.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day33.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day33.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day34.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day34.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day35.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day35.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day36.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day36.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day37.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day37.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day38.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day38.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day39.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day39.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day40.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day40.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day41.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day41.UniqueID, "Select$" + grdrowsMonth.ToString());
        }
        for (int grdrowsMonth = 0; grdrowsMonth <= gvm_Day42.Rows.Count; grdrowsMonth++)
        {
            ClientScript.RegisterForEventValidation(gvm_Day42.UniqueID, "Select$" + grdrowsMonth.ToString());
        }

        base.Render(writer);
    }

    public String  REQDDAY 
    { 
        get
        {
            if (Session["Reqdday"] == null)
                Session["Reqdday"] = DateTime.Now.ToString();
            return Session["Reqdday"].ToString();
        } 
        set
        {
            if (Session["Reqdday"] != null)
                Session.Remove("Reqdday");
            Session["Reqdday"] = value;
        } 
    }

    #endregion
    #region PAGE LOAD AND DISPLAY
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack == false)
            {
                ClassRFID.StopReader();
                if (Session["HospID"] != null)
                    span_hospfilter.Visible = false;
                if (Session["SuplierID"] != null)
                    spanSupplier.Visible = false;

                var oSupplier = from Supp in Orfid.tblImplantSuppliers
                                select new
                                {
                                    SupplierID = Supp.supId,
                                    SupplierName = Supp.supName
                                };
                ddl_filter_Supplier.DataSource = oSupplier;
                ddl_filter_Supplier.DataTextField = "SupplierName";
                ddl_filter_Supplier.DataValueField = "SupplierID";
                ddl_filter_Supplier.DataBind();

                var oHospital = from hosp in Orfid.tblImplantHospitals
                                select new
                                {
                                    HospitalID = hosp.hospId,
                                    Hospitalname = hosp.hospName + "," + hosp.hospCampus
                                };
                ddl_filter_hosp.DataSource = oHospital;
                ddl_filter_hosp.DataTextField = "Hospitalname";
                ddl_filter_hosp.DataValueField = "HospitalID";
                ddl_filter_hosp.DataBind();
                Search_SessionState();

                if (Session["CalView"] != null)
                {
                    SetCalView(Session["CalView"].ToString());
                }
                else
                {
                    SetCalView("WEEK");
                }
                if (Session["staticOrderno"] != null)
                    Fun_OrderbyOrderno();

                if (Request.QueryString["QAlerts"] != null)
                {
                    var Accountreq = Orfid.rp_implantAccountRequests(int.Parse(Session["AccID"].ToString()), null);

                    int IntRequests = Accountreq.Count();
                    if (IntRequests > 0)
                    {
                        lbl_alerts.Text = IntRequests.ToString();
                        Panel_AdminAlerts.Visible = true;
                        Panel_AdminAlertsIFrame.Visible = true;
                    }
                }
                else
                {
                    Panel_AdminAlerts.Visible = false;
                    Panel_AdminAlertsIFrame.Visible = false;
                }

                //DYNAMIC TOOLTIPS
                DynamicTooltips("help\\Tooltips.xml", Page.ToString().ToLower());
            }
            else
            {
                changeINweek(0, null);
            }
        }
        catch (Exception EProcedure)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "Page Load", DateTime.Now.AddDays(0), EProcedure.Message.ToString());
        }
    }
        
    private void DynamicTooltips(String xaddress, String ForPage)
    {
        Xml x = new Xml();
        x.DocumentSource = xaddress;
        foreach (System.Xml.XmlNode i in x.Document.ChildNodes)
        {
            foreach (System.Xml.XmlNode item in i.ChildNodes)
            {
                String page = string.Empty, section = string.Empty, identity = string.Empty, tooltip = string.Empty;
                foreach (System.Xml.XmlNode xnode in item.ChildNodes)
                {
                    switch (xnode.Name)
                    {
                        case "page":
                            page = xnode.InnerText;
                            break;
                        case "section":
                            section = xnode.InnerText;
                            break;
                        case "identity":
                            identity = xnode.InnerText;
                            break;
                        case "tooltip":
                            tooltip = xnode.InnerText;
                            break;
                        default:
                            break;
                    }
                }

                if (page.ToLower() == ForPage && identity != "" && tooltip != "")
                {
                    var c = FindControlRecursive(Page, identity);
                    if (c != null)
                    {
                        try
                        {
                            ((WebControl)c).ToolTip = tooltip ;
                        }
                        catch (Exception)
                        {
                            switch (c.GetType().Name)
                            {
                                case "Panel":
                                    ((WebControl)c).ToolTip = tooltip;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
    private static Control FindControlRecursive(Control Root, string Id)
    {
        if (Root.ID == Id)
            return Root;
        foreach (Control Ctl in Root.Controls)
        {
            Control FoundCtl = FindControlRecursive(Ctl, Id);
            if (FoundCtl != null)

                return FoundCtl;
        }
        return null;
    }

    /// <summary>
    /// Initializes the visible grids when page first loads
    /// </summary>
    /// <param name="IntDaydisplay"></param>
    #endregion
    #region SITE NAVIGATION
    protected void btn_Process_Click(object sender, EventArgs e)
    {
        Response.Redirect("Users.aspx?qry=req");
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Panel_AdminAlerts.Visible = false;
        Panel_AdminAlertsIFrame.Visible = false;
    }
    protected void Imgbtn_new_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("BookingForm.aspx");
    }
    #endregion
    #region CALENDAR VIEW
    public void SetCalView(string v)
    {
        DateTime ReqdDay = DateTime.Parse(REQDDAY);
        switch (v)
        {
            case "WEEK":
                Session["CalView"] = "WEEK";
                monthView.Visible = false;
                weekView.Visible = true;
                dayView.Visible = false;
                lnkbtn_MonthToggle.CssClass = null;
                lnkbtn_WeekToggle.CssClass = "disabled-button";
                lnkbtn_DayToggle.CssClass = null ;
                break;
            case "DAY":
                Session["CalView"] = "DAY";
                monthView.Visible = false;
                weekView.Visible = false;
                dayView.Visible = true;
                lnkbtn_MonthToggle.CssClass = null;
                lnkbtn_WeekToggle.CssClass = null;
                lnkbtn_DayToggle.CssClass = "disabled-button";
                break;
            case "MONTH":
                Session["CalView"] = "MONTH";
                monthView.Visible = true;
                weekView.Visible = false;
                dayView.Visible = false;
                lnkbtn_MonthToggle.CssClass = "disabled-button";
                lnkbtn_WeekToggle.CssClass = null;
                lnkbtn_DayToggle.CssClass = null;
                break;
            default:
                break;
        }
        UpdateDateDisplay(ReqdDay);
        changeINweek(0, null);
    }
    //VIEW TOGGLES
    protected void lnkbtn_DayToggle_Click(object sender, EventArgs e)
    {
        SetCalView("DAY");
    }
    protected void lnkbtn_WeekToggle_Click(object sender, EventArgs e)
    {
        SetCalView("WEEK");
    }
    protected void lnkbtn_MonthToggle_Click(object sender, EventArgs e)
    {
        SetCalView("MONTH");
    }

    protected void UpdateDateDisplay(DateTime d)
    {
        switch (Session["CalView"].ToString())
        {
            case "WEEK":
                lbl_DateDisplay.Text = d.ToString("MMMM yyyy");
                break;
            case "DAY":
                lbl_DateDisplay.Text = d.ToString("dd MMM yyyy");
                break;
            case "MONTH":
                lbl_DateDisplay.Text = d.ToString("MMMM  yyyy");
                break;
            default:
                break;
        }
    }
    #endregion
    #region DATA RETRIEVAL
    // strorderno never used
    private void Gv_DataSource(string strFromDate, string strToDate, string strorderno, GridView Gv)
    {
        string StrHospID = "";
        string StrSupplierID = "";

        if (Session["HospID"] != null)
        {
            if (Session["HospID"] != "")
            {
                StrHospID = Session["HospID"].ToString();
            }
        }
        if (Session["SuplierID"] != null)
        {
            if (Session["SuplierID"] != "")
            {
                StrSupplierID = Session["SuplierID"].ToString();
            }
        }
        try
        {
            // it has to be null inorder not to get confused with Search using Orderid
            DataSet ODs = ORfidclass.ProcedureDatasource(strFromDate, strToDate, ACCID, null, StrHospID, StrSupplierID);// it has to be null inorder not to get confused with Search using Orderid
            Gv.DataSource = ODs.Tables[0];
            Gv.DataBind();
        }
        catch (Exception EDatasource)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "Gv_DataSource", DateTime.Now.AddDays(0), EDatasource.Message.ToString());
        }
    }
    private void Fun_OrderbyOrderno()
    {
        try
        {
            string StrHospID = "";
            string StrSupplierID = "";
            string StrOrderno = "";

            if (Session["HospID"] != null)
            {
                if (Session["HospID"] != "")
                {
                    StrHospID = Session["HospID"].ToString();
                }
            }
            if (Session["SuplierID"] != null)
            {
                if (Session["SuplierID"] != "")
                {
                    StrSupplierID = Session["SuplierID"].ToString();
                }
            }
            if (Session["staticOrderno"] != null)
            {
                StrOrderno = Session["staticOrderno"].ToString();
            }
            DataSet DsOrder = ORfidclass.ProcedureDatasource(null, null, ACCID, StrOrderno, StrHospID, StrSupplierID);
            string OrderDate = DsOrder.Tables[0].Rows[0]["ordDttm"].ToString();
            changeINweek(0, OrderDate);
        }
        catch (Exception EbtnSearch)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "Imgbtn_Search_Click", DateTime.Now.AddDays(0), EbtnSearch.Message.ToString());
        }
    }
    private void changeINweek(int PNDays, string PDay)/*No of Days*/
    {
        try
        {
            int IntDay = 0;
            if (PDay == null)
                PDay = REQDDAY;
            DateTime PresentDay = DateTime.Parse(PDay);

            string Priordate = PresentDay.Date.AddDays(PNDays).DayOfWeek.ToString();
            IntDay = ORfidclass.CompareDay(Priordate);
            DateTime ReqdDay = PresentDay.Date.AddDays(PNDays);
            
            string NameRoot = "dow";
            string NameRoot2 = "lbl_day";
            if (weekView.Visible == true)
            {
                for (int i = 1; i <= 7; i++)
                {
                    Label dow = (Label)RfidClass.FindControlRecursive(Page, NameRoot + i);
                    LinkButton lbl_day = (LinkButton)RfidClass.FindControlRecursive(Page, NameRoot2 + i);
                    dow.Text = ReqdDay.AddDays(IntDay + i - 1).ToString("dd/MMM/yyyy");
                    lbl_day.Text = ReqdDay.AddDays(IntDay + i - 1).ToString("dd MMM yyyy");
                }
            }
            //DAY 
            if (dayView.Visible == true)
            {
                lbl_current_day.Text = PresentDay.AddDays(PNDays).ToString("dd/MMM/yyyy");
            }

            //MONTH
            //assign month day numbers            //work out what offset day1 is and count up from that
            if (monthView.Visible == true)
            {
                IntDay = ORfidclass.CompareDay(ReqdDay.AddDays(-ReqdDay.Day).DayOfWeek.ToString());
                IntDay = IntDay - ReqdDay.Day;

                NameRoot = "dom";
                NameRoot2 = "lbl_dom";
                for (int i = 1; i <= 42; i++)
                {
                    Label dom = (Label)RfidClass.FindControlRecursive(Page, NameRoot + i);
                    LinkButton lbl_dom = (LinkButton)RfidClass.FindControlRecursive(Page, NameRoot2 + i);
                    dom.Text = ReqdDay.AddDays(IntDay + i - 1).ToString("dd/MMM/yyyy");
                    lbl_dom.Text = ReqdDay.AddDays(IntDay + i - 1).ToString("dd");
                }

                NameRoot = "mcd";
                for (int i = 1; i <= 42; i++)
                {
                    Panel testP = (Panel)RfidClass.FindControlRecursive(Page, NameRoot + i);
                    HighlightCurrentDay(testP, ReqdDay, ReqdDay.AddDays(IntDay + i - 1));
                    ShadeOutOfMonthDay(testP, ReqdDay, ReqdDay.AddDays(IntDay + i - 1));
                }
            }
            GridviewDisplay("");
            UpdateDateDisplay(ReqdDay);
            REQDDAY = PresentDay.AddDays(PNDays).ToString();
        }
        catch (Exception EChangeInWeek)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "changeINweek", DateTime.Now.AddDays(0), EChangeInWeek.Message.ToString());
        }
    }
    private void ShadeOutOfMonthDay(Panel P, DateTime TargetDate, DateTime Day)
    {
        if (Day.Month != TargetDate.Month)
        {
            P.BackColor  = System.Drawing.Color.LightGray ;
        }
    }
    private void HighlightCurrentDay(Panel P, DateTime TargetDate, DateTime Day)
    {
        if (Day == TargetDate)
            P.BackColor = System.Drawing.Color.FromArgb(198, 234,235);
        else
            P.BackColor = System.Drawing.Color.Transparent;
    }
    private void GridviewDisplay(string StrOrderID)
    {
        try
        {
            string NameRoot = string.Empty;
            string NameRoot2 = string.Empty;
            if (StrOrderID == "")
                StrOrderID = null;
            if (weekView.Visible == true)
            {
                NameRoot = "dow";
                NameRoot2 = "GridView_list";
                for (int i = 1; i <= 7; i++)
                {
                    Label lbl_day = (Label)RfidClass.FindControlRecursive(Page, NameRoot + i);
                    GridView GridView_List = (GridView)RfidClass.FindControlRecursive(Page, NameRoot2 + i);
                    Gv_DataSource(lbl_day.Text, lbl_day.Text, StrOrderID, GridView_List);
                }
            }
            // DAY
            if (dayView.Visible == true)
            {
                Gv_DataSource(lbl_current_day.Text, lbl_current_day.Text, StrOrderID, GridViewDay);
            }
            // MONTH
            if (monthView.Visible == true)
            {
                NameRoot = "dom";
                NameRoot2 = "gvm_Day";
                for (int i = 1; i <= 42; i++)
                {
                    Label lbl_day = (Label)RfidClass.FindControlRecursive(Page, NameRoot + i);
                    GridView GridView_List = (GridView)RfidClass.FindControlRecursive(Page, NameRoot2 + i);
                    Gv_DataSource(lbl_day.Text, lbl_day.Text, StrOrderID, GridView_List);
                }
            }
        }
        catch (Exception EDisplay)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "GridviewDisplay", DateTime.Now.AddDays(0), EDisplay.Message.ToString());
        }
    }
    /// <summary>
    /// Assigns values to the lables of each grid, and filters contents if running a filter
    /// </summary>
    /// <param name="e"></param>
    /// <param name="GV"></param>
    private void Gv_RowDataBound(GridViewRowEventArgs e, GridView GV)
    {
        if ((e.Row.DataItemIndex == -1))
        {
            return;
        }
        Panel panel_1 = (Panel)e.Row.Cells[0].FindControl("Panel_cell1");
        Label lblHospname = (Label)e.Row.Cells[0].FindControl("lbl_hospname");
        Label lblSuppname = (Label)e.Row.Cells[0].FindControl("lbl_supplier");
        Label lblpatient = (Label)e.Row.Cells[0].FindControl("lbl_patient");
        Label lblstatus = (Label)e.Row.Cells[0].FindControl("lbl_status");
        Label lblDoctor = (Label)e.Row.Cells[0].FindControl("lbl_Doctor");
        Label lbl_orderBknumber = (Label)e.Row.Cells[0].FindControl("lbl_orderBookingnumber");
        /*Blank Labels*/
        Label lblHospname_blank = (Label)e.Row.Cells[0].FindControl("lbl_hospname");
        Label lblSuppname_blank = (Label)e.Row.Cells[0].FindControl("lbl_suppname");
        string Strpatient = lblpatient.Text + "(" + lbl_orderBknumber.Text + ")";
        lblpatient.Text = Strpatient;

        //Detailed items
        if (e.Row.Cells[0].FindControl("lbl_Notes") != null)
        {
            Label lblNotes = (Label)e.Row.Cells[0].FindControl("lbl_Notes");
            Label lblProcessedBy = (Label)e.Row.Cells[0].FindControl("lbl_ProcessedBy");
            Label lblKitType = (Label)e.Row.Cells[0].FindControl("lbl_kitType");
            Label lblCaseType = (Label)e.Row.Cells[0].FindControl("lbl_caseType");
            Label lblKitSide = (Label)e.Row.Cells[0].FindControl("lbl_kitSide");
            Label lblOpType = (Label)e.Row.Cells[0].FindControl("lbl_opType");
            Label lblPtUR = (Label)e.Row.Cells[0].FindControl("lbl_ptUR");
            Label lblPtSex = (Label)e.Row.Cells[0].FindControl("lbl_ptSex");
        }
        if (e.Row.Cells[0].FindControl("lbl_opTime") != null)
        {
            Label lblOpTime = (Label)e.Row.Cells[0].FindControl("lbl_opTime");
            if (lblOpTime.Text == ":")
            { lblOpTime.Visible = false; }
            else { lblOpTime.Visible = true; }
        }

        /*To make sure Hospital information is not displayed when the user is a Hospital user*/
        if (Session["HospID"] != null)
        {
            lblHospname.Visible = false;
            lblHospname_blank.Visible = false;
        }
        //span_hospfilter.Visible = false;
        /*To make sure Supplier information is not displayed when the user is a Supplier*/
        if (Session["SuplierID"] != null)
        {
            lblSuppname.Visible = false;
            lblSuppname_blank.Visible = false;
        }
        //MONTH
        if (Session["calView"].ToString() == "MONTH")
        {
            ((Label)e.Row.Cells[0].FindControl("lbl_opTimeValue")).Visible = false;
            ((Label)e.Row.Cells[0].FindControl("lbl_doctorname")).Visible = false;
            ((Label)e.Row.Cells[0].FindControl("lbl_patientname")).Visible = true;
            if (Session["SuplierID"] != null)
                ((Label)e.Row.Cells[0].FindControl("lbl_hospname")).Visible = true;
            if (Session["HospID"] != null)
                ((Label)e.Row.Cells[0].FindControl("lbl_suppname")).Visible = true;
        }

        //DAY
        Label Stat = (Label)e.Row.Cells[0].FindControl("lbl_status");
        string S = Stat.Text.Replace(" ", "");

        RfidClass.PaintPanelsByStatus(panel_1, S);

        if (Page.IsPostBack == true)
        {
            if (ddl_fileterby.SelectedIndex != 0)
            {
                Session["Pro_status"] = ddl_fileterby.SelectedValue.ToString();
                if (ddl_fileterby.SelectedItem.Text != lblstatus.Text)
                    e.Row.Visible = false;
            }
            if (ddl_filter_Supplier.SelectedIndex != 0)
            {
                Session["Pro_Supplier"] = ddl_filter_Supplier.SelectedItem.ToString();
                if (ddl_filter_Supplier.SelectedItem.Text != lblSuppname.Text)
                {
                    e.Row.Visible = false;
                }
            }
            if (ddl_filter_hosp.SelectedIndex != 0)
            {
                Session["Pro_hospital"] = ddl_filter_hosp.SelectedValue.ToString();
                if (ddl_filter_hosp.SelectedItem.Text != lblHospname.Text)
                {
                    e.Row.Visible = false;
                }
            }
            if (txt_filter_doctor.Text != "")
            {
                string StrDocFamilyname = lblDoctor.Text.ToLower();
                StrDocFamilyname = StrDocFamilyname.Substring(0, (StrDocFamilyname.Length - (StrDocFamilyname.Length - StrDocFamilyname.IndexOf(','))));
                txt_filter_doctor.Text = txt_filter_doctor.Text.ToLower();

                if (!StrDocFamilyname.StartsWith(txt_filter_doctor.Text))
                    e.Row.Visible = false;
            }
            if (txt_filter_Patient.Text != "")
            {
                string strPatfamilyname = lblpatient.Text.ToUpper();
                strPatfamilyname = strPatfamilyname.Substring(0, (strPatfamilyname.Length - (strPatfamilyname.Length - strPatfamilyname.IndexOf('('))));

                if (!strPatfamilyname.Contains(txt_filter_Patient.Text.ToUpper()))
                    e.Row.Visible = false;
            }
            if (txt_filter_bookingno.Text != "")
            {
                string Strsuppref = lblpatient.Text;

                Strsuppref = Strsuppref.Substring(Strsuppref.IndexOf('(') + 1, ((Strsuppref.Length - Strsuppref.IndexOf('(')) - (Strsuppref.Length - Strsuppref.IndexOf(')'))) - 1);
                Strsuppref = Strsuppref.Replace("#", "");
                txt_filter_bookingno.Text = txt_filter_bookingno.Text.Replace("#", "");
                if (txt_filter_bookingno.Text != Strsuppref)
                    e.Row.Visible = false;
            }
        }
        e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GV, "Select$" + e.Row.RowIndex.ToString()));
    }
    private void GV_Selectindexchanged(GridView GV_List)
    {
        try
        {
            Panel Panel_1 = (Panel)GV_List.SelectedRow.Cells[0].FindControl("Panel_cell1");
            Label LblOrderno = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_orderno");
            Label lblOrdID = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_orderID");
            Label lblStatus = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_status");
            Label lblHospname = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_Hospital");
            Guid GorderID = new Guid(lblOrdID.Text);
            if (GV_List.SelectedRow.Cells[0].FindControl("lbl_Notes") != null)
            {
                Label lblNotes = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_Notes");
                Label lblProcessedBy = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_ProcessedBy");
                Label lblKitType = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_kitType");
                Label lblCaseType = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_caseType");
                Label lblKitSide = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_kitSide");
                Label lblOpType = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_opType");
                Label lblOpTime = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_opTime");
                Label lblPtUR = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_ptUR");
                Label lblPtSex = (Label)GV_List.SelectedRow.Cells[0].FindControl("lbl_ptSex");
            }
            Session["staticOrderno"] = LblOrderno.Text;
            if (Session["SuplierID"] != null)
            {
                Session["ProcedureHosp"] = (lblHospname != null) ? lblHospname.Text : "";   // this hospital is solely used for bookings
            }
            if (Session["Reqdday"] != null)
                Session.Remove("Reqdday");
            if (lblStatus.Text.ToUpper() == "NEW BOOKING" || lblStatus.Text.ToUpper() == "ACKNOWLEDGED")
            {           // Below procedure is to retrieve the BookingID using GorderID
                string StrBookingID = "";
                var BookingID = from tblbooking in Orfid.tblImplantBookings
                                where tblbooking.bookOrderId == GorderID
                                select new { BookID = tblbooking.bookId };
                foreach (var obookid in BookingID)
                {
                    StrBookingID = obookid.BookID.ToString();
                }
                Response.Redirect("BookingForm.aspx?BookingEdit=" + StrBookingID, false);
            }
            else
                Response.Redirect("scan.aspx?ordID=" + lblOrdID.Text, false);
        }
        catch (Exception Exgridviewindexchanged)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "GV_Selectindexchanged", DateTime.Now.AddDays(0), Exgridviewindexchanged.Message.ToString());
        }
    }
    #endregion
    #region NAVIGATION BUTTON BAR
    protected void Imgbtn_prevweek_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(-7, lbl_day1.Text);
    }
    protected void Lnkbtn_prevweek_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(-7, lbl_day1.Text);
    }
    protected void Imgbtn_nextweek_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(7, lbl_day1.Text);
    }
    protected void Lnkbtn_nextweek_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(7, lbl_day1.Text);
    }
    protected void Imgbtn_prevday_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(-1, lbl_current_day.Text);
    }
    protected void Lnkbtn_prevday_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(-1, lbl_current_day.Text);
    }
    protected void Imgbtn_nextday_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(1, lbl_current_day.Text);
    }
    protected void Lnkbtn_nextday_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        changeINweek(1, lbl_current_day.Text);
    }

    protected void Lnkbtn_prev_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        switch (Session["CalView"].ToString())
        {
            case "WEEK":
                changeINweek(-7, null);
                break;
            case "DAY":
                changeINweek(-1, null);
                break;
            case "MONTH":
                changeINweek(-30, null);
                break;
            default:
                break;
        }
    }
    protected void Lnkbtn_next_Click(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        switch (Session["CalView"].ToString())
        {
            case "WEEK":
                changeINweek(7, null);
                break;
            case "DAY":
                changeINweek(1, null);
                break;
            case "MONTH":
                changeINweek(30, null);
                break;
            default:
                break;
        }
    }
    #endregion
    #region FILTER
    protected void Lnkbtn_search_Click(object sender, EventArgs e)
    {
        try
        {
            string dtchweek = "";
            string strpat = null;
            string strdoc = null;
            string strbkno = null;
            Session["strpat"] = strpat = txt_filter_Patient.Text.ToLower().Replace(" ", "");
            Session["strdoc"] = strdoc = txt_filter_doctor.Text.ToLower().Replace(" ", "");
            Session["strbkno"] = strbkno = txt_filter_bookingno.Text.ToLower().Replace(" ", "");
            if (Session["staticOrderno"] != null)
                Session.Remove("staticOrderno");
            if (txt_Todate.Text != "")
                changeINweek(0, txt_Todate.Text);
            else
                //if (strpat != "" || strdoc != "" || strbkno != "")
                //{
                //    if (strpat == "")
                //        strpat = null;
                //    if (strdoc == "")
                //        strdoc = null;
                //    if (strbkno == "")
                //        strbkno = null;
                //    var vsearch = Orfid.usp_search_Procedure(strpat, strdoc, strbkno, null);
                //    foreach (var search in vsearch)
                //    {
                //        dtchweek = search.OperationDate.ToString();
                //    }
                //    if (dtchweek != "")
                //    {
                //        changeINweek(0, dtchweek.ToString());
                //        lbl_SearchResults.Text = "";
                //    }
                //    else
                //        lbl_SearchResults.Text = "No Results Found";
                //}
                //else
                //{
                changeINweek(0, null);
            lbl_SearchResults.Text = "";
            //}
        }
        catch (Exception EbnSearch)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "lnkbtn_Search_Click", DateTime.Now.AddDays(0), EbnSearch.Message.ToString());
        }
    }

    protected void ddl_fileterby_SelectedIndexChanged(object sender, EventArgs e)
    {
        changeINweek(0, null);
    }
    protected void ddl_filter_Supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        changeINweek(0, null);
    }
    protected void ddl_filter_hosp_SelectedIndexChanged(object sender, EventArgs e)
    {
        changeINweek(0, null);
    }
    protected void txt_Todate_TextChanged(object sender, EventArgs e)
    {
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        if (Session["Reqdday"] != null)
            Session.Remove("Reqdday");
        changeINweek(0, txt_Todate.Text);
    }

    protected void lnkbtn_Reset_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["staticOrderno"] != null)
                Session.Remove("staticOrderno");
            if (Session["Reqdday"] != null)
                Session["Reqdday"] = DateTime.Now.ToString();
            if (Session["strpat"] != null)
                Session.Remove("strpat");
            if (Session["strdoc"] != null)
                Session.Remove("strdoc");
            if (Session["strbkno"] != null)
                Session.Remove("strbkno");
            if (Session["Pro_status"] != null)
                Session.Remove("Pro_status");
            if (Session["Pro_Supplier"] != null)
                Session.Remove("Pro_Supplier");
            if (Session["Pro_hospital"] != null)
                Session.Remove("Pro_hospital");
            Response.Redirect("Procedure.aspx", false);
        }
        catch (Exception EbtnReset)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "lnkbtn_Reset_Click", DateTime.Now.AddDays(0), EbtnReset.Message.ToString());
        }
    }
    private void Search_SessionState()
    {
        try
        {
            if (Session["strpat"] != null)
                txt_filter_Patient.Text = Session["strpat"].ToString();
            if (Session["strdoc"] != null)
                txt_filter_doctor.Text = Session["strdoc"].ToString();
            if (Session["strbkno"] != null)
                txt_filter_bookingno.Text = Session["strbkno"].ToString();
            if (Session["Pro_status"] != null)
                ddl_fileterby.SelectedValue = Session["Pro_status"].ToString();
            if (Session["Pro_Supplier"] != null)
                ddl_filter_Supplier.SelectedValue = Session["Pro_Supplier"].ToString();
            if (Session["Pro_hospital"] != null)
                ddl_filter_hosp.SelectedValue = Session["Pro_hospital"].ToString();
        }
        catch (Exception ESessionState)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "Search_SessionState", DateTime.Now.AddDays(0), ESessionState.Message.ToString());
        }
    }
    #endregion
    #region WEEK
    #region WEEK - RowDataBound, SelectedIndexChanged
    protected void GridView_list1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list1);
    }
    protected void GridView_list1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list1);
    }
    protected void GridView_list2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list2);
    }
    protected void GridView_list2_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list2);
    }
    protected void GridView_list3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list3);
    }
    protected void GridView_list3_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list3);
    }
    protected void GridView_list4_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list4);
    }
    protected void GridView_list4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list4);
    }
    protected void GridView_list5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list5);
    }
    protected void GridView_list5_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list5);
    }
    protected void GridView_list6_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list6);
    }
    protected void GridView_list6_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list6);
    }
    protected void GridView_list7_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridView_list7);
    }
    protected void GridView_list7_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(GridView_list7);
    }
    #endregion
    #endregion
    #region DAY
    //GridViewDay Code
    protected void GridViewDay_list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, GridViewDay);
    }
    protected void GridViewDay_list_SelectedIndexChanged(object sender, EventArgs e) 
    {
        GV_Selectindexchanged(GridViewDay);
    }

    #region DAYVIEW TOGGELS
    private void ToggleSingleDayView(object sender, EventArgs e)
    {
        switch (sender.GetType().Name )
        {
            case "LinkButton":
                {
                    LinkButton s = (LinkButton)sender;
                    changeINweek(0, s.Text);
                    break;
                }
            case "Label":
                {
                    Label s = (Label)sender;
                    changeINweek(0, s.Text);
                    break;
                }
            default:
                break;
        }
        SetCalView("DAY");
    }
    protected void lbl_day1_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day2_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day3_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day4_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day5_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day6_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    protected void lbl_day7_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }
    #endregion

    protected void Lnkbtn_current_day_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(sender, e);
    }

    protected void lnkbtn_Scan_Click(object sender, EventArgs e)
    {
        string strpat = null;
        string strdoc = null;
        string strbkno = null;
        Session["strpat"] = strpat = txt_filter_Patient.Text.ToLower().Replace(" ", "");
        Session["strdoc"] = strdoc = txt_filter_doctor.Text.ToLower().Replace(" ", "");
        Session["strbkno"] = strbkno = txt_filter_bookingno.Text.ToLower().Replace(" ", "");
        if (Session["staticOrderno"] != null)
            Session.Remove("staticOrderno");
        if (Session["Reqdday"] != null)
            Session.Remove("Reqdday");

        lbl_current_day.Text = "";
        string startdate = DateTime.MinValue.AddYears(1800).ToString("dd/MMM/yyyy");
        string enddate = DateTime.MaxValue.AddDays(-1).ToString("dd/MMM/yyyy");
        string StrHospID = "";
        string StrSupplierID = "";

        if (Session["HospID"] != null)
        {
            if (Session["HospID"] != "")
            {
                StrHospID = Session["HospID"].ToString();
            }
        }
        if (Session["SuplierID"] != null)
        {
            if (Session["SuplierID"] != "")
            {
                StrSupplierID = Session["SuplierID"].ToString();
            }
        }

        try
        {
            // it has to be null inorder not to get confused with Search using Orderid
            DataSet ODs = ORfidclass.ProcedureDatasource(startdate, enddate, ACCID, null, StrHospID, StrSupplierID);// it has to be null inorder not to get confused with Search using Orderid
            GridViewDay.DataSource = ODs.Tables[0];
            GridViewDay.DataBind();
        }
        catch (Exception EDatasource)
        {
            Orfid.Usp_WebErrors("Procedure.aspx", "Gv_DataSource", DateTime.Now.AddDays(0), EDatasource.Message.ToString());
        }
    }
    #endregion
    #region MONTH
    #region MONTH - RowDataBount, SelectedIndexChanged
    protected void gvm_Day1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day1);
    }
    protected void gvm_Day1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day1);
    }
    protected void gvm_Day2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day2);
    }
    protected void gvm_Day2_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day2);
    }
    protected void gvm_Day3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day3);
    }
    protected void gvm_Day3_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day3);
    }
    protected void gvm_Day4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day4);
    }
    protected void gvm_Day4_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day4);
    }
    protected void gvm_Day5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day5);
    }
    protected void gvm_Day5_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day5);
    }
    protected void gvm_Day6_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day6);
    }
    protected void gvm_Day6_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day6);
    }
    protected void gvm_Day7_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day7);
    }
    protected void gvm_Day7_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day7);
    }
    protected void gvm_Day8_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day8);
    }
    protected void gvm_Day8_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day8);
    }
    protected void gvm_Day9_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day9);
    }
    protected void gvm_Day9_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day9);
    }
    protected void gvm_Day10_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day10);
    }
    protected void gvm_Day10_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day10);
    }
    protected void gvm_Day11_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day11);
    }
    protected void gvm_Day11_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day11);
    }
    protected void gvm_Day12_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day12);
    }
    protected void gvm_Day12_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day12);
    }
    protected void gvm_Day13_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day13);
    }
    protected void gvm_Day13_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day13);
    }
    protected void gvm_Day14_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day14);
    }
    protected void gvm_Day14_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day14);
    }
    protected void gvm_Day15_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day15);
    }
    protected void gvm_Day15_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day15);
    }
    protected void gvm_Day16_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day16);
    }
    protected void gvm_Day16_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day16);
    }
    protected void gvm_Day17_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day17);
    }
    protected void gvm_Day17_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day17);
    }
    protected void gvm_Day18_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day18);
    }
    protected void gvm_Day18_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day18);
    }
    protected void gvm_Day19_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day19);
    }
    protected void gvm_Day19_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day19);
    }
    protected void gvm_Day20_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day20);
    }
    protected void gvm_Day20_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day20);
    }
    protected void gvm_Day21_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day21);
    }
    protected void gvm_Day21_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day21);
    }
    protected void gvm_Day22_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day22);
    }
    protected void gvm_Day22_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day22);
    }
    protected void gvm_Day23_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day23);
    }
    protected void gvm_Day23_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day23);
    }
    protected void gvm_Day24_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day24);
    }
    protected void gvm_Day24_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day24);
    }
    protected void gvm_Day25_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day25);
    }
    protected void gvm_Day25_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day25);
    }
    protected void gvm_Day26_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day26);
    }
    protected void gvm_Day26_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day26);
    }
    protected void gvm_Day27_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day27);
    }
    protected void gvm_Day27_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day27);
    }
    protected void gvm_Day28_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day28);
    }
    protected void gvm_Day28_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day28);
    }
    protected void gvm_Day29_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day29);
    }
    protected void gvm_Day29_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day29);
    }
    protected void gvm_Day30_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day30);
    }
    protected void gvm_Day30_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day30);
    }
    protected void gvm_Day31_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day31);
    }
    protected void gvm_Day31_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day31);
    }
    protected void gvm_Day32_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day32);
    }
    protected void gvm_Day32_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day32);
    }
    protected void gvm_Day33_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day33);
    }
    protected void gvm_Day33_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day33);
    }
    protected void gvm_Day34_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day34);
    }
    protected void gvm_Day34_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day34);
    }
    protected void gvm_Day35_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day35);
    }
    protected void gvm_Day35_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day35);
    }
    protected void gvm_Day36_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day36);
    }
    protected void gvm_Day36_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day36);
    }
    protected void gvm_Day37_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day37);
    }
    protected void gvm_Day37_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day37);
    }
    protected void gvm_Day38_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day38);
    }
    protected void gvm_Day38_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day38);
    }
    protected void gvm_Day39_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day39);
    }
    protected void gvm_Day39_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day39);
    }
    protected void gvm_Day40_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day40);
    }
    protected void gvm_Day40_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day40);
    }
    protected void gvm_Day41_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day41);
    }
    protected void gvm_Day41_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day41);
    }
    protected void gvm_Day42_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Gv_RowDataBound(e, gvm_Day42);
    }
    protected void gvm_Day42_SelectedIndexChanged(object sender, EventArgs e)
    {
        GV_Selectindexchanged(gvm_Day42);
    }
#endregion
    #region MONTH - DayToggle
    protected object FindAnchorControl_dom(object sender)
    {
        LinkButton s = (LinkButton)sender;
        var n = s.ID.Replace ("lbl_","");
        Label dom = (Label)RfidClass.FindControlRecursive(Page, n);
        return dom;
    }

    protected void lbl_dom1_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom2_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom3_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom4_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom5_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom6_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom7_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom8_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom9_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom10_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom11_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom12_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom13_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom14_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom15_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom16_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom17_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom18_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom19_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom20_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom21_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom22_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom23_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom24_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom25_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom26_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom27_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom28_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom29_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom30_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom31_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom32_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom33_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom34_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom35_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom36_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom37_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom38_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom39_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom40_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom41_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    protected void lbl_dom42_Click(object sender, EventArgs e)
    {
        ToggleSingleDayView(FindAnchorControl_dom(sender), e);
    }
    #endregion
    #endregion
}
    