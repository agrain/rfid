﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Users : System.Web.UI.Page
{
    RFIDDataContext orfid = new RFIDDataContext();
    RFID ClassRFID = new RFID();
    static string Conn = ConfigurationManager.AppSettings["RfidConnection"].ToString();
    /// <>
    /// Page load event sole depends up on the querystring defined
    /// Rule1:NewUser
    /// No matter what New user must be shown only Panel_newuser and other 2 panels are always invisible
    /// Inorder to fulfil that we need pass a querystring    Signup
    /// 
    /// Admin Account
    /// Should be able to see all the Outstanding Requests
    /// when AccID is passed it should come up with Panel_grid(No Matter what Panel_NewUser must not be visible)
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    RfidClass ORfidclass = new RfidClass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.ServerVariables["http_referer"] != null)
        {
            try
            {
                if (Page.IsPostBack == false)
                {
                    ClassRFID.StopReader();
                    if (Request.QueryString["Signup"] != null)//NewUser
                    {
                        Panel_NewUser.Visible = true;
                        Panel_gridrequests.Visible = false;

                        // Supplier
                        var oSupplier = from Supp in orfid.tblImplantSupplierWarehouses
                                        select new
                                        {
                                            WarehouseID = Supp.whId,
                                            WareHousename = Supp.whName
                                        };
                        ddl_Supplier.DataSource = oSupplier;
                        ddl_Supplier.DataTextField = "WareHousename";
                        ddl_Supplier.DataValueField = "WarehouseID";
                        ddl_Supplier.DataBind();
                        // Hospital and Campus
                        var oHospital = from hosp in orfid.tblImplantHospitals
                                        select new
                                        {
                                            HospitalID = hosp.hospId,
                                            Hospitalname = hosp.hospName + "," + hosp.hospCampus
                                        };
                        ddl_hospital.DataSource = oHospital;
                        ddl_hospital.DataValueField = "HospitalID";
                        ddl_hospital.DataTextField = "Hospitalname";
                        ddl_hospital.DataBind();
                        // Roles
                        var Orole = from Role in orfid.tblImplantRoles
                                    select new { RoleID = Role.roleId, Rolename = Role.roleName };
                        chk_Roles.DataSource = Orole;
                        chk_Roles.DataValueField = "RoleID";
                        chk_Roles.DataTextField = "Rolename";
                        chk_Roles.DataBind();


                    }
                    //AccountId is visible and 


                    if (Request.QueryString["qry"] != null)
                    {
                        if (Request.QueryString["qry"].ToString() == "req")
                        {
                            Panel_NewUser.Visible = false;
                            Panel_gridrequests.Visible = true;
                            Grid_ListofUsers();
                            GridRequests();
                        }
                        else
                            if (Request.QueryString["qry"].ToString() == "Admin")
                            {
                                Panel_NewUser.Visible = false;
                                Panel_gridrequests.Visible = true;
                                Grid_ListofUsers();
                                GridRequests();
                            }
                    }
                }
            }
            catch (Exception ExPageload)
            {
                orfid.Usp_WebErrors("Users.aspx", "Page_Load", DateTime.Now.AddDays(0), ExPageload.Message.ToString());
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    private void Grid_ListofUsers()
    {
        try
        {
            var Listusers = orfid.rp_implantAdminUsers(int.Parse(Session["AccID"].ToString()));
            Grd_ListUsers.DataSource = Listusers;
            Grd_ListUsers.Columns[1].Visible = true;
            Grd_ListUsers.DataBind();
            Grd_ListUsers.Columns[1].Visible = false;
            int NoRows = int.Parse(Grd_ListUsers.Rows.Count.ToString());
            if (NoRows > 0)
                div_Listusers.Visible = true;
            else
                div_Listusers.Visible = false;
        }
        catch (Exception Elistusers)
        {
            orfid.Usp_WebErrors("Users.aspx", "Grid_ListofUsers", DateTime.Now.AddDays(0), Elistusers.Message.ToString());
        }
    }

    private void GridRequests()
    {
        try
        {
            int intAccID = int.Parse(Session["AccID"].ToString());
            var AccRequests = orfid.rp_implantAccountRequests(intAccID, null);
            Grid_requests.DataSource = AccRequests;
            Grid_requests.Columns[0].Visible = true;
            Grid_requests.Columns[1].Visible = true;
            Grid_requests.DataBind();
            Grid_requests.Columns[0].Visible = false;
            Grid_requests.Columns[1].Visible = false;
            int GridRows = Grid_requests.Rows.Count;
            if (GridRows > 0)
                div_listrequests.Visible = true;
            else
                div_listrequests.Visible = false;
        }
        catch (Exception EGridRequests)
        {
            orfid.Usp_WebErrors("Users.aspx", "GridRequests", DateTime.Now.AddDays(0), EGridRequests.Message.ToString());
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        for (int grdrows = 0; grdrows <= Grid_requests.Rows.Count - 1; grdrows++)
        {
            ClientScript.RegisterForEventValidation(Grid_requests.UniqueID, "Select$" + grdrows.ToString());
        }
        base.Render(writer);
    }
    /// <summary>
    /// Creating a Random Password
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"> none</param>
    /// Creating an AccountID
    /// An AccountId is created based on the Username, Random Password generated
    /// and displayname
    /// Request Access Method takes AccountID,Hospital ID and roles(or) Supplier ID
    //protected void btn_Create_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int SupplierID = -1;
    //        string StrDisplayname = "";
    //        string StrAdminAddress = "";
    //        string StrRandomPwd = "";
    //        string StrAccid = "";
    //        string MailAddress = "";
    //        int RequestID = 0;
    //        // Creating a Random Password.
    //        StrDisplayname = txt_Firstname.Text + " " + txt_Lastname.Text;

    //        var Randompwd = orfid.rp_implantRandomPassword();
    //        foreach (var pwd in Randompwd)
    //        {
    //            StrRandomPwd = pwd.RandomPassword.ToString();
    //        }
    //        //Getting the Account ID

    //        var AccIDReturn = orfid.rp_implantAccountAdd(txt_Username.Text, StrRandomPwd, StrDisplayname);
    //        foreach (var accid in AccIDReturn)
    //        {
    //            StrAccid = accid.AccountId.Value.ToString();
    //        }
    //        orfid.SubmitChanges();
    //        //Request Access
    //        //if the Supplier is disabled by enabling the chk_roles
    //        int IntAccID = int.Parse(StrAccid.ToString());

    //        if (chk_Roles.Enabled == true)
    //        {
    //            foreach (ListItem li in chk_Roles.Items)
    //            {
    //                if (li.Selected == true)
    //                {


    //                    int HospitalID = int.Parse(ddl_hospital.SelectedValue.ToString());
    //                    int RoleID = int.Parse(li.Value.ToString());
    //                    var RequestAccess = orfid.rp_implantRequestAccess(IntAccID, null,
    //                        HospitalID, RoleID);

    //                    foreach (var raccess in RequestAccess)
    //                    {
    //                        if (raccess.AdminAddress != null)
    //                            StrAdminAddress = StrAdminAddress + "," + raccess.AdminAddress.ToString();
    //                    }
    //                    RequestID = int.Parse(RequestAccess.ReturnValue.ToString());
    //                    orfid.SubmitChanges();
    //                }

    //            }

    //        }
    //        else// Here chk_roles is not enabled it means that Supplier is selected 
    //        {
    //            SupplierID = int.Parse(ddl_Supplier.SelectedValue.ToString());
    //            var SRequestAccess = orfid.rp_implantRequestAccess(IntAccID, SupplierID, null, null);
    //            foreach (var Saccess in SRequestAccess)
    //            {
    //                StrAdminAddress = StrAdminAddress + "," + Saccess.AdminAddress.ToString();
    //            }
    //            orfid.SubmitChanges();

    //        }

    //        /*Mail Functionality--> To send Emails to the Admin of the Roles selected to get Access.*/
    //        if (StrAdminAddress != "")
    //        {
    //            MailAddress = StrAdminAddress.Substring(StrAdminAddress.IndexOf(',') + 1, StrAdminAddress.Length - 1);
    //        }
    //        else
    //        {
    //            MailAddress = ConfigurationManager.AppSettings["SystemAdminAddress"].ToString();
    //        }
    //        //Sending Email to Admin
    //        string AdminSubject = "Request to Access Rfid for User:" + txt_Firstname + " " + txt_Lastname.Text;
    //        string AdminBody = "Can you please process the request from User:" + txt_Username.Text + "'<br/>'" + "To Accept or Deny user Follow the Link:";
    //        ORfidclass.MailRequests(MailAddress, txt_Username.Text, AdminSubject, AdminBody);
    //        //Sending Email to User with Password.
    //        string UserBody = "Your Request for RFID Access Has been Sent and Your Temporary Password" + " " + StrRandomPwd.ToString();
    //        string UserSubject = "Request for RFID Authentication";
    //        ORfidclass.MailRequests(txt_Username.Text, MailAddress, UserSubject, UserBody);
    //        Response.Redirect("Login.aspx?qy=NewUser", false);

    //    }
    //    catch (Exception EbtnCreate)
    //    {
    //        orfid.Usp_WebErrors("Users.aspx", "btn_create_click", DateTime.Now.AddDays(0), EbtnCreate.Message.ToString());
    //    }
        
    //}
    protected void chk_Roles_SelectedIndexChanged(object sender, EventArgs e)
    {
        checkroles_hospital();
    }
    protected void Chk_addadmin_CheckedChanged(object sender, EventArgs e)
    {
    }
    
    private void Mailusername(string StrRequestID)
    {
        try
        {
            if (StrRequestID != "")
            {
                int IntRequestID = int.Parse(StrRequestID.ToString());
            }

            Panel_NewUser.Visible = false;
            //Panel_gridrequests.Visible = false;

            var Orequest = orfid.rp_implantAccountRequests(int.Parse(Session["AccID"].ToString()), int.Parse(StrRequestID.ToString()));

            foreach (var Oreq in Orequest)
            {
                Session["MailUsername"] = Oreq.Requestor.ToString();
            }
        }
        catch (Exception EMailUser)
        {
            orfid.Usp_WebErrors("Users.aspx", "MailUsername", DateTime.Now.AddDays(0), EMailUser.Message.ToString());
        }
    }
    protected void Grid_requests_RowDataBound(object sender, GridViewRowEventArgs e)
    {
     /*   if ((e.Row.DataItemIndex == -1))
        {
            return;
        }

        e.Row.Attributes.Add("onMouseOver", "this.style.backgroundColor='#D7D7D7'");//this.style.cursor='hand'
        e.Row.Attributes.Add("onMouseOut", "this.style.backgroundColor='Silver'");*/
    }

    protected void btn_Approve_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    CheckBox chk_Approve;
        //    foreach (GridViewRow gRow in Grid_requests.Rows)
        //    {
        //        chk_Approve = (CheckBox)gRow.Cells[2].FindControl("chk_Requests");
        //        chk_Approve.Enabled = true;
        //        if (chk_Approve.Checked == true)
        //        {
        //            char CAdmin;
        //            bool chkAdmin = Chk_addadmin.Checked;
        //            if (chkAdmin == true)
        //                CAdmin = 'Y';
        //            else
        //                CAdmin = 'N';

        //            string StrRequestID = gRow.Cells[0].Text;
        //            Mailusername(StrRequestID);
        //            orfid.rp_implantAccountRoleApprove(int.Parse(StrRequestID), int.Parse(Session["AccID"].ToString()), 'Y', CAdmin);
        //            orfid.SubmitChanges();
        //            GridRequests();
        //            string StrAdmin = "";
        //            if (CAdmin == 'Y')
        //                StrAdmin = "Admin privilages has been Assigned";
        //            ORfidclass.MailRequests(Session["MailUsername"].ToString(), ConfigurationManager.AppSettings["SystemAdminAddress"].ToString(), "Request Approved", "Request For Rfid Access has been Approved by" + Session["User"].ToString() + " " + StrAdmin);
        //            Session.Remove("MailUsername");
        //        }
        //    }
        //}

        //catch (Exception EbtnApprove)
        //{
        //    orfid.Usp_WebErrors("Users.aspx", "btnApprove", DateTime.Now.AddDays(0), EbtnApprove.Message.ToString());
        //}
        
    }
    protected void btn_deny_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow gRow in Grid_requests.Rows)
            {
                CheckBox chk_Deny = (CheckBox)gRow.Cells[2].FindControl("chk_Requests");
                if (chk_Deny.Checked == true)
                {
                    char CAdmin;
                    bool chkAdmin = Chk_addadmin.Checked;
                    if (chkAdmin == true)
                        CAdmin = 'Y';
                    else
                        CAdmin = 'N';
                    string StrRequestID = gRow.Cells[0].Text;
                    Mailusername(StrRequestID);
                    orfid.rp_implantAccountRoleApprove(int.Parse(StrRequestID), int.Parse(Session["AccID"].ToString()), 'N', CAdmin);
                    orfid.SubmitChanges();
                    GridRequests();
                    ORfidclass.MailRequests(Session["MailUsername"].ToString(), ConfigurationManager.AppSettings["SystemAdminAddress"].ToString(), "Request Denied", "Request For Rfid Access has been Denied by" + Session["User"].ToString());
                    Session.Remove("MailUsername");
                }
            }
        }
        catch (Exception EbtnDeny)
        {
            orfid.Usp_WebErrors("Users.aspx", "btnApprove", DateTime.Now.AddDays(0), EbtnDeny.Message.ToString());
        }  
    }
    protected void ddl_Supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddl_Supplier.SelectedIndex != 0)
            {
                chk_Roles.Enabled = false;
                ddl_hospital.Enabled = false;
                ddl_hospital.CssClass = "ddl_Disabled";
            }
            else
            {
                chk_Roles.Enabled = true;
                ddl_hospital.Enabled = true;
                ddl_hospital.CssClass = "ddl_Enabled";
            }
        }
        catch (Exception EddlSupplier)
        {
            orfid.Usp_WebErrors("Users.aspx", "ddl_Supplier", DateTime.Now.AddDays(0), EddlSupplier.Message.ToString());
        }
    }
    protected void ddl_hospital_SelectedIndexChanged(object sender, EventArgs e)
    {
        checkroles_hospital();
    }

    private void checkroles_hospital()
    {
        try
        {
            if (ddl_hospital.SelectedIndex != 0 || bChk_Roles() == true)
            {
                ddl_Supplier.Enabled = false;
                ddl_Supplier.CssClass = "ddl_Disabled";
            }
            else
            {
                ddl_Supplier.Enabled = true;
                ddl_Supplier.CssClass = "ddl_Enabled";
            }
        }
        catch (Exception EchkRoles)
        {
            orfid.Usp_WebErrors("Users.aspx", "chkRoles_Hospital", DateTime.Now.AddDays(0), EchkRoles.Message.ToString());
        }
    }

    private bool bChk_Roles()
    {
        try
        {
            bool bchkroles = false;
            foreach (ListItem chk in chk_Roles.Items)
            {
                if (chk.Selected == true)
                    bchkroles = true;
            }
            return bchkroles;
        }
        catch (Exception EchkRoles)
        {
            orfid.Usp_WebErrors("Users.aspx", "bChk_Roles()", DateTime.Now.AddDays(0), EchkRoles.Message.ToString());
            return bChk_Roles();
        }
    }
    //protected void btn_Enable_Click(object sender, EventArgs e)
    //{
    //    char CenStatus='N';
    //    Accountmodify(CenStatus);
    //}

    private void Accountmodify(char Cstatus)
    {
        try
        {
            foreach (GridViewRow Orow in Grd_ListUsers.Rows)
            {
                CheckBox Chk_Enable = (CheckBox)Orow.Cells[0].FindControl("chk_listusers");
                if (Chk_Enable.Checked == true)
                {
                    //var AccountEnable = from AccStatus in orfid.tblImplantAccounts
                    //                    where AccStatus.accUsername == Orow.Cells[1].Text
                    //                    select AccStatus;
                    //foreach (var item in AccountEnable)
                    //{
                    //    item.accInactive = Cstatus;
                    //}
                    var AccountRoleenable = from Roleenable in orfid.tblImplantAccountRoles
                                            where Roleenable.arAccId == int.Parse(Orow.Cells[1].Text)
                                            select Roleenable;
                    foreach (var item in AccountRoleenable)
                    {
                        item.arInactive = Cstatus;
                    }

                    orfid.SubmitChanges();
                }
            }
            Grid_ListofUsers();
        }
        catch (Exception EAccountModify)
        {
            orfid.Usp_WebErrors("Users.aspx", "AccountModify", DateTime.Now.AddDays(0), EAccountModify.Message.ToString());
           
        }
    }
    //protected void btn_Disable_Click(object sender, EventArgs e)
    //{
    //    char CdisStatus = 'Y';
    //    Accountmodify(CdisStatus);
        
    //}
    //protected void btn_Delete_Click(object sender, EventArgs e)
    //{
        

    //}
   
    protected void Imgbtn_deny_Click(object sender, ImageClickEventArgs e)
    {
    }
    //protected void Imgbtn_enable_Click(object sender, ImageClickEventArgs e)
    //{
    //    char CenStatus = 'N';
    //    Accountmodify(CenStatus);
    //}
    //protected void Imgbtn_Disable_Click(object sender, ImageClickEventArgs e)
    //{
    //    char CdisStatus = 'Y';
    //    Accountmodify(CdisStatus);
    //}
    
    protected void Grd_ListUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void Imgbtn_Createaccount_Click(object sender, EventArgs e)
    {
        try
        {
            int SupplierID = -1;
            string StrDisplayname = "";
            string StrAdminAddress = "";
            string StrRandomPwd = "";
            string StrAccid = "";
            string MailAddress = "";
            int RequestID = 0;
            string StrHospname = "";
            string StrSuppliername = "";
            string StrQrynewuser = "";
            // Creating a Random Password.
            StrDisplayname = txt_Firstname.Text + " " + txt_Lastname.Text;

            var Randompwd = orfid.rp_implantRandomPassword();
            foreach (var pwd in Randompwd)
            {
                StrRandomPwd = pwd.RandomPassword.ToString();
            }
            //Getting the Account ID

            var AccIDReturn = orfid.rp_implantAccountAdd(txt_Username.Text, StrRandomPwd, StrDisplayname);
            foreach (var accid in AccIDReturn)
            {
                StrAccid = accid.AccountId.Value.ToString();
            }
            orfid.SubmitChanges();
            //Request Access
            //if the Supplier is disabled by enabling the chk_roles
            int IntAccID = int.Parse(StrAccid.ToString());

            if (chk_Roles.Enabled == true)
            {
                foreach (ListItem li in chk_Roles.Items)
                {
                    if (li.Selected == true)
                    {
                        StrHospname = ddl_hospital.SelectedItem.Text;
                        int HospitalID = int.Parse(ddl_hospital.SelectedValue.ToString());
                        int RoleID = int.Parse(li.Value.ToString());
                        var RequestAccess = orfid.rp_implantRequestAccess(IntAccID, null,
                            HospitalID, RoleID);

                        foreach (var raccess in RequestAccess)
                        {
                            if (raccess.AdminAddress != null)
                                StrAdminAddress = StrAdminAddress + "," + raccess.AdminAddress.ToString();
                        }
                        RequestID = int.Parse(RequestAccess.ReturnValue.ToString());
                        orfid.SubmitChanges();
                    }
                }
            }
            else// Here chk_roles is not enabled it means that Supplier is selected 
            {
                StrSuppliername = ddl_Supplier.SelectedItem.Text;
                SupplierID = int.Parse(ddl_Supplier.SelectedValue.ToString());
                var SRequestAccess = orfid.rp_implantRequestAccess(IntAccID, SupplierID, null, null);
                foreach (var Saccess in SRequestAccess)
                {
                    StrAdminAddress = StrAdminAddress + "," + Saccess.AdminAddress.ToString();
                }
                orfid.SubmitChanges();

            }
            var SystemAdminAddress = ConfigurationManager.AppSettings["SystemAdminAddress"].ToString();

            /*Mail Functionality--> To send Emails to the Admin of the Roles selected to get Access.*/
            if (StrAdminAddress != "")
            {
                MailAddress = StrAdminAddress.Substring(StrAdminAddress.IndexOf(',') + 1, StrAdminAddress.Length - 1);
            }
            else
            {
                MailAddress = SystemAdminAddress;
            }
            //Sending Email to Admin
            string AdminSubject = "Request to Access Rfid for User:" + txt_Firstname.Text + " " + txt_Lastname.Text;
            string AdminBody = "Can you please process the request from User:" + txt_Username.Text + "'<br/>'";
            ORfidclass.MailRequests(MailAddress, SystemAdminAddress, AdminSubject, AdminBody);
            //Sending Email to User with Password.
            string UserBody = "Your Request for RFID Access Has been Sent and Your Temporary Password" + " " + StrRandomPwd.ToString();
            string UserSubject = "Request for RFID Authentication";
            ORfidclass.MailRequests(txt_Username.Text, SystemAdminAddress, UserSubject, UserBody);
            if (StrHospname != "")
            {
                StrQrynewuser = StrHospname;
                
            }
            else
            {
                StrQrynewuser = StrSuppliername;
            }
            Response.Redirect("Login.aspx?qy=" + StrQrynewuser, false);
            //Response.Redirect("Login.aspx?qy=NewUser", false);

        }
        catch (Exception EbtnCreate)
        {
            orfid.Usp_WebErrors("Users.aspx", "btn_create_click", DateTime.Now.AddDays(0), EbtnCreate.Message.ToString());
        }
    }
   
    protected void lnkbtn_Approve_Click(object sender, EventArgs e)
    {
        try
        {
            CheckBox chk_Approve;
            foreach (GridViewRow gRow in Grid_requests.Rows)
            {
                chk_Approve = (CheckBox)gRow.Cells[2].FindControl("chk_Requests");
                chk_Approve.Enabled = true;
                if (chk_Approve.Checked == true)
                {
                    char CAdmin;
                    bool chkAdmin = Chk_addadmin.Checked;
                    if (chkAdmin == true)
                        CAdmin = 'Y';
                    else
                        CAdmin = 'N';

                    string StrRequestID = gRow.Cells[0].Text;
                    Mailusername(StrRequestID);
                    orfid.rp_implantAccountRoleApprove(int.Parse(StrRequestID), int.Parse(Session["AccID"].ToString()), 'Y', CAdmin);
                    orfid.SubmitChanges();
                    GridRequests();
                    string StrAdmin = "";
                    if (CAdmin == 'Y')
                        StrAdmin = "Admin privilages has been Assigned";
                    ORfidclass.MailRequests(Session["MailUsername"].ToString(), ConfigurationManager.AppSettings["SystemAdminAddress"].ToString(), "Request Approved", "Request For Rfid Access has been Approved by" + Session["User"].ToString() + " " + StrAdmin);
                    Session.Remove("MailUsername");
                }
            }
            Chk_addadmin.Checked = false;
        }

        catch (Exception EbtnApprove)
        {
            orfid.Usp_WebErrors("Users.aspx", "btnApprove", DateTime.Now.AddDays(0), EbtnApprove.Message.ToString());
        }
    }
    protected void lnkbtn_deny_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow gRow in Grid_requests.Rows)
            {
                CheckBox chk_Deny = (CheckBox)gRow.Cells[2].FindControl("chk_Requests");
                if (chk_Deny.Checked == true)
                {
                    char CAdmin;
                    bool chkAdmin = Chk_addadmin.Checked;
                    if (chkAdmin == true)
                        CAdmin = 'Y';
                    else
                        CAdmin = 'N';
                    string StrRequestID = gRow.Cells[0].Text;
                    Mailusername(StrRequestID);
                    orfid.rp_implantAccountRoleApprove(int.Parse(StrRequestID), int.Parse(Session["AccID"].ToString()), 'N', CAdmin);
                    orfid.SubmitChanges();
                    GridRequests();
                    ORfidclass.MailRequests(Session["MailUsername"].ToString(), ConfigurationManager.AppSettings["SystemAdminAddress"].ToString(), "Request Denied", "Request For Rfid Access has been Denied by" + Session["User"].ToString());
                    Session.Remove("MailUsername");
                }
            }
        }
        catch (Exception EbtnDeny)
        {
            orfid.Usp_WebErrors("Users.aspx", "btnApprove", DateTime.Now.AddDays(0), EbtnDeny.Message.ToString());
        }  
    }
    protected void lnkbtn_enable_Click(object sender, EventArgs e)
    {
        char CenStatus = 'N';
        Accountmodify(CenStatus);
    }
    protected void lnkbtn_disable_Click(object sender, EventArgs e)
    {
        char CdisStatus = 'Y';
        Accountmodify(CdisStatus);
    }
    protected void Imgbtn_Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
}

